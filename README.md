**そげぶキラー**
====

**Overview**
====

*2017/3月に完成した作品です。*
*  [Execution Data](Execution%20Data) - そげぶキラーの実行データです。
*  [Develop](Develop) - そげぶキラーの開発データです。

**Requirement**
====

*  [IDE (Integrated Development Environment)](https://my.visualstudio.com/Downloads?q=Visual%20Studio%202010&pgroup=) - Visual Studio 2010

##

![VisualStudio2010](Asset/VisualStudio2010.png)![c](Asset/c.png)