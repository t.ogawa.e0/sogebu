//***********************************************************
//	HEW作品制作
//	そげぶキラー[AA.h]
//***********************************************************
#include"main.h"
#include"PlayerStructure.h"
#include"EnemyStructure.h"
#include"Lifebar.h"

//***********************************************************
//マクロ定義
#ifndef _AA_H_			//二重インクルード防止のマクロ定義
#define _AA_H_
#define _AA (1)			//アスキーアート構造の数
#define PlayerLimit (5)	//数
#define EnemyLimit (6)	//数
#define EnemyAAcase (8)	//箱上限
#define AAbite (256)	//バイト数

//***********************************************************
//構造体定義
typedef struct
{
	int PlayerAA;						//AA種類決め
	int PlayerlastAA;					//最後のAA
	int PlayerAAold;					//ひとつ前のAA
	int EnemyAA[EnemyAAcase + 3];		//エネミーのAA
	int EnemyAAold[EnemyAAcase + 3];	//ひとつ前のエネミーのAA
	int EnemyMoveoldAA[EnemyAAcase + 3];//動きに使用された前回のAA
}AA;

//***********************************************************
//プロトタイプ宣言
void InitAA(AA *aAA);									//初期処理
void UpdateAA(AA *aAA);									//更新処理
void DrawAA(AA *aAA);									//描画処理
void UninitAA(void);									//後処理
void AAPlayer(PLAYER *aPlayer, AA *aAA);				//プレイヤーのAA
void AAPlayerold(PLAYER *aPlayer, AA *aAA);				//ひとつ前用のAA
void PlayerAAlast(PLAYER *aPlayer, AA *aAA);			//最後のAA
void AAEnemy(MAIN *aMain, ENEMY *aEnemy, AA *aAA);		//エネミーのAA
void EnemyAAlast(MAIN *aMain, ENEMY *aEnemy, AA *aAA);	//エネミーの最後のAA
void AAEnemyold(MAIN *aMain, ENEMY *aEnemy, AA *aAA);	//エネミーのひとつ前のAA

#endif // !_AA_H_