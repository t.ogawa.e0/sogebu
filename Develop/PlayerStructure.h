//***********************************************************
//	HEW作品制作
//	そげぶキラー[PlayerStructure.h]
//***********************************************************
#include"main.h"

//***********************************************************
//	マクロ定義
#ifndef _PlayerStructure_H_			//二重インクルード防止のマクロ定義
#define _PlayerStructure_H_
#define PlayerKeyCase (1)			//キーケース
#define PlayerSpaceCoordinates (0)	//スペースキー座標
#define PlayerProcessCase (1)		//処理判定ケース
#define PlayerCountCase (2)			//カウントの種類
#define PlayerProcessJump (0)		//ジャンプ処理終了判定番地
#define PlayerCountJump (0)			//ジャンプ処理番地
#define PlayerCountGravity (1)		//重力処理番地
#define Player_to_Use (0)			//プレイヤーの番地
#define PlayerXpozi (2)				//AA表示座標
#define PlayerLife (5000)			//プレイヤーのHP

//***********************************************************
//構造体宣言
typedef struct
{
	int nGravity;								//重力
	int Life;									//体力
	int EffectCount;							//エフェクトカウント
	int EffectPozisyonX, EffectPozisyonY;		//エフェクトポジション
	float fPosX;								//プレイヤーの位置(X座標)
	float fPosY;								//プレイヤーの位置(Y座標)
	float fPosXold;								//プレイヤーの前回の位置(X座標)
	float fPosYold;								//プレイヤーの前回の位置(Y座標)
	float fCount[PlayerCountCase];				//カウント格納ケース
	bool bPushDOWN;								//降りる判定
	bool bTump;									//ジャンプ中かどうか
	bool bPush_Key[PlayerKeyCase];				//キーの状態
	bool bProcess_Judgment[PlayerProcessCase];	//処理終了判定
	bool bLifeIni;								//体力ゲージ描画フラグ
	bool bAnnihilationEffect, bLipopEffect, EffectEnd;	//エフェクトフラグ
}PLAYER;

#endif // !_PlayerStructure_H_