//***********************************************************
//	HEW作品制作
//	そげぶキラー[Explosion.h]
//***********************************************************
#include"main.h"
#include"world.h"
#include"Barrett.h"
#include"PlayerStructure.h"
#include"EnemyStructure.h"

//***********************************************************
//マクロ定義
#ifndef _Explosion_H_	//二重インクルード防止のマクロ定義
#define _Explosion_H_

//***********************************************************
//構造体定義
typedef struct
{
	int ExplosionCount[BarrettCase][LimitBarrett];		//爆破カウント
	int nExplosionCountStart[BarrettCase];				//開始値
	int nExplosionCountEnd[BarrettCase];				//弾カウントの終わり
	float fExplosion_X[BarrettCase][LimitBarrett];		//現在の弾の移動距離
	float fExplosion_Y[BarrettCase][LimitBarrett];		//現在の弾の高さ
	float fExplosionEnd_X[BarrettCase][LimitBarrett];	//ひとつ前の弾の場所
	float fExplosionEnd_Y[BarrettCase][LimitBarrett];	//ひとつ前の弾の高さ
	bool bExplosionPush;								//キーフラグ
}EXPLOSION;

//***********************************************************
//プロトタイプ宣言
void InitExplosion(EXPLOSION *aExplosion);											//初期化
void UpdateExplosion(BARRETT *aBarrett, EXPLOSION *aExplosion, PLAYER *aPlayer);	//更新
void DrawExplosion(BARRETT *aBarrett, EXPLOSION *aExplosion);						//描画
void UninitExplosion(void);															//後処理

#endif // !_Explosion_H_
