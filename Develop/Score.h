//***********************************************************
//	HEW作品制作
//	そげぶキラー[Score.h]
//***********************************************************
#include"main.h"

//***********************************************************
//マクロ定義
#ifndef _Score_H_				//二重インクルード防止のマクロ定義
#define _Score_H_
#define ScorePositionX (137)	//スコアポジション
#define ScorePositionY (88)		//スコアポジション

//***********************************************************
//プロトタイプ宣言
void InitScore(MAIN *aMain, SCORE *aScore);		//初期化
void ScoreProcess(MAIN *aMain, SCORE *aScore);	//スコア処理
void Score(MAIN *aMain);						//スコア描画

#endif // !_Score_H_