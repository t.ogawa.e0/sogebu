//***********************************************************
//	デバッグ用
//	そげぶキラー[Debug.h]
//***********************************************************
#include"main.h"
#include"player.h"
#include"PlayerStructure.h"
#include"world.h"
#include"Enemy.h"
#include"EnemyStructure.h"
#include"Barrett.h"
#include"Explosion.h"

//***********************************************************
//マクロ定義
#ifndef _Debug_H_
#define _Debug_H_

//***********************************************************
//構造体宣言
typedef struct
{
	int nDebugChange;
	bool push;
}DEBUG;

//***********************************************************
//プロトタイプ宣言
void InitDebug(DEBUG *aDebug);
void Debug(MAIN *aMain, PLAYER *aPlayer, ENEMY *aEnemy, WORLD *aWorld, BARRETT *aBarrett, EXPLOSION *aExplosion, DEBUG *aDebug);

#endif // !_Debug_H_
