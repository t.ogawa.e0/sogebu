//***********************************************************
//	HEW作品制作
//	そげぶキラー[Barrett.h]
//***********************************************************
#include"main.h"
#include"world.h"
#include"PlayerStructure.h"
#include"EnemyStructure.h"
#include"Enemy.h"
#include"AA.h"

//***********************************************************
//マクロ定義
#ifndef _Barrett_H_			//二重インクルード防止のマクロ定義
#define _Barrett_H_
#define BarrettSpeed (1.5)	//弾の速さ
#define BarrettCase (4)		//弾のケース
#define LimitBarrett (50)	//各ケース上限
#define BarrettDamage (100)	//バレットの威力

//***********************************************************
//構造体定義
typedef struct
{
	int BarrettCount;
	int nBarrettCountStart[BarrettCase];					//開始値
	int nBarrettCountEnd[BarrettCase];						//弾カウントの終わり
	int nAnnihilationCountEnd[BarrettCase];					//バレットアニメーション最終値
	int nBarrettDamage;										//ダメージ
	int nBarrettSE;
	float fBarrett_X[BarrettCase][LimitBarrett];			//現在の弾の移動距離
	float fBarrett_Y[BarrettCase][LimitBarrett];			//現在の弾の高さ
	float fBarrettEnd_X[BarrettCase][LimitBarrett];			//ひとつ前の弾の場所
	float fBarrettEnd_Y[BarrettCase][LimitBarrett];			//ひとつ前の弾の高さ
	float fAnnihilation_X[BarrettCase][LimitBarrett];		//消滅した弾の位置
	float fAnnihilation_Y[BarrettCase][LimitBarrett];		//消滅した弾の高さ
	bool bBarrettEnd;										//処理フラグ
	bool bBarrettPush;										//キーフラグ
	bool bAnnihilation;										//発動判定
	bool bBarrettJudgment[BarrettCase][LimitBarrett];		//当たり判定
	bool bbarrettAnnihilation[BarrettCase][LimitBarrett];	//アニメーション判定
}BARRETT;

//***********************************************************
//プロトタイプ宣言
void InitBarrett(BARRETT *aBarrett);																			//初期化
void UpdateBarrett(MAIN *aMain, BARRETT *aBarrett, PLAYER *aPlayer,  AA *aAA, ENEMY *aEnemy, WORLD *aWorld);	//更新
void DrawBarrett(BARRETT *aBarrett);																			//描画
void UninitBarrett(void);																						//後処理
#endif // !_Barrett_H_
