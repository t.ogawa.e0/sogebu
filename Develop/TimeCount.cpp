//***********************************************************
//	HEWμi§μ
//	»°ΤL[[Time.cpp]
//***********************************************************
#include"main.h"
#include"TimeCount.h"
#include"Home.h"

//***********************************************************
//ϊ
void InitTime(TIME *aTimeCount)
{
	int nCount;

	//Τϊ»
	for (nCount = 0; nCount < 5; nCount++)
	{
		aTimeCount->bTime[nCount][Time_left] = true;
		aTimeCount->bTime[nCount][Time_right] = true;
		aTimeCount->fTime[nCount][Time_left] = 0;
		aTimeCount->fTime[nCount][Time_right] = 0;
		aTimeCount->mTimePosition[nCount][Time_left] = 0;
		aTimeCount->mTimePosition[nCount][Time_right] = 0;
		aTimeCount->nTimeold[nCount][Time_left] = 100;
		aTimeCount->nTimeold[nCount][Time_right] = 100;
	}
	aTimeCount->nTimePozi = 0;
	aTimeCount->nTimeHeit = 0;
	aTimeCount->bEndGame = true;
	aTimeCount->nTimeCount = 0;

	//Iπ΅½ΤΙνΉΔ^C}[Zbg
	switch (aTimeCount->nTime)
	{
	case 0:
		aTimeCount->fTime[TimeA][Time_right] = 1;
		aTimeCount->fTime[TimeB][Time_left] = 0;
		aTimeCount->fTime[TimeB][Time_right] = 0;
		aTimeCount->fTime[TimeC][Time_left] = 0;
		aTimeCount->fTime[TimeC][Time_right] = 0;
		break;
	case 1:
		aTimeCount->fTime[TimeA][Time_right] = 1;
		aTimeCount->fTime[TimeB][Time_left] = 3;
		aTimeCount->fTime[TimeB][Time_right] = 0;
		aTimeCount->fTime[TimeC][Time_left] = 0;
		aTimeCount->fTime[TimeC][Time_right] = 0;
		break;
	case 2:
		aTimeCount->fTime[TimeA][Time_right] = 2;
		aTimeCount->fTime[TimeB][Time_left] = 0;
		aTimeCount->fTime[TimeB][Time_right] = 0;
		aTimeCount->fTime[TimeC][Time_left] = 0;
		aTimeCount->fTime[TimeC][Time_right] = 0;
		break;
	default:
		break;
	}
}

//***********************************************************
//XV
void UpdateTime(TIME *aTimeCount, MAIN *aMain, SCORE*aScore)
{
	//Τ
	TimeReverseCount(&aTimeCount[0], &aMain[0], &aScore[0]);
}

//***********************************************************
//`ζ
void DrawTime(TIME *aTimeCount)
{
	//Τ`ζp
	TimeA_light(&aTimeCount[0]);
	TimeA_left(&aTimeCount[0]);
	TimeB_light(&aTimeCount[0]);
	TimeB_left(&aTimeCount[0]);
	TimeC_light(&aTimeCount[0]);
	TimeC_left(&aTimeCount[0]);
}

//***********************************************************
//γ
void UninitTime(void)
{}

//***********************************************************
//^C}[
void TimeReverseCount(TIME *aTimeCount, MAIN *aMain, SCORE*aScore)
{
	//e^CΜΚͺ0ΕΝ³’Ζ«
	if (!((aTimeCount->fTime[TimeA][Time_right] == 0) && (aTimeCount->fTime[TimeA][Time_left] == 0) && (aTimeCount->fTime[TimeB][Time_right] == 0) && (aTimeCount->fTime[TimeB][Time_left] == 0) && (aTimeCount->fTime[TimeC][Time_right] == 0) && (aTimeCount->fTime[TimeC][Time_left] == 0)))
	{
		//ͺΜeΚͺ0ΕΝΘ’Ζ«
		if (!((aTimeCount->fTime[TimeA][Time_right] == 0) && (aTimeCount->fTime[TimeA][Time_left] == 0)))
		{
			//bΖR}ͺ0ΙΘΑ½Ζ«
			if ((aTimeCount->fTime[TimeB][Time_right] <= 0) && (aTimeCount->fTime[TimeB][Time_left] <= 0) && (aTimeCount->fTime[TimeC][Time_right] <= 2) && (aTimeCount->fTime[TimeC][Time_left] <= 0))
			{
				//ͺΜκΜΚ©ηψ­
				aTimeCount->fTime[TimeA][Time_right] -= 1;
			}

			//ͺΜ1ΜΚͺ0Μ
			if (aTimeCount->fTime[TimeA][Time_right] <= 0)
			{
				//1ΜΚ
				aTimeCount->fTime[TimeA][Time_right] = 0;

				//10ΜΚ
				aTimeCount->fTime[TimeA][Time_left] -= 1;

				//ͺΜeΚ
				aTimeCount->fTime[TimeB][Time_right] = 0;
				aTimeCount->fTime[TimeB][Time_left] = 6;
			}

			//ͺΜ10ΜΚͺ`Μ
			if (aTimeCount->fTime[TimeA][Time_left] <= -1)
			{
				//10ΜΚ
				aTimeCount->fTime[TimeA][Time_left] = 0;
				aTimeCount->fTime[TimeB][Time_right] = 0;
				aTimeCount->fTime[TimeB][Time_left] = 6;
			}
		}

		//ͺΖbΜeΚͺ0ΕΝΘ’Ζ«
		if (!((aTimeCount->fTime[TimeA][Time_right] == 0) && (aTimeCount->fTime[TimeA][Time_left] == 0) && (aTimeCount->fTime[TimeB][Time_right] == 0) && (aTimeCount->fTime[TimeB][Time_left] == 0)))
		{
			//bͺ0ΕΝΘ’Ζ«
			if ((aTimeCount->fTime[TimeC][Time_right] <= 2) && (aTimeCount->fTime[TimeC][Time_left] <= 0))
			{
				//bΜκΜΚ©ηψ­
				aTimeCount->fTime[TimeB][Time_right] -= 1;
			}

			//bΜκΜΚͺ0ΘΊΜΖ«
			if (aTimeCount->fTime[TimeB][Time_right] <= -1)
			{
				//1ΜΚ
				aTimeCount->fTime[TimeB][Time_right] = 9;

				//10ΜΚ
				aTimeCount->fTime[TimeB][Time_left] -= 1;
			}

			//bΜ10ΜΚͺ`Μ
			if (aTimeCount->fTime[TimeB][Time_left] <= -1)
			{
				//10ΜΚ
				aTimeCount->fTime[TimeB][Time_left] = 5;
			}
		}

		//ͺΖbΖR}ͺ0ΕΝΘ’Ζ«
		if (!((aTimeCount->fTime[TimeA][Time_right] == 0) && (aTimeCount->fTime[TimeA][Time_left] == 0) && (aTimeCount->fTime[TimeB][Time_right] == 0) && (aTimeCount->fTime[TimeB][Time_left] == 0) && (aTimeCount->fTime[TimeC][Time_left] == 0) && (aTimeCount->nTimeCount == 5)))
		{
			//R}ΜΈZ
			//1b[100R}]60FPS=1.6666(ry ¨ ¬_ΘΊζOΚlΜάό ¨ 1.67
			aTimeCount->fTime[TimeC][Time_right] -= 1.67;

			//R}ΜκΜΚͺ0ΘΊΙΘΑ½Ζ«
			if (aTimeCount->fTime[TimeC][Time_right] <= 0)
			{
				aTimeCount->fTime[TimeC][Time_right] = 0;
			}

			//R}ΜκΜΚͺ0ΜΖ«
			if (aTimeCount->fTime[TimeC][Time_right] <= 0)
			{
				//1ΜΚ
				aTimeCount->fTime[TimeC][Time_right] = 9;

				//IΉL[ΜJEgZbg
				aTimeCount->nTimeCount = 0;

				//10ΜΚ
				aTimeCount->fTime[TimeC][Time_left] -= 1;
			}

			//R}Μ10ΜΚͺ`Μ
			if (aTimeCount->fTime[TimeC][Time_left] <= -1)
			{
				//10ΜΚ
				aTimeCount->fTime[TimeC][Time_left] = 9;
			}
			//IΉL[ΜJEg
			aTimeCount->nTimeCount += 1;
		}
	}

	//IΉ
	if ((aTimeCount->fTime[TimeA][Time_right] == 0) && (aTimeCount->fTime[TimeA][Time_left] == 0) && (aTimeCount->fTime[TimeB][Time_right] == 0) && (aTimeCount->fTime[TimeB][Time_left] == 0) && (aTimeCount->fTime[TimeC][Time_left] == 0) && (aTimeCount->nTimeCount == 5))
	{
		//R}ΜκΜΚπ0Ι
		aTimeCount->fTime[TimeC][Time_right] = 0;

		//tOμ
		aMain->bIniHome = true;
		aMain->bHome = false;
		aMain->bHomeLiset = true;
		aMain->bStartGame = true;
		aMain->bGameLiset = true;
		aMain->bRog = false;

		//IΉΜXRAπL^\¦pP[XΙi[
		aMain->nPlayIniRecord[0] = aMain->nScore[0];
		aMain->nPlayIniRecord[1] = aMain->nScore[1];
		aMain->nPlayIniRecord[2] = aMain->nScore[2];

		//XRA
		//ΕVL^©Ϋ©Μ
		HomeScoreConp(&aMain[0], &aScore[0]);

		//OΦi[
		HomeRecord(&aMain[0], &aScore[0]);

		//ΫΆ
		RecordSave(&aScore[0]);

		//etOμ
		aMain->bPlayGame = false;
		aMain->bGameEnd = true;
		aMain->bLoad = true;
	}
}

//***********************************************************
//ͺΜ10ΜΚ
void TimeA_light(TIME *aTimeCount)
{
	int time, nCount;

	time = aTimeCount->fTime[TimeA][Time_left];
	aTimeCount->mTimePosition[TimeA][Time_left] = aTimeCount->nTimePozi;
	if (!(time == aTimeCount->nTimeold[TimeA][Time_left]))
	{
		aTimeCount->nTimeold[TimeA][Time_left] = time;
		nCount = aTimeCount->nTimeHeit;
		switch (time)
		{
		case 0:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			break;
		case 1:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			break;
		case 2:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			break;
		case 3:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			break;
		case 4:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			break;
		case 5:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			break;
		case 6:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			break;
		case 7:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			break;
		case 8:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			break;
		case 9:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_left], nCount);
			printf("‘‘‘‘‘");
			break;
		default:
			break;
		}
	}
}

//***********************************************************
//ͺΜ1ΜΚ
void TimeA_left(TIME *aTimeCount)
{
	int time, nCount;

	time = aTimeCount->fTime[TimeA][Time_right];
	aTimeCount->mTimePosition[TimeA][Time_right] = aTimeCount->mTimePosition[TimeA][Time_left] + 11;
	if (!(time == aTimeCount->nTimeold[TimeA][Time_right]))
	{
		aTimeCount->nTimeold[TimeA][Time_right] = time;
		nCount = aTimeCount->nTimeHeit;
		switch (time)
		{
		case 0:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			break;
		case 1:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			break;
		case 2:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			break;
		case 3:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			break;
		case 4:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			break;
		case 5:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			break;
		case 6:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			break;
		case 7:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			break;
		case 8:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			break;
		case 9:
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeA][Time_right], nCount);
			printf("‘‘‘‘‘");
			break;
		default:
			break;
		}
	}
}

//***********************************************************
//bΜ10ΜΚ
void TimeB_light(TIME *aTimeCount)
{
	int time, nCount;

	time = aTimeCount->fTime[TimeB][Time_left];
	aTimeCount->mTimePosition[TimeB][Time_left] = aTimeCount->mTimePosition[TimeA][Time_right] + 14;
	if (!(time == aTimeCount->nTimeold[TimeB][Time_left]))
	{
		aTimeCount->nTimeold[TimeB][Time_left] = time;
		nCount = aTimeCount->nTimeHeit;
		nCount += 1;
		LOCATE(aTimeCount->mTimePosition[TimeB][Time_left] - 3, nCount);
		printf("");
		nCount += 4;
		LOCATE(aTimeCount->mTimePosition[TimeB][Time_left] - 3, nCount);
		printf("");

		nCount = aTimeCount->nTimeHeit;
		switch (time)
		{
		case 0:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			break;
		case 1:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			break;
		case 2:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			break;
		case 3:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			break;
		case 4:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			break;
		case 5:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			break;
		case 6:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			break;
		case 7:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			break;
		case 8:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			break;
		case 9:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_left], nCount);
			printf("‘‘‘‘‘");
			break;
		default:
			break;
		}
	}
}

//***********************************************************
//bΜ1ΜΚ
void TimeB_left(TIME *aTimeCount)
{
	int time, nCount;

	time = aTimeCount->fTime[TimeB][Time_right];
	aTimeCount->mTimePosition[TimeB][Time_right] = aTimeCount->mTimePosition[TimeB][Time_left] + 11;
	if (!(time == aTimeCount->nTimeold[TimeB][Time_right]))
	{
		aTimeCount->nTimeold[TimeB][Time_right] = time;
		nCount = aTimeCount->nTimeHeit;
		switch (time)
		{
		case 0:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			break;
		case 1:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			break;
		case 2:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			break;
		case 3:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			break;
		case 4:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			break;
		case 5:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			break;
		case 6:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			break;
		case 7:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			break;
		case 8:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			break;
		case 9:
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("@@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeB][Time_right], nCount);
			printf("‘‘‘‘‘");
			break;
		default:
			break;
		}
	}
}

//***********************************************************
//R}Μ10ΜΚ
void TimeC_light(TIME *aTimeCount)
{
	int time, nCount;

	time = aTimeCount->fTime[TimeC][Time_left];
	aTimeCount->mTimePosition[TimeC][Time_left] = aTimeCount->mTimePosition[TimeB][Time_right] + 14;
	if (!(time == aTimeCount->nTimeold[TimeC][Time_left]))
	{
		nCount = (aTimeCount->nTimeHeit + 2);
		switch (time)
		{
		case 0:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			break;
		case 1:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("@@@‘");
			break;
		case 2:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			break;
		case 3:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			break;
		case 4:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("@@@‘");
			break;
		case 5:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			break;
		case 6:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			break;
		case 7:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("@@@‘");
			break;
		case 8:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			break;
		case 9:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_left], nCount);
			printf("‘‘‘‘");
			break;
		default:
			break;
		}
	}
}

//***********************************************************
//R}Μ1ΜΚ
void TimeC_left(TIME *aTimeCount)
{
	int time, nCount;

	time = aTimeCount->fTime[TimeC][Time_right];
	aTimeCount->mTimePosition[TimeC][Time_right] = aTimeCount->mTimePosition[TimeC][Time_left] + 9;
	if (!(time == aTimeCount->nTimeold[TimeC][Time_right]))
	{
		aTimeCount->nTimeold[TimeC][Time_right] = time;
		nCount = (aTimeCount->nTimeHeit + 2);
		switch (time)
		{
		case 0:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			break;
		case 1:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("@@@‘");
			break;
		case 2:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			break;
		case 3:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			break;
		case 4:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("@@@‘");
			break;
		case 5:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			break;
		case 6:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘@@@");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			break;
		case 7:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("@@@‘");
			break;
		case 8:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			break;
		case 9:
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("@@@‘");
			nCount += 1;
			LOCATE(aTimeCount->mTimePosition[TimeC][Time_right], nCount);
			printf("‘‘‘‘");
			break;
		default:
			break;
		}
	}
}