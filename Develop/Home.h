//***********************************************************
//	HEW作品制作
//	そげぶキラー[Home.h]
//***********************************************************
#include"TimeCount.h"
#include"AA.h"
#include"text.h"

//***********************************************************
//	マクロ定義
#ifndef _HOME_H_		//二重インクルード防止のマクロ定義
#define _HOME_H_
#define HomeCount (1)	//ホームの構造数

//メニューA
#define MenuA_X (16)
#define MenuA_endX (76)
#define MenuA_Y (60)
#define MenuA_endY (90)
#define MenuA_TimeX (8)
#define MenuA_TimeY (82)

//メニューB
#define MenuB_X (86)
#define MenuB_endX (146)
#define MenuB_Y (60)
#define MenuB_endY (90)
#define MenuB_TimeX (78)
#define MenuB_TimeY (82)

//メニューC
#define MenuC_X (156)
#define MenuC_endX (216)
#define MenuC_Y (60)
#define MenuC_endY (90)
#define MenuC_TimeX (148)
#define MenuC_TimeY (82)

//ステージA
#define Stage_A_X (16)
#define Stage_A_endX (216)
#define Stage_A_Y (10)
#define Stage_A_endY (20)

//ステージB
#define Stage_B_X (16)
#define Stage_B_endX (216)
#define Stage_B_Y (22)
#define Stage_B_endY (32)

//ステージC
#define Stage_C_X (16)
#define Stage_C_endX (216)
#define Stage_C_Y (34)
#define Stage_C_endY (44)

//ステージB
#define Stage_D_X (16)
#define Stage_D_endX (216)
#define Stage_D_Y (46)
#define Stage_D_endY (56)

//記録
#define Record_X (220)
#define Record_endX (295)
#define Record_Y (34)
#define Record_endY (90)

//***********************************************************
//構造体宣言
typedef struct
{
	int nKeySE;
	int nMenutTarget, nMenutTargetold;				//time選択
	int nMenuStage, nMenuStageold;					//stage選択
	bool bConfirmation;								//ホーム初期化フラグ
	bool bPkRight, bPkLeft,bPkDown,bPkUp,bPkEnter;	//フラグ帯
	bool bHomeBGMSTART;
}HOME;

//***********************************************************
//	プロトタイプ宣言
void InitHome(HOME *aHome, TIME *aTimeCount, TEXT *aText, MAIN *aMain, SCORE *aScore);		//初期化
void UpdateHome(HOME *aHome, TIME *aTimeCount, TEXT *aText, MAIN *aMain);					//更新
void DrawHome(HOME *aHome, TIME *aTimeCount, TEXT *aText, MAIN *aMain, SCORE *aScore);		//描画
void UninitHome(void);																		//後処理
void HomeTimeLiset(TIME *aTimeCount);														//セレクトタイムリセット
void HomePk(HOME *aHome, TIME *aTimeCount, TEXT *aText, MAIN *aMain);						//キー帯
void HomeStageSelect(HOME *aHome, TEXT *aText, MAIN *aMain, SCORE *aScore);					//ステージセレクト
void HomeTimeSelect(HOME *aHome, TIME *aTimeCount, TEXT *aText, MAIN *aMain, SCORE *aScore);//タイムセレクト
void HomeScoreList(TEXT *aText, MAIN *aMain, SCORE *aScore);								//スコア表示
void HomeScoreConp(MAIN *aMain, SCORE *aScore);												//スコア比較
void HomeRecord(MAIN *aMain, SCORE *aScore);												//ログ
void RecordSave(SCORE *aScore);																//保存
void RecordRoad(SCORE *aScore);																//読み込み

#endif // !_HOME_H_