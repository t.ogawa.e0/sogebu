//***********************************************************
//	HEW作品制作
//	そげぶキラー[title.h]
//***********************************************************
#include"main.h"
#include"Home.h"

//***********************************************************
//	マクロ定義
#ifndef _title_H_				//二重インクルード防止のマクロ定義
#define _title_H_
#define title (1)				//タイトル構造数
#define TitleX_Position (60)	//タイトル基礎位置X
#define TitleY_Position (10)	//タイトル基礎位置Y
#define Interval (50)			//間隔

//***********************************************************
//構造体宣言
typedef struct
{
	bool bTitleDraw;	//描画終了判定
	bool bTitleComp;	//初期化判定
}TITLE;

//***********************************************************
//	プロトタイプ宣言
void IniTitle(TITLE*aTitle);								//初期化
void UpdateTitle(TITLE*aTitle, HOME *aHome, MAIN *aMain);	//更新
void DrawTitle(TITLE*aTitle);								//描画

#endif // !_title_H_
