//***********************************************************
//	HEW作品制作
//	そげぶキラー[player.h]
//***********************************************************
#include"main.h"
#include"world.h"
#include"AA.h"
#include"PlayerStructure.h"

//***********************************************************
//	マクロ定義
#ifndef _PLAYER_H_							//二重インクルード防止のマクロ定義
#define _PLAYER_H_
#define PlayerCase (1)						//プレイヤーケース
#define PlayerMove_X (0.5)					//左右への移動速度基準値
#define PlayerMove_Y (1)					//上下への移動速度基準値
#define PlayerJump_frame (14)				//ジャンプのフレーム数
#define PlayerMove_Jump (1.1)				//ジャンプ速度
#define PlayerMove_Jump_Attenuation (0.025)	//落下加速量

//***********************************************************
//	プロトタイプ宣言
void InitPlayer(PLAYER *aPlayer, WORLD *aWorld);					//プレイヤー初期処理
void UpdatePlayer(PLAYER *aPlayer, WORLD *aWorld, AA *aAA);			//プレイヤー更新処理
void DrawPlayer(PLAYER *aPlayer, AA *aAA, MAIN *aMain, SCORE *aScore);							//プレイヤー描画処理
void UninitPlayer(void);											//プレイヤー終了処理
void PlayerMove(PLAYER *aPlayer, AA *aAA);							//プレイヤー移動処理
void PlayerDown(PLAYER *aPlayer);									//プレイヤー降りる処理
void PlayerJump(PLAYER *aPlayer);									//プレイヤージャンプフラグ
void ReverseMove(PLAYER *aPlayer, WORLD *aWorld);					//プレイヤー戻す処理
void ProcessJump(PLAYER *aPlayer);									//ジャンプ処理
void GravityJudgment(PLAYER *aPlayer, WORLD *aWorld);				//重力処理判定
void ProcessJumpGravity(PLAYER *aPlayer, WORLD *aWorld, AA *aAA);	//重力処理,地面判定
void ProcessUpdate(PLAYER *aPlayer, WORLD *aWorld);					//処理内容引き継ぎ
void PlayerEffect(PLAYER *aPlayer, MAIN *aMain, SCORE *aScore);		//エフェクト処理

#endif // !_PLAYER_H_