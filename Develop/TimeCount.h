//***********************************************************
//	HEW作品制作
//	そげぶキラー[TimeCount.h]
//***********************************************************
#include"main.h"

//***********************************************************
//マクロ定義
#ifndef _TimeCount_H_	//二重インクルード防止のマクロ定義
#define _TimeCount_H_
#define TimeA (0)		//分
#define TimeB (1)		//秒
#define TimeC (2)		//コンマ
#define Time_right (0)	//一の位
#define Time_left (1)	//十の位
#define TimePozi (40)	//ポジション
#define TimeHeit (88)	//ポジション

//***********************************************************
//構造体定義
typedef struct
{
	float fTime[5][2];			//タイムケース
	bool bTime[5][2];			//タイムフラグケース
	bool bEndGame;				//ゲーム終了フラグ
	int nTimePozi;				//ポジション
	int nTimeHeit;				//ポジション
	int nTimeCount;				//時間のカウント
	int nTimeold[5][2];			//前回の時間
	int mTimePosition[5][2];	//各ポジション
	int nTime;					//描画のための変換
	bool nDrawStop;				//描画止め
}TIME;

//***********************************************************
//プロトタイプ宣言
void InitTime(TIME *aTimeCount);									//初期化
void UpdateTime(TIME *aTimeCount, MAIN *aMain, SCORE*aScore);		//更新
void DrawTime(TIME *aTimeCount);									//描画
void UninitTime(void);												//後処理
void TimeReverseCount(TIME *aTimeCount, MAIN *aMain, SCORE*aScore);	//タイマー
void TimeA_light(TIME *aTimeCount);									//分の10の位
void TimeA_left(TIME *aTimeCount);									//分の1の位
void TimeB_light(TIME *aTimeCount);									//秒の10の位
void TimeB_left(TIME *aTimeCount);									//秒の1の位
void TimeC_light(TIME *aTimeCount);									//コンマの10の位
void TimeC_left(TIME *aTimeCount);									//コンマの1の位

#endif // !_TimeCount_H_