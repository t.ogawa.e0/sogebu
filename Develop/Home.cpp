//***********************************************************
//	HEW作品制作
//	そげぶキラー[Home.cpp]
//***********************************************************
#include"main.h"
#include"Home.h"
#include"player.h"
#include"Score.h"

//***********************************************************
//初期化
void InitHome(HOME *aHome, TIME *aTimeCount, TEXT *aText, MAIN *aMain, SCORE *aScore)
{
	int nCount;

	//ホームの初期化
	if (aMain->bIniHome == true)
	{
		//値の初期化
		RecordRoad(&aScore[0]);
		aMain->nRecordID = 0;
		aMain->nMenutTarget = 0;
		aMain->nRecordAddress = 0;
		aText->n_TimeText = 0;
		aHome->nMenutTarget = 0;
		aHome->nMenutTargetold = 100;
		aHome->nMenuStage = 0;
		aHome->nMenuStageold = 100;
		aText->n_textold = 100;

		//各フラグの初期化
		aMain->bRog = true;
		aHome->bPkRight = true;
		aHome->bPkLeft = true;
		aHome->bPkDown = true;
		aHome->bPkUp = true;
		aHome->bPkEnter = true;
		aHome->bConfirmation = true;
		aText->bDrawConfi = true;
		aText->bDrawText = true;
		aText->bWindDelete = true;
		aTimeCount->nDrawStop = true;
		aMain->bHome = false;
		aMain->bNewRecord = true;
		aMain->bNEWScore = true;
	}

	//テキスト初期化
	Inittext(&aText[0]);

	//初期描画
	COLOR(BLACK, BLACK);
	for (nCount = MenuA_X; nCount <= MenuA_endX; nCount += 2)
	{
		LOCATE(nCount, MenuA_Y);
		printf("　");
		LOCATE(nCount, MenuA_endY);
		printf("　");
	}
	for (nCount = MenuA_Y; nCount <= MenuA_endY; nCount++)
	{
		LOCATE(MenuA_X, nCount);
		printf("　");
		LOCATE(MenuA_endX, nCount);
		printf("　");
	}
	if (aMain->bIniHome == true)
	{
		COLOR(BLACK, WHITE);
		aTimeCount->nTime = 0;
		HomeTimeLiset(&aTimeCount[0]);
		aTimeCount->nTimePozi = MenuA_TimeX;
		aTimeCount->nTimeHeit = MenuA_TimeY;
		aText->n_TimeTextX = MenuA_TimeX + 11;
		aText->n_TimeTextY = MenuA_TimeY - 20;
		DrawTime(&aTimeCount[0]);
		textTimeSelect(&aText[0]);
	}
	//end

	//初期描画
	COLOR(BLACK, BLACK);
	for (nCount = MenuB_X; nCount <= MenuB_endX; nCount += 2)
	{
		LOCATE(nCount, MenuB_Y);
		printf("　");
		LOCATE(nCount, MenuB_endY);
		printf("　");
	}
	for (nCount = MenuB_Y; nCount <= MenuB_endY; nCount++)
	{
		LOCATE(MenuB_X, nCount);
		printf("　");
		LOCATE(MenuB_endX, nCount);
		printf("　");
	}
	if (aMain->bIniHome == true)
	{
		COLOR(BLACK, WHITE);
		aTimeCount->nTime = 1;
		HomeTimeLiset(&aTimeCount[0]);
		aTimeCount->nTimePozi = MenuB_TimeX;
		aTimeCount->nTimeHeit = MenuB_TimeY;
		aText->n_TimeTextX = MenuB_TimeX + 11;
		aText->n_TimeTextY = MenuB_TimeY - 20;
		DrawTime(&aTimeCount[0]);
		COLOR(WHITE, WHITE);
		textTimeSelect(&aText[0]);
	}
	//end

	//初期描画
	COLOR(BLACK, BLACK);
	for (nCount = MenuC_X; nCount <= MenuC_endX; nCount += 2)
	{
		LOCATE(nCount, MenuC_Y);
		printf("　");
		LOCATE(nCount, MenuC_endY);
		printf("　");
	}
	for (nCount = MenuC_Y; nCount <= MenuC_endY; nCount++)
	{
		LOCATE(MenuC_X, nCount);
		printf("　");
		LOCATE(MenuC_endX, nCount);
		printf("　");
	}
	if (aMain->bIniHome == true)
	{
		COLOR(BLACK, WHITE);
		aTimeCount->nTime = 2;
		HomeTimeLiset(&aTimeCount[0]);
		aTimeCount->nTimePozi = MenuC_TimeX;
		aTimeCount->nTimeHeit = MenuC_TimeY;
		aText->n_TimeTextX = MenuC_TimeX + 11;
		aText->n_TimeTextY = MenuC_TimeY - 20;
		DrawTime(&aTimeCount[0]);
		COLOR(WHITE, WHITE);
		textTimeSelect(&aText[0]);
		aTimeCount->nTime = 0;
		aTimeCount->nTimePozi = 0;
		aTimeCount->nTimeHeit = 0;
		aText->n_TimeTextX = 0;
		aText->n_TimeTextY = 0;
		aMain->bIniHome = false;
	}
	//end

	//初期描画
	COLOR(BLACK, BLACK);
	for (nCount = Stage_A_X; nCount <= Stage_A_endX; nCount += 2)
	{
		LOCATE(nCount, Stage_A_Y);
		printf("　");
		LOCATE(nCount, Stage_A_endY);
		printf("　");
	}
	for (nCount = Stage_A_Y; nCount <= Stage_A_endY; nCount++)
	{
		LOCATE(Stage_A_X, nCount);
		printf("　");
		LOCATE(Stage_A_endX, nCount);
		printf("　");
	}
	aText->n_textX = (Stage_A_X + 10);
	aText->n_textY = (Stage_A_Y + 2);
	aText->n_text = 0;
	COLOR(BLACK, WHITE);
	textStage(&aText[0]);
	//end

	//初期描画
	COLOR(BLACK, BLACK);
	for (nCount = Stage_B_X; nCount <= Stage_B_endX; nCount += 2)
	{
		LOCATE(nCount, Stage_B_Y);
		printf("　");
		LOCATE(nCount, Stage_B_endY);
		printf("　");
	}
	for (nCount = Stage_B_Y; nCount <= Stage_B_endY; nCount++)
	{
		LOCATE(Stage_B_X, nCount);
		printf("　");
		LOCATE(Stage_B_endX, nCount);
		printf("　");
	}
	aText->n_textX = (Stage_B_X + 10);
	aText->n_textY = (Stage_B_Y + 2);
	aText->n_text = 1;
	COLOR(BLACK, WHITE);
	textStage(&aText[0]);
	//end

	//初期描画
	COLOR(BLACK, BLACK);
	for (nCount = Stage_C_X; nCount <= Stage_C_endX; nCount += 2)
	{
		LOCATE(nCount, Stage_C_Y);
		printf("　");
		LOCATE(nCount, Stage_C_endY);
		printf("　");
	}
	for (nCount = Stage_C_Y; nCount <= Stage_C_endY; nCount++)
	{
		LOCATE(Stage_C_X, nCount);
		printf("　");
		LOCATE(Stage_C_endX, nCount);
		printf("　");
	}
	aText->n_textX = (Stage_C_X + 10);
	aText->n_textY = (Stage_C_Y + 2);
	aText->n_text = 2;
	COLOR(BLACK, WHITE);
	textStage(&aText[0]);
	//end

	//初期描画
	COLOR(BLACK, BLACK);
	for (nCount = Stage_D_X; nCount <= Stage_D_endX; nCount += 2)
	{
		LOCATE(nCount, Stage_D_Y);
		printf("　");
		LOCATE(nCount, Stage_D_endY);
		printf("　");
	}
	for (nCount = Stage_D_Y; nCount <= Stage_D_endY; nCount++)
	{
		LOCATE(Stage_D_X, nCount);
		printf("　");
		LOCATE(Stage_D_endX, nCount);
		printf("　");
	}
	aText->n_textX = (Stage_D_X + 10);
	aText->n_textY = (Stage_D_Y + 2);
	aText->n_text = 3;
	COLOR(BLACK, WHITE);
	textStage(&aText[0]);
	aText->n_text = 0;
	//end

	//レコードの初期描画
	COLOR(BLACK, BLACK);
	for (nCount = Record_X; nCount <= Record_endX; nCount += 2)
	{
		LOCATE(nCount, Stage_A_Y);
		printf("　");
		LOCATE(nCount, Stage_B_endY);
		printf("　");
	}
	for (nCount = Stage_A_Y; nCount <= Stage_B_endY; nCount++)
	{
		LOCATE(Record_X, nCount);
		printf("　");
		LOCATE(Record_endX, nCount);
		printf("　");
	}
	COLOR(BLACK, BLACK);
	for (nCount = Record_X; nCount <= Record_endX; nCount += 2)
	{
		LOCATE(nCount, Record_Y);
		printf("　");
		LOCATE(nCount, Record_endY);
		printf("　");
	}
	for (nCount = Record_Y; nCount <= Record_endY; nCount++)
	{
		LOCATE(Record_X, nCount);
		printf("　");
		LOCATE(Record_endX, nCount);
		printf("　");
	}
	HomeScoreList(&aText[0], &aMain[0], &aScore[0]);
	//end

	//BGMフラグ
	aHome->bHomeBGMSTART = true;
}

//***********************************************************
//更新
void UpdateHome(HOME *aHome, TIME *aTimeCount, TEXT *aText, MAIN *aMain)
{
	//キー帯
	HomePk(&aHome[0], &aTimeCount[0], &aText[0], &aMain[0]);
}

//***********************************************************
//描画
void DrawHome(HOME *aHome, TIME *aTimeCount, TEXT *aText, MAIN *aMain, SCORE *aScore)
{
	//メニュー描画
	if (aHome->bConfirmation == true)
	{
		HomeStageSelect(&aHome[0], &aText[0], &aMain[0], &aScore[0]);
		HomeTimeSelect(&aHome[0], &aTimeCount[0], &aText[0], &aMain[0], &aScore[0]);
	}

	//確認ウィンドウ描画
	if (aHome->bConfirmation == false)
	{
		textWind(&aText[0]);
		aText->n_textX = 66;
		aText->n_textY = 34;
		aText->n_text = aHome->nMenuStage;
		COLOR(WHITE, BLACK);
		textStage(&aText[0]);
		aTimeCount->nTimePozi = 90;
		aTimeCount->nTimeHeit = 45;
		COLOR(WHITE, BLACK);
		HomeTimeLiset(&aTimeCount[0]);
		DrawTime(&aTimeCount[0]);
		aText->n_WindTimeX = aTimeCount->nTimePozi - 22;
		aText->n_WindTimeY = aTimeCount->nTimeHeit + 2;
		COLOR(WHITE, BLACK);
		textWintTime(&aText[0]);
	}

	//確認ウィンドウ消去
	if (aText->bWindDelete == false)
	{
		aTimeCount->nDrawStop = false;
		WindDelete(&aText[0]);
		InitHome(&aHome[0], &aTimeCount[0], &aText[0], &aMain[0], &aScore[0]);
		aHome->nMenuStageold = 100;
		aHome->nMenutTargetold = 100;
		aText->bWindDelete = true;
	}
}

//***********************************************************
//後処理
void UninitHome(void)
{}

//***********************************************************
//セレクトタイムリセット
void HomeTimeLiset(TIME *aTimeCount)
{
	aTimeCount->nTimeold[TimeA][Time_right] = 99;
	aTimeCount->nTimeold[TimeB][Time_left] = 99;
	aTimeCount->nTimeold[TimeB][Time_right] = 99;
	aTimeCount->nTimeold[TimeC][Time_left] = 99;
	aTimeCount->nTimeold[TimeC][Time_right] = 99;
	switch (aTimeCount->nTime)
	{
	case 0:
		aTimeCount->fTime[TimeA][Time_right] = 1;
		aTimeCount->fTime[TimeB][Time_left] = 0;
		aTimeCount->fTime[TimeB][Time_right] = 0;
		aTimeCount->fTime[TimeC][Time_left] = 0;
		aTimeCount->fTime[TimeC][Time_right] = 0;
		break;
	case 1:
		aTimeCount->fTime[TimeA][Time_right] = 1;
		aTimeCount->fTime[TimeB][Time_left] = 3;
		aTimeCount->fTime[TimeB][Time_right] = 0;
		aTimeCount->fTime[TimeC][Time_left] = 0;
		aTimeCount->fTime[TimeC][Time_right] = 0;
		break;
	case 2:
		aTimeCount->fTime[TimeA][Time_right] = 2;
		aTimeCount->fTime[TimeB][Time_left] = 0;
		aTimeCount->fTime[TimeB][Time_right] = 0;
		aTimeCount->fTime[TimeC][Time_left] = 0;
		aTimeCount->fTime[TimeC][Time_right] = 0;
		break;
	default:
		break;
	}
}

//***********************************************************
//キー帯
void HomePk(HOME *aHome, TIME *aTimeCount, TEXT *aText, MAIN *aMain)
{
	//ステージ選択
	if (aHome->bConfirmation == true)
	{
		//エンター押したら確認ウィンドウ
		if (INP(PK_ENTER))
		{
			if (aHome->bPkEnter == true)
			{
				if (!(aMain->bStartGame == false))
				{
					STOPWAVE(aHome->nKeySE);
					PLAYWAVE(aHome->nKeySE, 0);
					aHome->bConfirmation = false;
				}
			}
			aHome->bPkEnter = false;
		}
		else
		{
			aHome->bPkEnter = true;
		}

		//タイム選択
		if (INP(PK_RIGHT) || INP(PK_D))
		{
			if (aHome->bPkRight == true)
			{
				if (!(aHome->nMenutTarget == 2))
				{
					STOPWAVE(aHome->nKeySE);
					PLAYWAVE(aHome->nKeySE, 0);
					aHome->nMenutTarget++;
					aTimeCount->nTime = aHome->nMenutTarget;
					aMain->nMenutTarget = aHome->nMenutTarget;
				}
			}
			aHome->bPkRight = false;
		}
		else
		{
			aHome->bPkRight = true;
		}

		//タイム選択
		if (INP(PK_LEFT) || INP(PK_A))
		{
			if (aHome->bPkLeft == true)
			{
				if (!(aHome->nMenutTarget == 0))
				{
					STOPWAVE(aHome->nKeySE);
					PLAYWAVE(aHome->nKeySE, 0);
					aHome->nMenutTarget--;
					aTimeCount->nTime = aHome->nMenutTarget;
					aMain->nMenutTarget = aHome->nMenutTarget;
				}
			}
			aHome->bPkLeft = false;
		}
		else
		{
			aHome->bPkLeft = true;
		}

		//ステージ選択
		if (INP(PK_UP) || INP(PK_W))
		{
			if (aHome->bPkUp == true)
			{
				if (!(aHome->nMenuStage == 0))
				{
					STOPWAVE(aHome->nKeySE);
					PLAYWAVE(aHome->nKeySE, 0);
					aHome->nMenuStage--;
					aText->n_text = aHome->nMenuStage;
					aMain->nMenuStage = aHome->nMenuStage;
					aMain->nRecordAddress = aHome->nMenuStage;	//アドレス
				}
			}
			aHome->bPkUp = false;
		}
		else
		{
			aHome->bPkUp = true;
		}

		//ステージ選択
		if (INP(PK_DOWN) || INP(PK_S))
		{
			if (aHome->bPkDown == true)
			{
				if (!(aHome->nMenuStage == 3))
				{
					STOPWAVE(aHome->nKeySE);
					PLAYWAVE(aHome->nKeySE, 0);
					aHome->nMenuStage++;
					aText->n_text = aHome->nMenuStage;
					aMain->nMenuStage = aHome->nMenuStage;
					aMain->nRecordAddress = aHome->nMenuStage;	//アドレス
				}
			}
			aHome->bPkDown = false;
		}
		else
		{
			aHome->bPkDown = true;
		}
	}

	//確認ウィンドウ
	if (aHome->bConfirmation == false)
	{
		//エンターを押したときの処理
		if (INP(PK_ENTER))
		{
			if (aHome->bPkEnter == true)
			{
				if (!(aMain->bStartGame == false))
				{
					STOPWAVE(aHome->nKeySE);
					PLAYWAVE(aHome->nKeySE, 0);

					//YESが選択されたとき
					if (aText->nWindKey == 0)
					{
						aMain->bHome = true;
						aMain->bHomeLiset = true;
						aMain->bStartGame = false;
						aMain->bPlayGame = false;
						aMain->bLoad = true;
					}

					//NOが選択されたとき
					if (aText->nWindKey == 1)
					{
						aHome->bConfirmation = true;
						aText->bWindDelete = false;
						aText->bDrawConfi = true;
						aText->bDrawText = true;
						aText->nWindKey = 0;
					}
				}
			}
			aHome->bPkEnter = false;
		}
		else
		{
			aHome->bPkEnter = true;
		}

		//NO
		if (INP(PK_RIGHT) || INP(PK_D))
		{
			if (aHome->bPkRight == true)
			{
				STOPWAVE(aHome->nKeySE);
				PLAYWAVE(aHome->nKeySE, 0);
				aText->nWindKey = 1;
				aText->bDrawText = true;
			}
			aHome->bPkRight = false;
		}
		else
		{
			aHome->bPkRight = true;
		}

		//YES
		if (INP(PK_LEFT) || INP(PK_A))
		{
			if (aHome->bPkLeft == true)
			{
				STOPWAVE(aHome->nKeySE);
				PLAYWAVE(aHome->nKeySE, 0);
				aText->nWindKey = 0;
				aText->bDrawText = true;
			}
			aHome->bPkLeft = false;
		}
		else
		{
			aHome->bPkLeft = true;
		}
	}
}

//***********************************************************
//ステージセレクト
void HomeStageSelect(HOME *aHome, TEXT *aText, MAIN *aMain, SCORE *aScore)
{
	int nCount;

	//前回選択していた内用と一致していないときの再描画
	if (!(aHome->nMenuStageold == aHome->nMenuStage))
	{
		COLOR(BLACK, H_RED);
		//赤い枠
		switch (aHome->nMenuStage)
		{
		case 0:
			for (nCount = Stage_A_X; nCount <= Stage_A_endX; nCount += 2)
			{
				LOCATE(nCount, Stage_A_Y);
				printf("　");
				LOCATE(nCount, Stage_A_endY);
				printf("　");
			}
			for (nCount = Stage_A_Y; nCount <= Stage_A_endY; nCount++)
			{
				LOCATE(Stage_A_X, nCount);
				printf("　");
				LOCATE(Stage_A_endX, nCount);
				printf("　");
			}
			break;
		case 1:
			for (nCount = Stage_B_X; nCount <= Stage_B_endX; nCount += 2)
			{
				LOCATE(nCount, Stage_B_Y);
				printf("　");
				LOCATE(nCount, Stage_B_endY);
				printf("　");
			}
			for (nCount = Stage_B_Y; nCount <= Stage_B_endY; nCount++)
			{
				LOCATE(Stage_B_X, nCount);
				printf("　");
				LOCATE(Stage_B_endX, nCount);
				printf("　");
			}
			break;
		case 2:
			for (nCount = Stage_C_X; nCount <= Stage_C_endX; nCount += 2)
			{
				LOCATE(nCount, Stage_C_Y);
				printf("　");
				LOCATE(nCount, Stage_C_endY);
				printf("　");
			}
			for (nCount = Stage_C_Y; nCount <= Stage_C_endY; nCount++)
			{
				LOCATE(Stage_C_X, nCount);
				printf("　");
				LOCATE(Stage_C_endX, nCount);
				printf("　");
			}
			break;
		case 3:
			for (nCount = Stage_D_X; nCount <= Stage_D_endX; nCount += 2)
			{
				LOCATE(nCount, Stage_D_Y);
				printf("　");
				LOCATE(nCount, Stage_D_endY);
				printf("　");
			}
			for (nCount = Stage_D_Y; nCount <= Stage_D_endY; nCount++)
			{
				LOCATE(Stage_D_X, nCount);
				printf("　");
				LOCATE(Stage_D_endX, nCount);
				printf("　");
			}
			break;
		default:
			break;
		}

		COLOR(BLACK, BLACK);
		//黒い枠
		switch (aHome->nMenuStageold)
		{
		case 0:
			for (nCount = Stage_A_X; nCount <= Stage_A_endX; nCount += 2)
			{
				LOCATE(nCount, Stage_A_Y);
				printf("　");
				LOCATE(nCount, Stage_A_endY);
				printf("　");
			}
			for (nCount = Stage_A_Y; nCount <= Stage_A_endY; nCount++)
			{
				LOCATE(Stage_A_X, nCount);
				printf("　");
				LOCATE(Stage_A_endX, nCount);
				printf("　");
			}
			break;
		case 1:
			for (nCount = Stage_B_X; nCount <= Stage_B_endX; nCount += 2)
			{
				LOCATE(nCount, Stage_B_Y);
				printf("　");
				LOCATE(nCount, Stage_B_endY);
				printf("　");
			}
			for (nCount = Stage_B_Y; nCount <= Stage_B_endY; nCount++)
			{
				LOCATE(Stage_B_X, nCount);
				printf("　");
				LOCATE(Stage_B_endX, nCount);
				printf("　");
			}
			break;
		case 2:
			for (nCount = Stage_C_X; nCount <= Stage_C_endX; nCount += 2)
			{
				LOCATE(nCount, Stage_C_Y);
				printf("　");
				LOCATE(nCount, Stage_C_endY);
				printf("　");
			}
			for (nCount = Stage_C_Y; nCount <= Stage_C_endY; nCount++)
			{
				LOCATE(Stage_C_X, nCount);
				printf("　");
				LOCATE(Stage_C_endX, nCount);
				printf("　");
			}
			break;
		case 3:
			for (nCount = Stage_D_X; nCount <= Stage_D_endX; nCount += 2)
			{
				LOCATE(nCount, Stage_D_Y);
				printf("　");
				LOCATE(nCount, Stage_D_endY);
				printf("　");
			}
			for (nCount = Stage_D_Y; nCount <= Stage_D_endY; nCount++)
			{
				LOCATE(Stage_D_X, nCount);
				printf("　");
				LOCATE(Stage_D_endX, nCount);
				printf("　");
			}
			break;
		default:
			break;
		}
		HomeScoreList(&aText[0], &aMain[0], &aScore[0]);
		aHome->nMenuStageold = aHome->nMenuStage;
		aText->n_textold = aText->n_text;
	}
}

//***********************************************************
//タイムセレクト
void HomeTimeSelect(HOME *aHome, TIME *aTimeCount, TEXT *aText, MAIN *aMain, SCORE *aScore)
{
	int nCount;

	//前回選択していた内用と一致していないときの再描画
	if (!(aHome->nMenutTargetold == aHome->nMenutTarget))
	{
		COLOR(BLACK, H_RED);
		//現在の選択
		//赤い枠
		switch (aHome->nMenutTarget)
		{
		case 0:
			for (nCount = MenuA_X; nCount <= MenuA_endX; nCount += 2)
			{
				LOCATE(nCount, MenuA_Y);
				printf("　");
				LOCATE(nCount, MenuA_endY);
				printf("　");
			}
			for (nCount = MenuA_Y; nCount <= MenuA_endY; nCount++)
			{
				LOCATE(MenuA_X, nCount);
				printf("　");
				LOCATE(MenuA_endX, nCount);
				printf("　");
			}
			COLOR(BLACK, WHITE);
			HomeTimeLiset(&aTimeCount[0]);
			aTimeCount->nTimePozi = MenuA_TimeX;
			aTimeCount->nTimeHeit = MenuA_TimeY;
			aText->n_TimeTextX = MenuA_TimeX + 11;
			aText->n_TimeTextY = MenuA_TimeY - 20;
			textTimeSelect(&aText[0]);
			if (aTimeCount->nDrawStop == true)
			{
				DrawTime(&aTimeCount[0]);
			}
			aTimeCount->nDrawStop = true;
			break;
		case 1:
			for (nCount = MenuB_X; nCount <= MenuB_endX; nCount += 2)
			{
				LOCATE(nCount, MenuB_Y);
				printf("　");
				LOCATE(nCount, MenuB_endY);
				printf("　");
			}
			for (nCount = MenuB_Y; nCount <= MenuB_endY; nCount++)
			{
				LOCATE(MenuB_X, nCount);
				printf("　");
				LOCATE(MenuB_endX, nCount);
				printf("　");
			}
			COLOR(BLACK, WHITE);
			HomeTimeLiset(&aTimeCount[0]);
			aTimeCount->nTimePozi = MenuB_TimeX;
			aTimeCount->nTimeHeit = MenuB_TimeY;
			aText->n_TimeTextX = MenuB_TimeX + 11;
			aText->n_TimeTextY = MenuB_TimeY - 20;
			textTimeSelect(&aText[0]);
			if (aTimeCount->nDrawStop == true)
			{
				DrawTime(&aTimeCount[0]);
			}
			aTimeCount->nDrawStop = true;
			break;
		case 2:
			for (nCount = MenuC_X; nCount <= MenuC_endX; nCount += 2)
			{
				LOCATE(nCount, MenuC_Y);
				printf("　");
				LOCATE(nCount, MenuC_endY);
				printf("　");
			}
			for (nCount = MenuC_Y; nCount <= MenuC_endY; nCount++)
			{
				LOCATE(MenuC_X, nCount);
				printf("　");
				LOCATE(MenuC_endX, nCount);
				printf("　");
			}
			COLOR(BLACK, WHITE);
			HomeTimeLiset(&aTimeCount[0]);
			aTimeCount->nTimePozi = MenuC_TimeX;
			aTimeCount->nTimeHeit = MenuC_TimeY;
			aText->n_TimeTextX = MenuC_TimeX + 11;
			aText->n_TimeTextY = MenuC_TimeY - 20;
			textTimeSelect(&aText[0]);
			if (aTimeCount->nDrawStop == true)
			{
				DrawTime(&aTimeCount[0]);
			}
			aTimeCount->nDrawStop = true;
			break;
		default:
			break;
		}

		//前回の選択
		//黒い枠
		switch (aHome->nMenutTargetold)
		{
		case 0:
			COLOR(BLACK, BLACK);
			for (nCount = MenuA_X; nCount <= MenuA_endX; nCount += 2)
			{
				LOCATE(nCount, MenuA_Y);
				printf("　");
				LOCATE(nCount, MenuA_endY);
				printf("　");
			}
			for (nCount = MenuA_Y; nCount <= MenuA_endY; nCount++)
			{
				LOCATE(MenuA_X, nCount);
				printf("　");
				LOCATE(MenuA_endX, nCount);
				printf("　");
			}
			COLOR(WHITE, WHITE);
			aText->n_TimeTextX = MenuA_TimeX + 11;
			aText->n_TimeTextY = MenuA_TimeY - 20;
			textTimeSelect(&aText[0]);
			break;
		case 1:
			COLOR(BLACK, BLACK);
			for (nCount = MenuB_X; nCount <= MenuB_endX; nCount += 2)
			{
				LOCATE(nCount, MenuB_Y);
				printf("　");
				LOCATE(nCount, MenuB_endY);
				printf("　");
			}
			for (nCount = MenuB_Y; nCount <= MenuB_endY; nCount++)
			{
				LOCATE(MenuB_X, nCount);
				printf("　");
				LOCATE(MenuB_endX, nCount);
				printf("　");
			}
			COLOR(WHITE, WHITE);
			aText->n_TimeTextX = MenuB_TimeX + 11;
			aText->n_TimeTextY = MenuB_TimeY - 20;
			textTimeSelect(&aText[0]);
			break;
		case 2:
			COLOR(BLACK, BLACK);
			for (nCount = MenuC_X; nCount <= MenuC_endX; nCount += 2)
			{
				LOCATE(nCount, MenuC_Y);
				printf("　");
				LOCATE(nCount, MenuC_endY);
				printf("　");
			}
			for (nCount = MenuC_Y; nCount <= MenuC_endY; nCount++)
			{
				LOCATE(MenuC_X, nCount);
				printf("　");
				LOCATE(MenuC_endX, nCount);
				printf("　");
			}
			COLOR(WHITE, WHITE);
			aText->n_TimeTextX = MenuC_TimeX + 11;
			aText->n_TimeTextY = MenuC_TimeY - 20;
			textTimeSelect(&aText[0]);
			break;
		default:
			break;
		}
		HomeScoreList(&aText[0], &aMain[0], &aScore[0]);
		COLOR(BLACK, BLACK);
		aHome->nMenutTargetold = aHome->nMenutTarget;
	}
}

//***********************************************************
//スコア表示
void HomeScoreList(TEXT *aText, MAIN *aMain, SCORE *aScore)
{
	int nCount, nCountA;

	//初期描画
	aMain->bNewRecord = true;

	//描画座標
	aMain->nScoreX = Record_X + 57;
	aMain->nScoreY = Stage_B_Y + 2;
	aText->n_RecordX = aMain->nScoreX - 50;
	aText->n_RecordY = aMain->nScoreY - 12;
	aText->RecordType = 0;
	COLOR(H_CYAN, WHITE);
	textRecord(&aText[0]);
	COLOR(BLACK, WHITE);
	InitScore(&aMain[0], &aScore[0]);
	//0消し
	for (nCountA = 0; nCountA< 2; nCountA++)
	{
		if (nCountA == 0)
		{
			aMain->nScoreOld[0] = 99;
			aMain->nScoreOld[1] = 99;
			aMain->nScoreOld[2] = 99;
			COLOR(WHITE, WHITE);
		}
		if (nCountA == 1)
		{
			aMain->nScoreOld[0] = 99;
			aMain->nScoreOld[1] = 0;
			aMain->nScoreOld[2] = 0;
			COLOR(BLACK, WHITE);
		}
		Score(&aMain[0]);
	}
	aMain->bNewRecord = false;
	//end

	//ログケース
	aMain->nScoreY = Record_Y + 8;
	aText->n_BorderX = Record_X + 3;
	aText->n_BorderEndX = Record_endX - 1;
	aText->n_BorderY = Record_Y + 6;
	aText->n_RecordX = aMain->nScoreX - 50;
	aText->n_RecordY = aMain->nScoreY - 7;
	aText->RecordType = 1;
	COLOR(H_CYAN, WHITE);
	textRecord(&aText[0]);
	COLOR(BLACK, WHITE);
	for (nCount = 0; nCount < 5; nCount++)
	{
		aText->n_BorderY += 10;
		InitScore(&aMain[0], &aScore[0]);
		//0消し
		for (nCountA = 0; nCountA< 2; nCountA++)
		{
			if (nCountA == 0)
			{
				aMain->nScoreOld[0] = 99;
				aMain->nScoreOld[1] = 99;
				aMain->nScoreOld[2] = 99;
				COLOR(WHITE, WHITE);
			}
			if (nCountA == 1)
			{
				aMain->nScoreOld[0] = 99;
				aMain->nScoreOld[1] = 0;
				aMain->nScoreOld[2] = 0;
				COLOR(BLACK, WHITE);
			}
			Score(&aMain[0]);
		}
		if (!(nCount == 4))
		{
			textBorder(&aText[0]);
		}
		aMain->nScoreY += 10;
		aMain->nRecordID++;
	}
	aMain->nRecordID = 0;
	aMain->nScoreY = 0;
	aText->n_BorderX = 0;
	aText->n_BorderEndX = 0;
	aText->n_BorderY = 0;
	//end
}

//最新記録か否かの処理
void HomeScoreConp(MAIN *aMain, SCORE *aScore)
{
	int nCount;

	//現在格納されている最新記録とプレイ後の記録を比べる
	if ((aScore + aMain->nRecordAddress)->nNewRecord[aMain->nMenutTarget] < aMain->nPlayRecord)
	{
		//最新記録フラグ
		aMain->bNEWScore = false;

		//最新記録を書き換える
		(aScore + aMain->nRecordAddress)->nNewRecord[aMain->nMenutTarget] = aMain->nPlayRecord;

		//描画用記録を置き換える
		for (nCount = 0; nCount < 3; nCount++)
		{
			(aScore + aMain->nRecordAddress)->nIniNewRecord[aMain->nMenutTarget][nCount] = aMain->nPlayIniRecord[nCount];
		}
	}
	aMain->nPlayRecord = 0;
}

//ログ
void HomeRecord(MAIN *aMain, SCORE *aScore)
{
	int nCount;

	//ログ入れ替え
	if (aMain->bRog == false)
	{
		//プレイ後の記録を若い順に格納するため、古いものを一つずつ、ずらしてゆく
		for (nCount = 3; -1 < nCount; nCount -= 1)
		{
			(aScore + aMain->nRecordAddress)->nIniRecord[aMain->nMenutTarget][nCount + 1][0] = (aScore + aMain->nRecordAddress)->nIniRecord[aMain->nMenutTarget][nCount][0];
			(aScore + aMain->nRecordAddress)->nIniRecord[aMain->nMenutTarget][nCount + 1][1] = (aScore + aMain->nRecordAddress)->nIniRecord[aMain->nMenutTarget][nCount][1];
			(aScore + aMain->nRecordAddress)->nIniRecord[aMain->nMenutTarget][nCount + 1][2] = (aScore + aMain->nRecordAddress)->nIniRecord[aMain->nMenutTarget][nCount][2];
		}

		//新しいものを先頭に格納
		(aScore + aMain->nRecordAddress)->nIniRecord[aMain->nMenutTarget][0][0] = aMain->nPlayIniRecord[0];
		(aScore + aMain->nRecordAddress)->nIniRecord[aMain->nMenutTarget][0][1] = aMain->nPlayIniRecord[1];
		(aScore + aMain->nRecordAddress)->nIniRecord[aMain->nMenutTarget][0][2] = aMain->nPlayIniRecord[2];
	}
}

//保存
void RecordSave(SCORE *aScore)
{
	FILE *pFile;
	pFile = fopen(SAVEFAILE, "wb");
	if (pFile != NULL)
	{
		fwrite(&aScore[0], sizeof(SCORE), RecordCase, pFile);
		fclose(pFile);
	}
}

//読み込み
void RecordRoad(SCORE *aScore)
{
	FILE *pFile;
	//読み込み
	pFile = fopen(SAVEFAILE, "rb");
	if (pFile != NULL)
	{
		fread(&aScore[0], sizeof(SCORE), RecordCase, pFile);
		fclose(pFile);
	}
}