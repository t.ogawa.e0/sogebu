//***********************************************************
//	HEW作品制作
//	そげぶキラー[Explosion.cpp]
//***********************************************************
#include"main.h"
#include"Explosion.h"

//***********************************************************
//初期化
void InitExplosion(EXPLOSION *aExplosion)
{
	int nCount, nCountS;

	//フラグ初期化
	aExplosion->bExplosionPush = true;

	//射出可能弾数分初期化
	//マガジン数
	for (nCount = 0; nCount < BarrettCase; nCount++)
	{
		aExplosion->nExplosionCountEnd[nCount] = 0;
		aExplosion->nExplosionCountStart[nCount] = 0;

		//弾数
		for (nCountS = 0; nCountS < LimitBarrett; nCountS++)
		{
			aExplosion->fExplosion_X[nCount][nCountS] = 0;
			aExplosion->fExplosion_Y[nCount][nCountS] = 0;
			aExplosion->fExplosionEnd_X[nCount][nCountS] = 0;
			aExplosion->fExplosionEnd_Y[nCount][nCountS] = 0;
			aExplosion->ExplosionCount[nCount][nCountS] = 0;
		}
	}
}

//***********************************************************
//更新処理
void UpdateExplosion(BARRETT *aBarrett, EXPLOSION *aExplosion, PLAYER *aPlayer)
{
	int nCount, nsCount;

	//弾爆破開始
	if (aBarrett->bAnnihilation == false)
	{
		//マガジンループ
		for (nsCount = 0; nsCount < BarrettCase; nsCount++)
		{
			//各マガジンの変動する上限より処理終了の値が小さい間
			for (nCount = aExplosion->nExplosionCountStart[nsCount];nCount < aBarrett->nAnnihilationCountEnd[nsCount]; nCount++)
			{
				//各弾のアニメーションカウント
				aExplosion->ExplosionCount[nsCount][nCount] += 1;

				//各弾のアニメーションカウントが終了する値と一致しているとき
				if (aExplosion->ExplosionCount[nsCount][nCount] == 6)
				{
					//各弾のアニメーションカウントを初期化
					aExplosion->ExplosionCount[nsCount][nCount] = 0;

					//各マガジンの処理終了数の加算
					aExplosion->nExplosionCountStart[nsCount] ++;

					//射出された弾と処理が終了した弾の値が一致したとき
					if (aBarrett->nBarrettCountEnd[nsCount] == aBarrett->nBarrettCountStart[nsCount])
					{
						//各弾カウント初期化
						aBarrett->nBarrettCountStart[nsCount] = 0;
						aBarrett->nBarrettCountEnd[nsCount] = 0;
						aExplosion->nExplosionCountStart[nsCount] = 0;
						aBarrett->nAnnihilationCountEnd[nsCount] = 0;
					}
				}
			}
		}
	}
}

//***********************************************************
//描画処理
void DrawExplosion(BARRETT *aBarrett, EXPLOSION *aExplosion)
{
	int nCount, nsCount;

	//弾爆破開始
	if (aBarrett->bAnnihilation == false)
	{
		//マガジンループ
		for (nsCount = 0; nsCount < BarrettCase; nsCount++)
		{
			//各マガジンの変動する上限より処理終了の値が小さい間
			for (nCount = aExplosion->nExplosionCountStart[nsCount];nCount < aBarrett->nAnnihilationCountEnd[nsCount]; nCount++)
			{
				//各弾の座標
				LOCATE(aBarrett->fAnnihilation_X[nsCount][nCount], aBarrett->fAnnihilation_Y[nsCount][nCount]);
				COLOR(BLACK, WHITE);

				//弾を描画するか否か
				if (!(aBarrett->bbarrettAnnihilation[nsCount][nCount] == true))
				{
					//右方向
					if ((nsCount == 0) || (nsCount == 1))
					{
						switch (aExplosion->ExplosionCount[nsCount][nCount])
						{
						case 1:
							printf(".*");
							break;
						case 2:
							printf("'.");
							break;
						case 3:
							printf(" .");
							break;
						case 4:
							printf("  ");
							break;
						default:
							break;
						}
					}

					//左方向
					if ((nsCount == 2) || (nsCount == 3))
					{
						switch (aExplosion->ExplosionCount[nsCount][nCount])
						{
						case 1:
							printf("*.");
							break;
						case 2:
							printf(".'");
							break;
						case 3:
							printf(". ");
							break;
						case 4:
							printf("  ");
							break;
						default:
							break;
						}
					}
				}
			}
		}
	}

	//全て描画終了後
	if ((aBarrett->nAnnihilationCountEnd[0] == 0) && (aBarrett->nAnnihilationCountEnd[1] == 0) && (aBarrett->nAnnihilationCountEnd[2] == 0) && (aBarrett->nAnnihilationCountEnd[3] == 0))
	{
		//アニメーション終了
		aBarrett->bAnnihilation = true;
	}
}

//***********************************************************
//バレット爆発後処理
void UninitExplosion(void)
{}