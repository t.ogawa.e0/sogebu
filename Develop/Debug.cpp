//***********************************************************
//	デバッグ用
//	そげぶキラー[Debug.cpp]
//***********************************************************
#include"main.h"
#include"Debug.h"

//***********************************************************
//デバッグ初期化処理
void InitDebug(DEBUG *aDebug)
{
	aDebug[0].nDebugChange = 0;
	aDebug[0].push = true;
}

//***********************************************************
//デバッグ時出力処理
void Debug(MAIN *aMain, PLAYER *aPlayer, ENEMY *aEnemy, WORLD *aWorld, BARRETT *aBarrett, EXPLOSION *aExplosion, DEBUG *aDebug)
{
	int count;


	if (INP(PK_ENTER))
	{
		if (aDebug[0].push==true)
		{
			aDebug[0].nDebugChange++;
			aDebug[0].push = false;
		}
	}
	else
	{
		aDebug[0].push = true;
	}
	COLOR(BLACK, WHITE);
	switch (aDebug[0].nDebugChange)
	{
	case 1:
		count = 2;
		LOCATE(count, 2);
		printf("プレイヤー処理");
		LOCATE(count, 3);
		printf("           ");
		LOCATE(count, 3);
		printf("X:%g", aPlayer[Player_to_Use].fPosX);
		LOCATE(count, 4);
		printf("           ");
		LOCATE(count, 4);
		printf("Y:%g", aPlayer[Player_to_Use].fPosY);
		count = 10;
		LOCATE(count, 3);
		printf("                 ");
		LOCATE(count, 3);
		printf("Gravity:%g", aPlayer[Player_to_Use].fCount[PlayerCountGravity]);
		break;
	case 2:
		count = 2;
		LOCATE(count, 2);
		printf("エネミー処理");
		LOCATE(count, 3);
		printf("                   ");
		LOCATE(count, 3);
		printf("X:%g", (aEnemy + aMain->nAddress)->fPosX);
		LOCATE(count, 4);
		printf("                   ");
		LOCATE(count, 4);
		printf("Y:%g", (aEnemy + aMain->nAddress)->fPosY);
		count = 12;
		LOCATE(count, 3);
		printf("                   ");
		LOCATE(count, 3);
		printf("Gravity:%g", (aEnemy + aMain->nAddress)->fCount[PlayerCountGravity]);

		count = 26;
		LOCATE(count, 2);
		printf("                     ");
		LOCATE(count, 2);

		switch ((aEnemy + aMain->nAddress)->nEnemyPattern)
		{
		case EnemyMove:
			printf("パターン:動く");
			break;
		case EnemyStop:
			printf("パターン:停止");
			break;
		case EnemyJump:
			break;
			printf("パターン:ジャンプ");
		default:
			printf("パターン:なし");
			break;
		}
		LOCATE(count, 3);
		printf("                   ");
		LOCATE(count, 3);
		printf("フレーム[動き]:%2d<%2d", (aEnemy + aMain->nAddress)->nCount[EnemyMove], (aEnemy + aMain->nAddress)->nEnemyProcessFlame[EnemyMove]);
		printf("                   ");
		LOCATE(count, 4);
		printf("停止時間:%2d<%2d", (aEnemy + aMain->nAddress)->nCount[EnemyStop], (aEnemy + aMain->nAddress)->nEnemyProcessFlame[EnemyStop]);

		count = 47;
		LOCATE(count, 3);
		printf("                   ");
		LOCATE(count, 3);
		printf("確率:%3d<%3d>%3d", (aEnemy + aMain->nAddress)->Greater_than, (aEnemy + aMain->nAddress)->Decision, (aEnemy + aMain->nAddress)->Less_than);
		LOCATE(count, 4);
		break;
	case 3:
		//デバッグ用
		count = 2;
		LOCATE(count, 2);
		printf("バレット処理                                                  ");
		LOCATE(count, 3);
		printf("[0][処理上限:%2d],[処理中:%2d]      ", aBarrett[0].nBarrettCountEnd[0], aBarrett[0].nBarrettCountStart[0]);
		LOCATE(count, 4);
		printf("[1][処理上限:%2d],[処理中:%2d]      ", aBarrett[0].nBarrettCountEnd[1], aBarrett[0].nBarrettCountStart[1]);
		count = 30;
		LOCATE(count, 3);
		printf("[2][処理上限:%2d],[処理中:%2d]      ", aBarrett[0].nBarrettCountEnd[2], aBarrett[0].nBarrettCountStart[2]);
		LOCATE(count, 4);
		printf("[3][処理上限:%2d],[処理中:%2d]      ", aBarrett[0].nBarrettCountEnd[3], aBarrett[0].nBarrettCountStart[3]);
		//デバッグ用
		count = 60;
		LOCATE(count, 2);
		printf("弾着処理                     ");
		LOCATE(count, 3);
		printf("[0]%2d,%2d      ", aBarrett[0].nAnnihilationCountEnd[0], aExplosion[0].nExplosionCountStart[0]);
		LOCATE(count, 4);
		printf("[1]%2d,%2d      ", aBarrett[0].nAnnihilationCountEnd[1], aExplosion[0].nExplosionCountStart[1]);
		count = 70;
		LOCATE(count, 3);
		printf("[2]%2d,%2d      ", aBarrett[0].nAnnihilationCountEnd[2], aExplosion[0].nExplosionCountStart[2]);
		LOCATE(count, 4);
		printf("[3]%2d,%2d      ", aBarrett[0].nAnnihilationCountEnd[3], aExplosion[0].nExplosionCountStart[3]);
		break;
	case 9:
		LOCATE(2, 2);
		printf("                                      　　　　　         　　　　　　                     ");
		LOCATE(2, 3);
		printf("                                                      　　　　　　 　　　　　             ");
		LOCATE(2, 4);
		printf("                                                     　　　　　　　  　　　　             ");
		aDebug[0].nDebugChange = 0;
		break;
	default:
		break;
	}
}