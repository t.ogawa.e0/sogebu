//***********************************************************
//	HEW作品制作
//	そげぶキラー[music.h]
//***********************************************************

//***********************************************************
//	マクロ定義
#ifndef _MUSIC_H_																		//二重インクルード防止のマクロ定義
#define _MUSIC_H_

//　BGM
#define WORLDBGM "Deta/bgm/☆☆☆.wav"													//ステージのBGM
#define TitleMusic "Deta/bgm/Jubeat_knit_APPEND_Green_Green_Dance_(Long Version).wav"	//タイトルBGMリンク
#define ResultBGM "Deta/bgm/Resultmusic.wav"											//リザルトのBGM
#define LoadingSE "Deta/bgm/LoadingSE.wav"												//ロード開始SE
#define LoadingOpnSE "Deta/bgm/LoadingOpnSE.wav"										//ロード終了SE
#define HOMEBGM "Deta/bgm/Jubeat_knit_APPEND_Green_Green_Dance_(Long Version)HOME.wav"	//ホームBGM

//　SE
#define HomeKeySE "Deta/RPG_SE/RPG_SE[232].wav"											//メニューSE
#define ENEMYDEADSE "Deta/RPG_SE/RPG_SE[146].wav"										//エネミー消滅時SE
#define BarrettSE "Deta/RPG_SE/RPG_SE[495].wav"											//バレットのSE

typedef struct
{
	int nTitleBGM, nHomeBGM, nWorldBGM, nResultBGM;	//BGM
	int nLoadingSE, nLoadingOpnSE; //SE
}MUSIC;

#endif // !_MUSIC_H_