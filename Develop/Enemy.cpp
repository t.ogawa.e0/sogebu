//***********************************************************
//	HEW作品制作
//	そげぶキラー[Enemy.cpp]
//***********************************************************
#include"main.h"
#include"Enemy.h"

//***********************************************************
//エネミー初期処理
void InitEnemy(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld)
{
	srand(time(NULL));

	//空回し
	rand();rand();rand();rand();rand();

	//エネミー索敵範囲初期設定
	(aEnemy + aMain->nAddress)->fReconnaissance[ReconnaissanceLeftAndRight] = LeftAndRight;
	(aEnemy + aMain->nAddress)->fReconnaissance[ReconnaissanceUpAndDown] = UpAndDown;

	//エネミー初期位置
	(aEnemy + aMain->nAddress)->fPosX = aWorld->fEnemy_Start_Position[1][aMain->nAddress];
	(aEnemy + aMain->nAddress)->fPosY = aWorld->fEnemy_Start_Position[0][aMain->nAddress];
	(aEnemy + aMain->nAddress)->fPosXold = (aEnemy + aMain->nAddress)->fPosX;
	(aEnemy + aMain->nAddress)->fPosYold = (aEnemy + aMain->nAddress)->fPosY;

	//フラグ初期化
	(aEnemy + aMain->nAddress)->bScoreFlag = true;
	(aEnemy + aMain->nAddress)->bTump = true;
	(aEnemy + aMain->nAddress)->bPattern[PatternProcess] = true;
	(aEnemy + aMain->nAddress)->bPattern[EnemyMove] = true;
	(aEnemy + aMain->nAddress)->bPattern[EnemyDown] = true;
	(aEnemy + aMain->nAddress)->bPattern[EnemyStop] = true;
	(aEnemy + aMain->nAddress)->bPattern[EnemyJump] = true;
	(aEnemy + aMain->nAddress)->bProcess_Judgment[PlayerProcessJump] = true;
	(aEnemy + aMain->nAddress)->bPushDOWN = true;
	(aEnemy + aMain->nAddress)->bMove_Left_Right[Left] = true;
	(aEnemy + aMain->nAddress)->bMove_Left_Right[Right] = true;
	(aEnemy + aMain->nAddress)->bAnnihilationEffect = true;
	(aEnemy + aMain->nAddress)->bLipopEffect = true;
	(aEnemy + aMain->nAddress)->EffectEnd = false;
	(aEnemy + aMain->nAddress)->AAbldData = true;

	//エフェクトポジションの初期化
	(aEnemy + aMain->nAddress)->EffectPozisyonX = 0;
	(aEnemy + aMain->nAddress)->EffectPozisyonY = 0;

	//エフェクトカウンター
	(aEnemy + aMain->nAddress)->EffectCount = 0;

	//エネミー行動処理初期化
	(aEnemy + aMain->nAddress)->fCount[PlayerCountJump] = 0;
	(aEnemy + aMain->nAddress)->fCount[PlayerCountGravity] = 0;
	(aEnemy + aMain->nAddress)->fCount[EnemyMove] = 0;
	(aEnemy + aMain->nAddress)->nGravity = 0;

	//エネミー各行動上限カウント初期化
	(aEnemy + aMain->nAddress)->nEnemyProcessFlame[EnemyMove] = 0;
	(aEnemy + aMain->nAddress)->nEnemyProcessFlame[EnemyStop] = 0;
	(aEnemy + aMain->nAddress)->nEnemyProcessFlame[EnemyPursuit] = 0;

	//エネミーカウント各管理
	(aEnemy + aMain->nAddress)->nCount[EnemyMove] = 0;
	(aEnemy + aMain->nAddress)->nCount[EnemyStop] = 0;

	//確率初期化
	(aEnemy + aMain->nAddress)->Decision = 0;
	(aEnemy + aMain->nAddress)->Greater_than = 0;
	(aEnemy + aMain->nAddress)->Less_than = 0;

	//リポップ情報の初期化
	(aEnemy + aMain->nAddress)->Life = EnemyLife;
	(aEnemy + aMain->nAddress)->EnemyRecovery = 0;
	(aEnemy + aMain->nAddress)->RecoveryCount = 0;
	(aEnemy + aMain->nAddress)->LipopPozisyon = 0;

	//前回のAA格納
	(aEnemy + aMain->nAddress)->AAbld = 1;
}

//***********************************************************
//エネミー更新処理
void UpdateEnemy(MAIN *aMain, PLAYER *aPlayer, ENEMY *aEnemy, WORLD *aWorld, AA *aAA, SCORE*aScore)
{
	int nGravity, nCount;

	nCount = 0;
	nGravity = 0;

	//エネミーのライフが0以下のとき
	if ((aEnemy + aMain->nAddress)->Life <= 0)
	{
		EnemyRecoveryProcess(&aMain[0], &aEnemy[0], &aWorld[0]);
	}

	//エネミーのライフが0以下ではないとき
	if (!((aEnemy + aMain->nAddress)->Life <= 0))
	{
		rand();rand();rand();rand();rand();
		//前回の座標を保存
		ProcessUpdateEnemy(&aMain[0], &aEnemy[0], &aWorld[0]);

		//行動パターン
		Pattern(&aMain[0], &aEnemy[0], &aWorld[0]);

		//追跡モード、ここに書く事により強制的にフラグを元に戻します。
		PlayerPursuit(&aMain[0], &aPlayer[0], &aEnemy[0], &aWorld[0], &aScore[0]);

		//停止処理
		Stop(&aMain[0], &aEnemy[0], &aWorld[0]);

		//探索範囲判定
		Process_Left_Right(&aMain[0], &aEnemy[0], &aWorld[0]);

		//降りる処理
		Down(&aMain[0], &aEnemy[0]);

		//ジャンプ処理判定
		JumpProcess(&aMain[0], &aEnemy[0], &aWorld[0]);

		//ジャンプ処理
		Jump(&aMain[0], &aEnemy[0], &aWorld[0]);

		//右移動左移動
		Left_Right(&aMain[0], &aEnemy[0], &aAA[0]);

		//重力判定
		GravityProcess(&aMain[0], &aEnemy[0], &aWorld[0]);

		//重力処理
		Gravity(&aMain[0], &aEnemy[0], &aWorld[0], &aAA[0]);

		//エネミー戻す処理
		EnemyReverseMove(&aMain[0], &aEnemy[0], &aWorld[0]);

		EnemyAAlast(&aMain[0], &aEnemy[0], &aAA[0]);

		(aEnemy + aMain->nAddress)->RecoveryCount = 0;
	}
}

//***********************************************************
//エネミー描画処理
void DrawEnemy(MAIN *aMain, ENEMY *aEnemy, AA *aAA)
{
	//エネミー消滅時
	EnemyEffect(&aMain[0], &aEnemy[0]);

	//ライフが0以下の時
	if ((aEnemy + aMain->nAddress)->Life <= 0)
	{
		(aEnemy + aMain->nAddress)->fPosY = 290;
		(aEnemy + aMain->nAddress)->fPosX = 90;
	}

	//前回の位置と今の位置が一致していないとき塗りつぶし
	if (!(((aEnemy + aMain->nAddress)->fPosXold == (aEnemy + aMain->nAddress)->fPosX) && ((aEnemy + aMain->nAddress)->fPosYold == (aEnemy + aMain->nAddress)->fPosY)))
	{
		AAEnemyold(&aMain[0], &aEnemy[0], &aAA[0]);

		//ライフが0以下の時
		if ((aEnemy + aMain->nAddress)->Life <= 0)
		{
			(aEnemy + aMain->nAddress)->fPosXold = 290;
			(aEnemy + aMain->nAddress)->fPosYold = 90;
			aAA->EnemyAA[aMain->nAddress] = 99;
		}
	}

	//ライフが0以上の時
	if (!((aEnemy + aMain->nAddress)->Life <= 0))
	{
		AAEnemy(&aMain[0], &aEnemy[0], &aAA[0]);
	}
}

//***********************************************************
//エネミー後処理
void UninitEnemy(void)
{}

//***********************************************************
//限定的な初期化
void LimitedInit(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld)
{
	(aEnemy + aMain->nAddress)->nEnemyPattern = 0;
	(aEnemy + aMain->nAddress)->nLeftOrRight = 0;
	(aEnemy + aMain->nAddress)->nEnemyProcessFlame[EnemyMove] = 0;
	(aEnemy + aMain->nAddress)->nEnemyProcessFlame[EnemyStop] = 0;
	(aEnemy + aMain->nAddress)->bPattern[EnemyDown] = true;
	(aEnemy + aMain->nAddress)->bPattern[EnemyMove] = true;
	(aEnemy + aMain->nAddress)->bPattern[EnemyStop] = true;
	(aEnemy + aMain->nAddress)->bPattern[EnemyJump] = true;
	(aEnemy + aMain->nAddress)->bPattern[PatternProcess] = true;
	(aEnemy + aMain->nAddress)->bProcess_Judgment[PlayerProcessJump] = true;
	(aEnemy + aMain->nAddress)->nCount[EnemyMove] = 0;
	(aEnemy + aMain->nAddress)->nCount[EnemyStop] = 0;
	(aEnemy + aMain->nAddress)->nCount[EnemyJump] = 0;
	aWorld->nScaffoldCount = 0;
}

//***********************************************************
//行動パターン決定
void Pattern(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld)
{
	//エネミー索敵プログラム判定
	if ((aEnemy + aMain->nAddress)->bPattern[PatternProcess] == true)
	{
		(aEnemy + aMain->nAddress)->nEnemyPattern = rand() % EnemyPattern;

		//索敵プログラムフラグ密集帯
		switch ((aEnemy + aMain->nAddress)->nEnemyPattern)
		{
		case EnemyMove:
			//自動索敵判定
			(aEnemy + aMain->nAddress)->AAbldData = true;
			(aEnemy + aMain->nAddress)->bPattern[EnemyMove] = false;
			(aEnemy + aMain->nAddress)->bPattern[PatternProcess] = false;
			(aEnemy + aMain->nAddress)->nEnemyProcessFlame[EnemyMove] = rand() % EnemyMoveTime;
			(aEnemy + aMain->nAddress)->nLeftOrRight = rand() % MoveLeftOrRight;
			(aEnemy + aMain->nAddress)->Decision = rand() % LimitProbability;
			(aEnemy + aMain->nAddress)->Greater_than = (aEnemy + aMain->nAddress)->Decision - Probability;
			(aEnemy + aMain->nAddress)->Less_than = (aEnemy + aMain->nAddress)->Decision + Probability;
			(aEnemy + aMain->nAddress)->Decision = (LimitProbability - (rand() % LimitProbability));
			break;
		case EnemyStop:
			//静止判定
			(aEnemy + aMain->nAddress)->bPattern[EnemyStop] = false;
			(aEnemy + aMain->nAddress)->bPattern[PatternProcess] = false;
			(aEnemy + aMain->nAddress)->nEnemyProcessFlame[EnemyStop] = rand() % EnemyStopTime;
			break;
		case EnemyJump:
			//ジャンプ判定
			(aEnemy + aMain->nAddress)->nLeftOrRight = rand() % MoveLeftOrRight;
			(aEnemy + aMain->nAddress)->Decision = rand() % LimitProbability;
			(aEnemy + aMain->nAddress)->Greater_than = (aEnemy + aMain->nAddress)->Decision - Probability;
			(aEnemy + aMain->nAddress)->Less_than = (aEnemy + aMain->nAddress)->Decision + Probability;
			(aEnemy + aMain->nAddress)->Decision = rand() % LimitProbability;
			//ジャンプ処理発生確率20％
			if (((aEnemy + aMain->nAddress)->Greater_than <= (aEnemy + aMain->nAddress)->Decision) && ((aEnemy + aMain->nAddress)->Decision <= (aEnemy + aMain->nAddress)->Less_than))
			{
				(aEnemy + aMain->nAddress)->bPattern[EnemyJump] = false;
				(aEnemy + aMain->nAddress)->bPattern[PatternProcess] = false;
			}
			break;
		default:
			LimitedInit(&aMain[0], &aEnemy[0], &aWorld[0]);
			break;
		}
	}
}

//***********************************************************
//プレイヤーを追尾する処理
void PlayerPursuit(MAIN *aMain, PLAYER *aPlayer, ENEMY *aEnemy, WORLD *aWorld, SCORE*aScore)
{
	if (aPlayer->fPosY == aPlayer->fPosYold)
	{
		//エネミー視野、エネミーの各座標に索敵範囲として値を+-する
		//+-した値とプレイヤーのX,Y座標が一致していない場合、追跡してきません
		if ((aPlayer->fPosX <= ((aEnemy + aMain->nAddress)->fPosX + (aEnemy + aMain->nAddress)->fReconnaissance[ReconnaissanceLeftAndRight])) && (((aEnemy + aMain->nAddress)->fPosX - (aEnemy + aMain->nAddress)->fReconnaissance[ReconnaissanceLeftAndRight]) <= aPlayer->fPosX))
		{
			if ((aPlayer->fPosY <= (aEnemy + aMain->nAddress)->fPosY) && (((aEnemy + aMain->nAddress)->fPosY - (aEnemy + aMain->nAddress)->fReconnaissance[ReconnaissanceUpAndDown]) <= aPlayer->fPosY))
			{
				//初期化
				LimitedInit(&aMain[0], &aEnemy[0], &aWorld[0]);

				//追跡方向[左]
				if (aPlayer->fPosX < (aEnemy + aMain->nAddress)->fPosX)
				{
					(aEnemy + aMain->nAddress)->bMove_Left_Right[Right] = false;
				}

				//追跡方向[右]
				if ((aEnemy + aMain->nAddress)->fPosX < aPlayer->fPosX)
				{
					(aEnemy + aMain->nAddress)->bMove_Left_Right[Left] = false;
				}

				if ((((aEnemy + aMain->nAddress)->fPosX - 2) <= aPlayer->fPosX) && (aPlayer->fPosX <= ((aEnemy + aMain->nAddress)->fPosX + 2)))
				{
					if (aPlayer->EffectEnd == false)
					{
						aPlayer->Life -= EnemyAttak;
						DrawPlayerLifebar(&aMain[0], &aPlayer[0]);
					}
				}
			}
		}
	}
}

//***********************************************************
//停止処理
void Stop(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld)
{
	//停止処理
	if ((aEnemy + aMain->nAddress)->bPattern[EnemyStop] == false)
	{
		(aEnemy + aMain->nAddress)->nCount[EnemyStop] += 1;

		//終了処理
		if (!((aEnemy + aMain->nAddress)->nCount[EnemyStop] < (aEnemy + aMain->nAddress)->nEnemyProcessFlame[EnemyStop]))
		{
			LimitedInit(&aMain[0], &aEnemy[0], &aWorld[0]);
		}
	}
}

//***********************************************************
//降りる処理
void Down(MAIN *aMain, ENEMY *aEnemy)
{
	//現在地面の上
	if ((aEnemy + aMain->nAddress)->bTump == true)
	{
		if ((aEnemy + aMain->nAddress)->bPattern[EnemyDown] == false)
		{
			if ((aEnemy + aMain->nAddress)->bPushDOWN == true)
			{
				(aEnemy + aMain->nAddress)->fPosY += 1.1;
			}
			(aEnemy + aMain->nAddress)->bPushDOWN = false;
		}
		else
		{
			(aEnemy + aMain->nAddress)->bPushDOWN = true;
		}
	}
}

//***********************************************************
//ジャンプ処理判定
void JumpProcess(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld)
{
	int nCount = 0;

	//降りる処理[調整完了]&ジャンプ処理
	if ((aEnemy + aMain->nAddress)->bTump == true)
	{
		//エネミージャンプ
		if ((aEnemy + aMain->nAddress)->bPattern[EnemyJump] == false)
		{
			for (nCount = 0; nCount < aWorld->nScaffold; nCount++)
			{
				//エネミーと空中床のY座標一致時
				if ((aEnemy + aMain->nAddress)->fPosY == aWorld->fScaffold[nCount][2] - 1)
				{
					(aEnemy + aMain->nAddress)->nCount[EnemyJump] = nCount - 1;
					nCount = aWorld->nScaffold;
					(aEnemy + aMain->nAddress)->bProcess_Judgment[PlayerProcessJump] = false;
					(aEnemy + aMain->nAddress)->bPattern[EnemyJump] = true;
					if ((aEnemy + aMain->nAddress)->fPosY == aWorld->fScaffold[0][2] - 1)
					{
						LimitedInit(&aMain[0], &aEnemy[0], &aWorld[0]);
					}
				}
				else if ((aEnemy + aMain->nAddress)->fPosY == UnderWall - 1)
				{
					(aEnemy + aMain->nAddress)->nCount[EnemyJump] = aWorld->nScaffold - 1;
					nCount = aWorld->nScaffold;
					(aEnemy + aMain->nAddress)->bProcess_Judgment[PlayerProcessJump] = false;
					(aEnemy + aMain->nAddress)->bPattern[EnemyJump] = true;
				}
			}
		}
	}
}

//***********************************************************
//ジャンプ処理
void Jump(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld)
{
	//ジャンプ移動距離
	if ((aEnemy + aMain->nAddress)->bProcess_Judgment[PlayerProcessJump] == false)
	{
		if (((aEnemy + aMain->nAddress)->fPosX <= aWorld->fScaffold[(aEnemy + aMain->nAddress)->nCount[EnemyJump]][0] - JumpPosition) || (aWorld->fScaffold[(aEnemy + aMain->nAddress)->nCount[EnemyJump]][1] + JumpPosition <= (aEnemy + aMain->nAddress)->fPosX))
		{
			if ((aEnemy + aMain->nAddress)->fPosX <= aWorld->fScaffold[(aEnemy + aMain->nAddress)->nCount[EnemyJump]][0] - JumpPosition)
			{
				(aEnemy + aMain->nAddress)->bMove_Left_Right[Left] = false;
			}
			if (aWorld->fScaffold[(aEnemy + aMain->nAddress)->nCount[EnemyJump]][1] + JumpPosition <= (aEnemy + aMain->nAddress)->fPosX)
			{
				(aEnemy + aMain->nAddress)->bMove_Left_Right[Right] = false;
			}
		}

		//移動
		if ((aWorld->fScaffold[(aEnemy + aMain->nAddress)->nCount[EnemyJump]][0] - JumpPosition <= (aEnemy + aMain->nAddress)->fPosX) && ((aEnemy + aMain->nAddress)->fPosX <= aWorld->fScaffold[(aEnemy + aMain->nAddress)->nCount[EnemyJump]][1] + JumpPosition))
		{
			if ((aEnemy + aMain->nAddress)->fPosX <= aWorld->fScaffold[(aEnemy + aMain->nAddress)->nCount[EnemyJump]][0] + (((aWorld->fScaffold[(aEnemy + aMain->nAddress)->nCount[EnemyJump]][1] - aWorld->fScaffold[(aEnemy + aMain->nAddress)->nCount[EnemyJump]][0]) / 2) + 0.5))
			{
				(aEnemy + aMain->nAddress)->bMove_Left_Right[Left] = false;
			}
			if (aWorld->fScaffold[(aEnemy + aMain->nAddress)->nCount[EnemyJump]][0] + (((aWorld->fScaffold[(aEnemy + aMain->nAddress)->nCount[EnemyJump]][1] - aWorld->fScaffold[(aEnemy + aMain->nAddress)->nCount[EnemyJump]][0]) / 2) + 0.5) <= (aEnemy + aMain->nAddress)->fPosX)
			{
				(aEnemy + aMain->nAddress)->bMove_Left_Right[Right] = false;
			}
			//天井接触時判定
			if ((aEnemy + aMain->nAddress)->fPosY - 1 <= TopWall + 1)
			{
				(aEnemy + aMain->nAddress)->fPosY = TopWall + 2;
				(aEnemy + aMain->nAddress)->fCount[PlayerCountJump] += EnemyJump_frame;
			}

			//ジャンプ処理
			if ((aEnemy + aMain->nAddress)->fCount[PlayerCountJump] < EnemyJump_frame)
			{
				(aEnemy + aMain->nAddress)->fPosY -= EnemyMove_Jump;
				(aEnemy + aMain->nAddress)->fCount[PlayerCountJump] += 1;
			}
			else
			{
				(aEnemy + aMain->nAddress)->fCount[PlayerCountGravity] = 0;
				(aEnemy + aMain->nAddress)->fCount[PlayerCountJump] = 0;
				LimitedInit(&aMain[0], &aEnemy[0], &aWorld[0]);
			}
		}
	}
}

//***********************************************************
//重力判定
void GravityProcess(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld)
{
	int nCount;

	//重力処理判定[最下床]
	if (!((aEnemy + aMain->nAddress)->fPosY == UnderWall - 1))
	{
		(aEnemy + aMain->nAddress)->nGravity = 1;
	}

	//重力処理判定[空中床]
	for (nCount = 0; nCount < aWorld->nScaffold; nCount++)
	{
		if ((aWorld->fScaffold[nCount][0] - 1.5 <= (aEnemy + aMain->nAddress)->fPosX) && ((aEnemy + aMain->nAddress)->fPosX <= aWorld->fScaffold[nCount][1]))
		{
			if ((aEnemy + aMain->nAddress)->fPosY <= aWorld->fScaffold[nCount][2])
			{
				aWorld->nScaffoldCount = nCount;
				nCount = aWorld->nScaffold;
				(aEnemy + aMain->nAddress)->nGravity = 2;
			}
		}
	}
}

//***********************************************************
//重力処理
void Gravity(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld, AA *aAA)
{
	switch ((aEnemy + aMain->nAddress)->nGravity)
	{
	case 1:
		//重力処理[最下床]
		if (UnderWall - 1 <= (aEnemy + aMain->nAddress)->fPosY)
		{
			(aEnemy + aMain->nAddress)->fPosY = UnderWall - 1;
			(aEnemy + aMain->nAddress)->fCount[PlayerCountGravity] = 0;
			(aEnemy + aMain->nAddress)->bTump = true;
		}
		else
		{
			(aEnemy + aMain->nAddress)->bTump = false;
			aAA->EnemyAA[aMain->nAddress] = 5;
			(aEnemy + aMain->nAddress)->fPosY += ((aEnemy + aMain->nAddress)->fCount[PlayerCountGravity] += EnemyMove_Jump_Attenuation);
		}
		break;
	case 2:
		//重力処理[空中床]
		if (aWorld->fScaffold[aWorld->nScaffoldCount][2] - 1 <= (aEnemy + aMain->nAddress)->fPosY)
		{
			(aEnemy + aMain->nAddress)->fPosY = aWorld->fScaffold[aWorld->nScaffoldCount][2] - 1;
			(aEnemy + aMain->nAddress)->fCount[PlayerCountGravity] = 0;
			(aEnemy + aMain->nAddress)->bTump = true;
		}
		else
		{
			(aEnemy + aMain->nAddress)->bTump = false;
			aAA->EnemyAA[aMain->nAddress] = 5;
			(aEnemy + aMain->nAddress)->fPosY += ((aEnemy + aMain->nAddress)->fCount[PlayerCountGravity] += EnemyMove_Jump_Attenuation);
		}
		break;
	default:
		break;
	}
}

//***********************************************************
//探索範囲判定
void Process_Left_Right(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld)
{
	int nCount = 0;

	//エネミー索敵処理,右左移動
	if ((aEnemy + aMain->nAddress)->bPattern[EnemyMove] == false)
	{
		switch ((aEnemy + aMain->nAddress)->nLeftOrRight)
		{
		case 1:
			//地上時
			if ((aEnemy + aMain->nAddress)->fPosY == UnderWall - 1)
			{
				if (LeftWall_X <= ((aEnemy + aMain->nAddress)->fPosX - Approach_Distance_Wall))
				{
					(aEnemy + aMain->nAddress)->bMove_Left_Right[Right] = false;
					(aEnemy + aMain->nAddress)->nCount[EnemyMove] += 1;
				}
				else
				{
					//索敵限界位置接近時切り替え
					(aEnemy + aMain->nAddress)->nLeftOrRight = 2;
				}
			}
			else
			{
				//空中床時
				for (nCount = 0; nCount < aWorld->nScaffold; nCount++)
				{
					if ((aEnemy + aMain->nAddress)->fPosY == aWorld->fScaffold[nCount][2] - 1)
					{
						if (aWorld->fScaffold[nCount][0] < ((aEnemy + aMain->nAddress)->fPosX - Approach_Distance_Wall))
						{
							(aEnemy + aMain->nAddress)->bMove_Left_Right[Right] = false;
							(aEnemy + aMain->nAddress)->nCount[EnemyMove] += 1;
						}
						else
						{
							//索敵限界位置接近時切り替え
							(aEnemy + aMain->nAddress)->nLeftOrRight = 2;
						}
						nCount = aWorld->nScaffold;
					}
				}
			}
			break;
		case 2:
			//地上時
			if ((aEnemy + aMain->nAddress)->fPosY == UnderWall - 1)
			{
				if (((aEnemy + aMain->nAddress)->fPosX + Approach_Distance_Wall) <= RightWall_X - 8.5)
				{
					(aEnemy + aMain->nAddress)->bMove_Left_Right[Left] = false;
					(aEnemy + aMain->nAddress)->nCount[EnemyMove] += 1;
				}
				else
				{
					//索敵限界位置接近時切り替え
					(aEnemy + aMain->nAddress)->nLeftOrRight = 1;
				}
			}
			else
			{
				//空中床時
				for (nCount = 0; nCount < aWorld->nScaffold; nCount++)
				{
					if ((aEnemy + aMain->nAddress)->fPosY == aWorld->fScaffold[nCount][2] - 1)
					{
						if (((aEnemy + aMain->nAddress)->fPosX + Approach_Distance_Wall) < aWorld->fScaffold[nCount][1] - 6.5)
						{
							(aEnemy + aMain->nAddress)->bMove_Left_Right[Left] = false;
							(aEnemy + aMain->nAddress)->nCount[EnemyMove] += 1;
						}
						else
						{
							//索敵限界位置接近時切り替え
							(aEnemy + aMain->nAddress)->nLeftOrRight = 1;
						}
						nCount = aWorld->nScaffold;
					}
				}
			}
			break;
		default:
			//可能性は低いですが関係のない値が出た場合の終了処理
			(aEnemy + aMain->nAddress)->nCount[EnemyMove] = (aEnemy + aMain->nAddress)->nEnemyProcessFlame[EnemyMove];
			break;
		}

		//20％の確率で降りる
		if (((aEnemy + aMain->nAddress)->Greater_than <= (aEnemy + aMain->nAddress)->Decision) && ((aEnemy + aMain->nAddress)->Decision <= (aEnemy + aMain->nAddress)->Less_than))
		{
			//エネミー動作フレーム数を半分にした値と一致時フラグ処理
			if (!((aEnemy + aMain->nAddress)->nCount[EnemyMove] < (((aEnemy + aMain->nAddress)->nEnemyProcessFlame[EnemyMove] / 2) + 0.5)))
			{
				(aEnemy + aMain->nAddress)->bPattern[EnemyDown] = false;
			}
		}

		//終了処理
		if (!((aEnemy + aMain->nAddress)->nCount[EnemyMove] < (aEnemy + aMain->nAddress)->nEnemyProcessFlame[EnemyMove]))
		{
			LimitedInit(&aMain[0], &aEnemy[0], &aWorld[0]);
		}
	}
}

//***********************************************************
//右移動左移動
void Left_Right(MAIN *aMain, ENEMY *aEnemy, AA *aAA)
{
	//右移動処理
	if ((aEnemy + aMain->nAddress)->bMove_Left_Right[Left] == false)
	{
		(aEnemy + aMain->nAddress)->fPosX += EnemyMove_X;
		//AA分岐
		if (((aEnemy + aMain->nAddress)->AAbld == 1) || ((aEnemy + aMain->nAddress)->AAbld == 3))
		{
			aAA->EnemyAA[aMain->nAddress] = 2;
		}
		if (((aEnemy + aMain->nAddress)->AAbld == 2) || ((aEnemy + aMain->nAddress)->AAbld == 4))
		{
			aAA->EnemyAA[aMain->nAddress] = 1;
		}
		aAA->EnemyMoveoldAA[aMain->nAddress] = aAA->EnemyAA[aMain->nAddress];
		aAA->EnemyAAold[aMain->nAddress] = aAA->EnemyAA[aMain->nAddress];
		(aEnemy + aMain->nAddress)->bMove_Left_Right[Left] = true;
		if ((aEnemy + aMain->nAddress)->AAbldData == true)
		{
			(aEnemy + aMain->nAddress)->AAbld = aAA->EnemyAA[aMain->nAddress];
			(aEnemy + aMain->nAddress)->AAbldData = false;
		}
	}

	//左移動処理
	if ((aEnemy + aMain->nAddress)->bMove_Left_Right[Right] == false)
	{
		(aEnemy + aMain->nAddress)->fPosX -= EnemyMove_X;
		//AA分岐
		if (((aEnemy + aMain->nAddress)->AAbld == 1) || ((aEnemy + aMain->nAddress)->AAbld == 3))
		{
			aAA->EnemyAA[aMain->nAddress] = 4;
		}
		if (((aEnemy + aMain->nAddress)->AAbld == 2) || ((aEnemy + aMain->nAddress)->AAbld == 4))
		{
			aAA->EnemyAA[aMain->nAddress] = 3;
		}
		aAA->EnemyMoveoldAA[aMain->nAddress] = aAA->EnemyAA[aMain->nAddress];
		aAA->EnemyAAold[aMain->nAddress] = aAA->EnemyAA[aMain->nAddress];
		(aEnemy + aMain->nAddress)->bMove_Left_Right[Right] = true;
		if ((aEnemy + aMain->nAddress)->AAbldData == true)
		{
			(aEnemy + aMain->nAddress)->AAbld = aAA->EnemyAA[aMain->nAddress];
			(aEnemy + aMain->nAddress)->AAbldData = false;
		}
	}
}

//***********************************************************
//エネミー戻す処理
void EnemyReverseMove(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld)
{
	//戻す処理[制作完了]
	if ((aEnemy + aMain->nAddress)->fPosX >= (RightWall_X - 8.5))
	{
		(aEnemy + aMain->nAddress)->fPosX = RightWall_X - 8.5;
	}

	//戻す処理[制作完了]
	if ((aEnemy + aMain->nAddress)->fPosX <= RightWall_Y + 1)
	{
		(aEnemy + aMain->nAddress)->fPosX = RightWall_Y + 1.5;
	}
}

//***********************************************************
//前回の座標を保存
void ProcessUpdateEnemy(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld)
{
	(aEnemy + aMain->nAddress)->EffectPozisyonX = (aEnemy + aMain->nAddress)->fPosXold;
	(aEnemy + aMain->nAddress)->EffectPozisyonY = (aEnemy + aMain->nAddress)->fPosYold;
	(aEnemy + aMain->nAddress)->fPosXold = (aEnemy + aMain->nAddress)->fPosX;
	(aEnemy + aMain->nAddress)->fPosYold = (aEnemy + aMain->nAddress)->fPosY;
}

//***********************************************************
//エネミー復帰処理
void EnemyRecoveryProcess(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld)
{
	int Lipop, LipopA, nScaffold, nScaffoldA;

	Lipop = 0;
	LipopA = 0;
	nScaffold = 0;
	nScaffoldA = 0;

	if ((aEnemy + aMain->nAddress)->bScoreFlag == true)
	{
		//比較用レコードに加算
		aMain->nPlayRecord++;

		//描画用スコア加算
		aMain->nScore[0] += 1;
		(aEnemy + aMain->nAddress)->bScoreFlag = false;
		(aEnemy + aMain->nAddress)->EffectEnd = true;
	}

	if ((aEnemy + aMain->nAddress)->EnemyRecovery == (aEnemy + aMain->nAddress)->RecoveryCount)
	{
		LimitedInit(&aMain[0], &aEnemy[0], &aWorld[0]);

		//再度HPを与える
		(aEnemy + aMain->nAddress)->Life = EnemyLife;

		//座標データをintに入れる
		nScaffold = aWorld->fScaffold[(aEnemy + aMain->nAddress)->LipopPozisyon][1];
		nScaffoldA = aWorld->fScaffold[(aEnemy + aMain->nAddress)->LipopPozisyon][0];
		while (!(((aWorld->fScaffold[(aEnemy + aMain->nAddress)->LipopPozisyon][0] + 20)<Lipop) && (Lipop<(aWorld->fScaffold[(aEnemy + aMain->nAddress)->LipopPozisyon][1] - 20))))
		{
			//空回し
			rand();rand();rand();rand();rand();

			//リポップするX座標決め
			Lipop = rand() % nScaffold;

			//空回し
			rand();rand();rand();rand();rand();

			//調整用座標
			LipopA = rand() % nScaffoldA;

			if (Lipop<(aWorld->fScaffold[(aEnemy + aMain->nAddress)->LipopPozisyon][0] + 20))
			{
				Lipop = Lipop + LipopA;
			}

			if ((aWorld->fScaffold[(aEnemy + aMain->nAddress)->LipopPozisyon][1] - 20)<Lipop)
			{
				Lipop = Lipop - LipopA;
			}
		}
		//値を代入
		(aEnemy + aMain->nAddress)->fPosX = Lipop;
		(aEnemy + aMain->nAddress)->fPosY = (aWorld->fScaffold[(aEnemy + aMain->nAddress)->LipopPozisyon][2] - 1);
		(aEnemy + aMain->nAddress)->RecoveryCount = 0;
		(aEnemy + aMain->nAddress)->bScoreFlag = true;
		(aEnemy + aMain->nAddress)->bAnnihilationEffect = true;
	}
	//蘇生タイマー
	(aEnemy + aMain->nAddress)->RecoveryCount++;
}

//***********************************************************
//エフェクト処理
void EnemyEffect(MAIN *aMain, ENEMY *aEnemy)
{
	if ((aEnemy + aMain->nAddress)->EffectEnd == true)
	{
		if ((aEnemy + aMain->nAddress)->bAnnihilationEffect == true)
		{
			(aEnemy + aMain->nAddress)->EffectCount = 0;
			(aEnemy + aMain->nAddress)->bAnnihilationEffect = false;
		}

		//消滅時エフェクト処理
		if ((aEnemy + aMain->nAddress)->bAnnihilationEffect == false)
		{
			(aEnemy + aMain->nAddress)->EffectCount++;
			switch ((aEnemy + aMain->nAddress)->EffectCount)
			{
			case 1:
				STOPWAVE(aEnemy->EnemyDeadSE);
				PLAYWAVE(aEnemy->EnemyDeadSE, 0);
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 2, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("▲");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 2, (aEnemy + aMain->nAddress)->EffectPozisyonY);
				printf("△");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("×");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX, (aEnemy + aMain->nAddress)->EffectPozisyonY);
				printf("□");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 2, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("○");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 2, (aEnemy + aMain->nAddress)->EffectPozisyonY);
				printf("●");
				break;
			case 2:
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 4, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("●");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 4, (aEnemy + aMain->nAddress)->EffectPozisyonY);
				printf("×");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 2, (aEnemy + aMain->nAddress)->EffectPozisyonY - 2);
				printf("▽");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 2, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("□");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 2, (aEnemy + aMain->nAddress)->EffectPozisyonY);
				printf("▲");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX, (aEnemy + aMain->nAddress)->EffectPozisyonY - 2);
				printf("×");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("○");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX, (aEnemy + aMain->nAddress)->EffectPozisyonY);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 2, (aEnemy + aMain->nAddress)->EffectPozisyonY - 2);
				printf("▲");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 2, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("×");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 2, (aEnemy + aMain->nAddress)->EffectPozisyonY);
				printf("■");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 4, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("△");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 4, (aEnemy + aMain->nAddress)->EffectPozisyonY);
				printf("○");
				break;
			case 3:
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 4, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("□");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 4, (aEnemy + aMain->nAddress)->EffectPozisyonY);
				printf("△");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 2, (aEnemy + aMain->nAddress)->EffectPozisyonY - 2);
				printf("▽");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 2, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 2, (aEnemy + aMain->nAddress)->EffectPozisyonY);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX, (aEnemy + aMain->nAddress)->EffectPozisyonY - 2);
				printf("▲");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 2, (aEnemy + aMain->nAddress)->EffectPozisyonY - 2);
				printf("○");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 2, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 2, (aEnemy + aMain->nAddress)->EffectPozisyonY);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 4, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("×");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 4, (aEnemy + aMain->nAddress)->EffectPozisyonY);
				printf("■");
				break;
			case 4:
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 5, (aEnemy + aMain->nAddress)->EffectPozisyonY - 2);
				printf("□");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 5, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("×");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 3, (aEnemy + aMain->nAddress)->EffectPozisyonY - 3);
				printf("●");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX, (aEnemy + aMain->nAddress)->EffectPozisyonY - 3);
				printf("▽");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 3, (aEnemy + aMain->nAddress)->EffectPozisyonY - 3);
				printf("■");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 5, (aEnemy + aMain->nAddress)->EffectPozisyonY - 2);
				printf("▲");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 5, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("■");

				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 4, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 4, (aEnemy + aMain->nAddress)->EffectPozisyonY);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 2, (aEnemy + aMain->nAddress)->EffectPozisyonY - 2);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX, (aEnemy + aMain->nAddress)->EffectPozisyonY - 2);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 2, (aEnemy + aMain->nAddress)->EffectPozisyonY - 2);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 4, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 4, (aEnemy + aMain->nAddress)->EffectPozisyonY);
				printf("　");
				break;
			case 5:
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 6, (aEnemy + aMain->nAddress)->EffectPozisyonY - 3);
				printf("■");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 6, (aEnemy + aMain->nAddress)->EffectPozisyonY - 2);
				printf("▲");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 4, (aEnemy + aMain->nAddress)->EffectPozisyonY - 4);
				printf("●");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX, (aEnemy + aMain->nAddress)->EffectPozisyonY - 4);
				printf("×");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 4, (aEnemy + aMain->nAddress)->EffectPozisyonY - 4);
				printf("▽");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 6, (aEnemy + aMain->nAddress)->EffectPozisyonY - 3);
				printf("□");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 6, (aEnemy + aMain->nAddress)->EffectPozisyonY - 2);
				printf("○");

				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 5, (aEnemy + aMain->nAddress)->EffectPozisyonY - 2);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 5, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 3, (aEnemy + aMain->nAddress)->EffectPozisyonY - 3);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX, (aEnemy + aMain->nAddress)->EffectPozisyonY - 3);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 3, (aEnemy + aMain->nAddress)->EffectPozisyonY - 3);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 5, (aEnemy + aMain->nAddress)->EffectPozisyonY - 2);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 5, (aEnemy + aMain->nAddress)->EffectPozisyonY - 1);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 5, (aEnemy + aMain->nAddress)->EffectPozisyonY);
				printf("　");
				break;
			case 6:
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 6, (aEnemy + aMain->nAddress)->EffectPozisyonY - 3);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 6, (aEnemy + aMain->nAddress)->EffectPozisyonY - 2);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX - 4, (aEnemy + aMain->nAddress)->EffectPozisyonY - 4);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX, (aEnemy + aMain->nAddress)->EffectPozisyonY - 4);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 4, (aEnemy + aMain->nAddress)->EffectPozisyonY - 4);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 6, (aEnemy + aMain->nAddress)->EffectPozisyonY - 3);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 6, (aEnemy + aMain->nAddress)->EffectPozisyonY - 2);
				printf("　");
				LOCATE((aEnemy + aMain->nAddress)->EffectPozisyonX + 6, (aEnemy + aMain->nAddress)->EffectPozisyonY);
				printf("　");
				break;
			case 7:
				(aEnemy + aMain->nAddress)->EffectEnd = false;
				(aEnemy + aMain->nAddress)->EffectCount = 0;
				(aEnemy + aMain->nAddress)->EffectPozisyonX = 0;
				(aEnemy + aMain->nAddress)->EffectPozisyonY = 0;
				break;
			default:
				break;
			}
		}
	}
}