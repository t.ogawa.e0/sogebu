//***********************************************************
//	HEWìi§ì
//	»°ÔL[[Lifebar.cpp]
//***********************************************************
#include"main.h"
#include"Lifebar.h"

//***********************************************************
//Gl~[Cto[`æ
void DrawEnemyLifebar(MAIN *aMain, ENEMY *aEnemy)
{
	COLOR(SKYBLUE);
	LOCATE((aEnemy + aMain->nAddress)->fPosX - 1, (aEnemy + aMain->nAddress)->fPosY - 3);
	printf("[        ]");
	COLOR(GREEN);
	LOCATE((aEnemy + aMain->nAddress)->fPosX, (aEnemy + aMain->nAddress)->fPosY - 3);

	//cèCtÊÉ¶ÄÏ®
	switch ((aEnemy + aMain->nAddress)->Life)
	{
	case (int(EnemyLife * 0)):
		printf(" ");
		break;
	case (int(EnemyLife * 0.1)):
		printf("-");
		break;
	case (int(EnemyLife * 0.3)):
		printf("--");
		break;
	case (int(EnemyLife * 0.4)):
		printf("---");
		break;
	case (int(EnemyLife * 0.5)):
		printf("----");
		break;
	case (int(EnemyLife * 0.6)):
		printf("-----");
		break;
	case (int(EnemyLife * 0.7)):
		printf("------");
		break;
	case (int(EnemyLife * 0.9)):
		printf("-------");
		break;
	case (int(EnemyLife * 1)):
		printf("--------");
		break;
	default:
		break;
	}
}

//***********************************************************
//vC[Cto[`æ
void DrawPlayerLifebar(MAIN *aMain, PLAYER *aPlayer)
{
	int nCount = 88;
	int nCountP = 0;

	COLOR(L_RED, WHITE);

	//lÏ®ÉtOð§Äé
	switch (aPlayer->Life)
	{
	case (int(PlayerLife * 0)):
		aPlayer->bLifeIni = false;
		break;
	case (int(PlayerLife * 0.1)):
		aPlayer->bLifeIni = false;
		break;
	case (int(PlayerLife * 0.2)):
		aPlayer->bLifeIni = false;
		break;
	case (int(PlayerLife * 0.3)):
		aPlayer->bLifeIni = false;
		break;
	case (int(PlayerLife * 0.4)):
		aPlayer->bLifeIni = false;
		break;
	case (int(PlayerLife * 0.5)):
		aPlayer->bLifeIni = false;
		break;
	case (int(PlayerLife * 0.6)):
		aPlayer->bLifeIni = false;
		break;
	case (int(PlayerLife * 0.7)):
		aPlayer->bLifeIni = false;
		break;
	case (int(PlayerLife * 0.8)):
		aPlayer->bLifeIni = false;
		break;
	case (int(PlayerLife * 0.9)):
		aPlayer->bLifeIni = false;
		break;
	case (int(PlayerLife * 1)):
		break;
	default:
		break;
	}

	//ÔQ[W`æJn
	if (aPlayer->bLifeIni == false)
	{
		switch (aPlayer->Life)
		{
		case (int(PlayerLife * 0)):
			nCountP = 0;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			aPlayer->bLifeIni = true;
			break;
		case (int(PlayerLife * 0.1)):
			nCountP = 8;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			aPlayer->bLifeIni = true;
			break;
		case (int(PlayerLife * 0.2)):
			nCountP = 16;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			aPlayer->bLifeIni = true;
			break;
		case (int(PlayerLife * 0.3)):
			nCountP = 24;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			aPlayer->bLifeIni = true;
			break;
		case (int(PlayerLife * 0.4)):
			nCountP = 32;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			aPlayer->bLifeIni = true;
			break;
		case (int(PlayerLife * 0.5)):
			nCountP = 40;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			aPlayer->bLifeIni = true;
			break;
		case (int(PlayerLife * 0.6)):
			nCountP = 48;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			aPlayer->bLifeIni = true;
			break;
		case (int(PlayerLife * 0.7)):
			nCountP = 56;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			aPlayer->bLifeIni = true;
			break;
		case (int(PlayerLife * 0.8)):
			nCountP = 64;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			aPlayer->bLifeIni = true;
			break;
		case (int(PlayerLife * 0.9)):
			nCountP = 72;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			nCount++;
			LOCATE(PlayerLifeBar + nCountP, nCount);
			printf("    ");
			aPlayer->bLifeIni = true;
			break;
		case (int(PlayerLife * 1)):
			break;
		default:
			break;
		}
	}
}