//***********************************************************
//	HEW作品制作
//	そげぶキラー[result.h]
//***********************************************************
#include"main.h"
#include"text.h"
#include"TimeCount.h"
#include"Home.h"
#include"Score.h"

//***********************************************************
//	マクロ定義
#ifndef _result_H_			//二重インクルード防止のマクロ定義
#define _result_H_
#define _RESULT_ (1)		//リザルト構造数
#define RESULTX (60)		//リザルト画面表示位置
#define RESULTendX (240)	//リザルト画面表示位置
#define RESULTY (19)		//リザルト画面表示位置
#define RESULTendY (76)		//リザルト画面表示位置
#define RESULTLIMITCOUNT (5)//リザルト処理タイム

//***********************************************************
//構造体宣言
typedef struct
{
	int nResultCount;							//リザルトフレーム数カウント
	int nResultScore[3];						//リザルト用スコア格納領域
	bool bResultWind;							//リザルト用ウィンドウ描画フラグ
	bool bResultIniScore, bResultIniScoreEnd;	//スコア処理後描画フラグ
	bool bResultStage, bResultStageEnd;			//ステージ描画フラグ
	bool bResultTime, bResultTimeEnd;			//タイム描画フラグ
	bool bResultScore, bResultScoreEnd;			//スコア描画フラグ
	bool bResultEnter, bResultEnterEnd;			//エンターキー発動フラグ
	bool bResultComp;							//リザルト初期化フラグ
}RESULT;

//***********************************************************
//	プロトタイプ宣言
void IniResult(RESULT *aResult);															//初期処理
void UpdateResult(RESULT *aResult, HOME *aHome, MAIN *aMain);								//行進処理
void DrawResult(RESULT *aResult, TEXT *aText, TIME *aTimeCount, MAIN *aMain, SCORE *aScore);//描画処理

#endif // !_result_H_