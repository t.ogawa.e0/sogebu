//***********************************************************
//	HEW作品制作
//	そげぶキラー[world.h]
//***********************************************************
#include"main.h"
#include"PlayerStructure.h"
#include"EnemyStructure.h"
#include"Home.h"
#include"text.h"

//***********************************************************
//	マクロ定義
#ifndef _world_H_					//二重インクルード防止のマクロ定義
#define _world_H_
#define Worldcase (1)				//ステージ構造数
#define Worldcase_to_Use (0)		//床座標初期位置
#define LeftWall_X (1)				//左の壁 X軸基準値
#define LeftWall_Y (1)				//左の壁 Y軸基準値
#define RightWall_X (300)			//右の壁 X軸基準値
#define RightWall_Y (1)				//右の壁 Y軸基準値
#define TopWall (1)					//上壁座標基準値
#define UnderWall (85)				//下壁座標基準値
#define Scaffold_or_Number (20)		//足場の上限
#define MoveSpeed_or_Scaffold (0.25)//足場の動く速度
#define Move_left_light_Scaffold (0)//左右移動
#define Move_up_down_Scaffold (1)	//上下移動
#define Update_Player (5)			//更新幅
#define Update_Enemy (8)			//更新幅
#define ReloadDraw (10)				//更新幅
#define PlayerReloadright (7.5)		//更新幅
#define PlayerReloadleft (2)		//更新幅

//***********************************************************
//構造体宣言
typedef struct
{
	int nScaffoldCount;										//カウント
	int nMoveScaffold;										//動く床の番地[左右]
	int nMoveScaffold_Up_Down;								//上か下か
	int nScaffold;											//空中床の現在の数
	float fPlayer_Start_PositionX, fPlayer_Start_PositionY;	//プレイヤーのスタートポジション
	float fEnemy_Start_Position[2][11];						//エネミーの各スタートポジション
	float fScaffold[Scaffold_or_Number][3];					//足場の数[]、座標[X]始まり,座標[X]終わり、高さ[Y]
	float fScaffoldLast[Scaffold_or_Number][3];				//足場の数[]、座標[X]始まり,座標[X]終わり、高さ[Y]
	bool bWorldBGMSTART;
}WORLD;

//***********************************************************
//	プロトタイプ宣言
void InitWorld(MAIN *aMain, WORLD *aWorld, HOME *aHome, TEXT *aText);		//ワールド初期処理
void UpdateWorld(WORLD *aWorld, PLAYER *aPlayer);							//ワールド更新処理
void DrawWorld(MAIN *aMain, WORLD *aWorld, PLAYER *aPlayer, ENEMY *aEnemy);	//ワールド描画処理
void UninitWorld(void);									//ワールド後処理
void StageA(WORLD *aWorld, HOME *aHome);				//ステージ1
void StageB(WORLD *aWorld, HOME *aHome);				//ステージ2
void StageC(WORLD *aWorld, HOME *aHome);				//ステージ3
void StageD(WORLD *aWorld, HOME *aHome);				//ステージ4

#endif // !_world_H_