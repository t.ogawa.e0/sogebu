//***********************************************************
//	HEW作品制作
//	そげぶキラー[Barrett.cpp]
//***********************************************************
#include"main.h"
#include"Barrett.h"
#include"Lifebar.h"

//***********************************************************
//バレット初期化
void InitBarrett(BARRETT *aBarrett)
{
	int nCount, nCountS;

	srand(time(NULL));

	//フラグ初期化
	aBarrett->bBarrettPush = true;
	aBarrett->bBarrettEnd = true;
	aBarrett->bAnnihilation = true;

	//値初期化
	aBarrett->BarrettCount = 0;
	aBarrett->nBarrettDamage = BarrettDamage;

	//マガジン
	for (nCount = 0; nCount < BarrettCase; nCount++)
	{
		aBarrett->nBarrettCountEnd[nCount] = 0;
		aBarrett->nBarrettCountStart[nCount] = 0;
		aBarrett->nAnnihilationCountEnd[nCount] = 0;

		//弾数
		for (nCountS = 0; nCountS < LimitBarrett; nCountS++)
		{
			aBarrett->bBarrettJudgment[nCount][nCountS] = true;
			aBarrett->bbarrettAnnihilation[nCount][nCountS] = true;
			aBarrett->fBarrett_X[nCount][nCountS] = 0;
			aBarrett->fBarrett_Y[nCount][nCountS] = 0;
			aBarrett->fBarrettEnd_X[nCount][nCountS] = 0;
			aBarrett->fBarrettEnd_Y[nCount][nCountS] = 0;
			aBarrett->fAnnihilation_X[nCount][nCountS] = 0;
			aBarrett->fAnnihilation_Y[nCount][nCountS] = 0;
		}
	}
}

//***********************************************************
//バレット更新処理
void UpdateBarrett(MAIN *aMain, BARRETT *aBarrett, PLAYER *aPlayer, AA *aAA, ENEMY *aEnemy, WORLD *aWorld)
{
	int nCount, nsCount;

	//右に向いているとき
	if ((INP(PK_RIGHT)) || (INP(PK_D)))
	{
		if ((aBarrett->BarrettCount == 0) || (aBarrett->BarrettCount == 2))
		{
			aBarrett->BarrettCount = 0;
		}
		if ((aBarrett->BarrettCount == 1) || (aBarrett->BarrettCount == 3))
		{
			aBarrett->BarrettCount = 1;
		}
	}

	//左に向いているとき
	if (INP(PK_LEFT) || INP(PK_A)) {
		if ((aBarrett->BarrettCount == 0) || (aBarrett->BarrettCount == 2))
		{
			aBarrett->BarrettCount = 2;
		}
		if ((aBarrett->BarrettCount == 1) || (aBarrett->BarrettCount == 3))
		{
			aBarrett->BarrettCount = 3;
		}
	}

	//ガンキー
	if ((aPlayer->bTump == true) && (aPlayer->fPosX == aPlayer->fPosXold))
	{
		if (INP(PK_SP))
		{
			STOPWAVE(aBarrett->nBarrettSE);
			PLAYWAVE(aBarrett->nBarrettSE, 0);
			if (aBarrett->bBarrettPush == true)
			{
				if ((aAA->PlayerlastAA == 2) || (aAA->PlayerlastAA == 0))
				{
					aBarrett->fBarrett_X[aBarrett->BarrettCount][aBarrett->nBarrettCountEnd[aBarrett->BarrettCount]] = aPlayer->fPosX + 20;
				}
				if ((aAA->PlayerlastAA == 3) || (aAA->PlayerlastAA == 1))
				{
					aBarrett->fBarrett_X[aBarrett->BarrettCount][aBarrett->nBarrettCountEnd[aBarrett->BarrettCount]] = aPlayer->fPosX - 13;
				}
				aBarrett->fBarrett_Y[aBarrett->BarrettCount][aBarrett->nBarrettCountEnd[aBarrett->BarrettCount]] = aPlayer->fPosY - 1;
				aBarrett->bBarrettJudgment[aBarrett->BarrettCount][aBarrett->nBarrettCountEnd[aBarrett->BarrettCount]] = false;
				aBarrett->bbarrettAnnihilation[aBarrett->BarrettCount][aBarrett->nBarrettCountEnd[aBarrett->BarrettCount]] = false;
				aBarrett->nBarrettCountEnd[aBarrett->BarrettCount]++;
				aBarrett->bBarrettPush = false;
				aBarrett->bBarrettEnd = false;
			}
		}
		else
		{
			aBarrett->bBarrettPush = true;
		}
	}

	//弾の処理
	if (aBarrett->bBarrettEnd == false)
	{
		for (nsCount = 0; nsCount < BarrettCase; nsCount++)
		{
			for (nCount = aBarrett->nBarrettCountStart[nsCount];nCount < aBarrett->nBarrettCountEnd[nsCount]; nCount++)
			{
				aBarrett->fBarrettEnd_X[nsCount][nCount] = aBarrett->fBarrett_X[nsCount][nCount];
				aBarrett->fBarrettEnd_Y[nsCount][nCount] = aBarrett->fBarrett_Y[nsCount][nCount];
				if ((nsCount == 0) || (nsCount == 1))
				{
					aBarrett->fBarrett_X[nsCount][nCount] += BarrettSpeed;
				}
				if ((nsCount == 2) || (nsCount == 3))
				{
					aBarrett->fBarrett_X[nsCount][nCount] -= BarrettSpeed;
				}

				//エネミーと接触時
				while (!(aMain->nAddress == aMain->nEnemyCount))
				{
					//エネミーの座標[Y]に変化が無いとき
					if ((aEnemy + aMain->nAddress)->fPosYold == (aEnemy + aMain->nAddress)->fPosY)
					{
						//座標[Y]参照
						if (aBarrett->fBarrett_Y[nsCount][nCount] == (aEnemy + aMain->nAddress)->fPosY - 1)
						{
							//座標[X]参照
							if (((aBarrett->fBarrett_X[nsCount][nCount] - 6) <= (aEnemy + aMain->nAddress)->fPosX) && ((aEnemy + aMain->nAddress)->fPosX) <= (aBarrett->fBarrett_X[nsCount][nCount] + 1))
							{
								//当たり判定が生きているとき
								if (aBarrett->bBarrettJudgment[nsCount][nCount] == false)
								{
									//エネミーのHPが0以下じゃないとき
									if (!((aEnemy + aMain->nAddress)->Life <= 0))
									{
										//空回し
										rand();rand();rand();rand();rand();

										//復帰時間決め
										(aEnemy + aMain->nAddress)->EnemyRecovery = rand() % RecoveryTime;
										(aEnemy + aMain->nAddress)->EnemyRecovery += 100;

										//空回し
										rand();rand();rand();rand();rand();

										//復帰する床座標を与える処理
										(aEnemy + aMain->nAddress)->LipopPozisyon = rand() % aWorld->nScaffold;

										//エネミーへダメージを与える
										(aEnemy + aMain->nAddress)->Life = (aEnemy + aMain->nAddress)->Life - aBarrett->nBarrettDamage;

										//各フラグ
										aBarrett->bBarrettJudgment[nsCount][nCount] = true;
										aBarrett->bbarrettAnnihilation[nsCount][nCount] = true;
									}
								}
							}
						}
					}
					aMain->nAddress++;
				}
				aMain->nAddress = 0;

				if (((RightWall_X - 1) <= aBarrett->fBarrett_X[nsCount][nCount]) || (aBarrett->fBarrett_X[nsCount][nCount] < (LeftWall_X + 1)))
				{
					aBarrett->bAnnihilation = false;
					aBarrett->fAnnihilation_X[nsCount][nCount] = aBarrett->fBarrett_X[nsCount][nCount];
					aBarrett->fAnnihilation_Y[nsCount][nCount] = aBarrett->fBarrett_Y[nsCount][nCount];
					aBarrett->bBarrettJudgment[nsCount][nCount] = true;
					aBarrett->nAnnihilationCountEnd[nsCount]++;
					if ((nsCount == 0) || (nsCount == 1))
					{
						aBarrett->fAnnihilation_X[nsCount][nCount] -= 1;
					}
					if ((nsCount == 2) || (nsCount == 3))
					{
						aBarrett->fAnnihilation_X[nsCount][nCount] += 1;
					}
					aBarrett->fBarrett_X[nsCount][nCount] = 0;
					aBarrett->fBarrett_Y[nsCount][nCount] = 0;
					aBarrett->nBarrettCountStart[nsCount]++;
				}
			}
		}
	}

	//バレット処理終了
	if ((aBarrett->nBarrettCountEnd[0] == 0) && (aBarrett->nBarrettCountEnd[1] == 0) && (aBarrett->nBarrettCountEnd[2] == 0) && (aBarrett->nBarrettCountEnd[3] == 0))
	{
		aBarrett->bBarrettEnd = true;
	}

	//各マガジン上限到達時の切り替え
	if (aBarrett->nBarrettCountEnd[aBarrett->BarrettCount] == LimitBarrett)
	{
		if (aBarrett->BarrettCount == 0)
		{
			aBarrett->BarrettCount = 1;
		}
		else if (aBarrett->BarrettCount == 1)
		{
			aBarrett->BarrettCount = 0;
		}
		if (aBarrett->BarrettCount == 2)
		{
			aBarrett->BarrettCount = 3;
		}
		else if (aBarrett->BarrettCount == 3)
		{
			aBarrett->BarrettCount = 2;
		}
	}
}

//***********************************************************
//バレット描画処理
void DrawBarrett(BARRETT *aBarrett)
{
	int nCount, nsCount;

	//処理フラグが発生時
	if (aBarrett->bBarrettEnd == false)
	{
		//マガジンループ
		for (nsCount = 0; nsCount < BarrettCase; nsCount++)
		{
			//変動する処理上限より処理終了数が少ないとき
			for (nCount = aBarrett->nBarrettCountStart[nsCount];nCount < aBarrett->nBarrettCountEnd[nsCount]; nCount++)
			{
				LOCATE(aBarrett->fBarrettEnd_X[nsCount][nCount], aBarrett->fBarrettEnd_Y[nsCount][nCount]);
				COLOR(WHITE);
				printf("―");
				if (!(aBarrett->bbarrettAnnihilation[nsCount][nCount] == true))
				{
					LOCATE(aBarrett->fBarrett_X[nsCount][nCount], aBarrett->fBarrett_Y[nsCount][nCount]);
					COLOR(BLACK);
					printf("―");
				}
			}
		}
	}
}

//***********************************************************
//バレット後処理
void UninitBarrett(void)
{}