//***********************************************************
//	HEW作品制作
//	そげぶキラー[Enemy.h]
//***********************************************************
#include"main.h"
#include"world.h"
#include"AA.h"
#include"PlayerStructure.h"
#include"EnemyStructure.h"
#include"Score.h"

//***********************************************************
//	マクロ定義
#ifndef _Enemy_H_							//二重インクルード防止のマクロ定義
#define _Enemy_H_
#define EnemyCase (11)						//エネミーケース
#define EnemyMove_X (0.325)					//左右への移動速度基準値
#define EnemyMove_Y (1)						//上下への移動速度基準値
#define EnemyJump_frame (14)				//ジャンプのフレーム数
#define EnemyMove_Jump (1.1)				//ジャンプ速度
#define JumpPosition (4.5)					//探索時ジャンプするポジション
#define EnemyMove_Jump_Attenuation (0.025)	//落下加速量

//***********************************************************
//	プロトタイプ宣言
void InitEnemy(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld);								//プレイヤー初期処理
void UpdateEnemy(MAIN *aMain, PLAYER *aPlayer, ENEMY *aEnemy, WORLD *aWorld, AA *aAA, SCORE*aScore);	//プレイヤー更新処理
void DrawEnemy(MAIN *aMain, ENEMY *aEnemy, AA *aAA);									//プレイヤー描画処理
void UninitEnemy(void);																	//プレイヤー終了処理
void PlayerPursuit(MAIN *aMain, PLAYER *aPlayer, ENEMY *aEnemy, WORLD *aWorld, SCORE*aScore);			//プレイヤー追尾
void LimitedInit(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld);							//限定的な初期化
void Left_Right(MAIN *aMain, ENEMY *aEnemy, AA *aAA);									//左右動作
void Pattern(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld);								//行動パターン
void Stop(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld);									//停止処理
void Down(MAIN *aMain, ENEMY *aEnemy);													//降りる処理
void JumpProcess(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld);							//ジャンプ処理判定
void Jump(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld);									//ジャンプ処理
void GravityProcess(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld);							//重力判定
void Gravity(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld, AA *aAA);						//重力処理
void Process_Left_Right(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld);						//探索範囲判定
void EnemyReverseMove(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld);						//エネミー戻す処理
void ProcessUpdateEnemy(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld);						//前回の座標を保存
void EnemyRecoveryProcess(MAIN *aMain, ENEMY *aEnemy, WORLD *aWorld);					//エネミー復帰処理
void EnemyEffect(MAIN *aMain, ENEMY *aEnemy);											//エフェクト処理

#endif // !_Enemy_H_