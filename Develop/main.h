//***********************************************************
//	HEW作品制作
//	そげぶキラー[main.h]
//***********************************************************
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include"CScreen.h"

//***********************************************************
//	マクロ定義
#ifndef _MAIN_H_		//二重インクルード防止のマクロ定義
#define _MAIN_H_
#define Stage_Case (4)	//ステージ数
#define RecordCase (5)	//レコード表示数
#define Time_Case (3)	//タイムの種類
#define Record_ (20)	//レコードケース
#define Digit (3)		//スコア格納領域
#define SAVEFAILE "Deta/savedata/savedata.bin"	//データ保存先

//***********************************************************
//構造体宣言
typedef struct
{
	int nPlayRecord;		//クリア時のスコア領域
	int nPlayIniRecord[5];	//クリア時のスコア描画情報
	int nRecordID;			//レコードずらし
	int nMenutTarget;		//タイム選択
	int nMenuStage;			//ステージ選択
	int nScore[Digit];		//スコアケース
	int nScoreOld[Digit];	//前回の数値ケース
	int nScoreX;			//スコア表示X軸
	int nScoreY;			//スコア表示Y軸
	int nRecordAddress;		//履歴構造体のアドレス渡し専用
	int nAddress;			//アドレス[エネミー用]
	int nEnemyCount;		//エネミーの体数
	int nMainIniRecord[Time_Case];
	bool bMenuGame;			//メニューフラグ
	bool bNEWScore;			//新しいスコアフラグ
	bool bStartGame;		//ゲームスタートフラグ
	bool bGameLiset;		//リストフラグ
	bool bHome;				//ホームフラグ
	bool bHomeLiset;		//ホームリストフラグ
	bool bNewRecord;		//最新記録フラグ
	bool bRog;				//ログフラグ
	bool bIniHome;			//ホーム描画フラグ
	bool bPlayeTitle;		//タイトルフラグ
	bool bPlayGame;			//ゲームスタートフラグ
	bool bGameEnd;			//ゲーム終了フラグ
	bool bLoad;				//ロードフラグ
	bool bResult;			//リザルトフラグ
}MAIN;

//バイナリデータへの保存用構造体
typedef struct
{
	int nRecord[RecordCase];					//比較用
	int nNewRecord[Time_Case];					//ステージ事の最新の記録格納
	int nIniNewRecord[Time_Case][3];			//ステージ事の最新の記録描画用
	int nOldNewRecord[Time_Case];				//古い履歴を避難
	int nOldIniNewRecord[Time_Case][3];			//描画用の値を避難
	int nLogRecord[Time_Case][RecordCase];		//比較後ログ格納領域
	int nIniRecord[Time_Case][RecordCase][3];	//描画用ログ格納領域
	
}SCORE;

//FPS表示
typedef struct
{
	int nFPS;
}_FPS;

void FPS(_FPS *aFPS);

#endif // !_MAIN_H_