//***********************************************************
//	HEW作品制作
//	そげぶキラー[result.cpp]
//***********************************************************
#include"main.h"
#include"result.h"

//***********************************************************
//初期化
void IniResult(RESULT *aResult)
{
	//数値の初期化
	aResult->nResultCount = 0;
	aResult->nResultScore[0] = 0;
	aResult->nResultScore[1] = 0;
	aResult->nResultScore[2] = 0;

	//フラグ初期化
	aResult->bResultWind = true;
	aResult->bResultIniScore = true;
	aResult->bResultIniScoreEnd = false;
	aResult->bResultStage = true;
	aResult->bResultStageEnd = true;
	aResult->bResultTime = true;
	aResult->bResultTimeEnd = false;
	aResult->bResultScore = true;
	aResult->bResultScoreEnd = false;
	aResult->bResultEnter = true;
	aResult->bResultEnterEnd = false;
}

//***********************************************************
//更新
void UpdateResult(RESULT *aResult, HOME *aHome, MAIN *aMain)
{
	//フレームカウント
	aResult->nResultCount++;

	//選択したステージの番号描画開始タイミング
	if (aResult->bResultStageEnd == true)
	{
		//現在のフレーム数と描画開始数値が一致したとき
		if (aResult->nResultCount == (int(RESULTLIMITCOUNT * 10)))
		{
			//フレーム数リセット
			aResult->nResultCount = 0;

			//ステージ描画開始
			aResult->bResultStage = false;
		}
	}

	//選択した時間の描画開始タイミング
	if (aResult->bResultTimeEnd == true)
	{
		//現在のフレーム数と描画開始数値が一致したとき
		if (aResult->nResultCount == (int(RESULTLIMITCOUNT * 10)))
		{
			//フレーム数リセット
			aResult->nResultCount = 0;

			
			aResult->bResultTime = false;
		}
	}

	//スコアテキスト描画開始タイミング
	if (aResult->bResultScoreEnd == true)
	{
		//現在のフレーム数と描画開始数値が一致したとき
		if (aResult->nResultCount == (int(RESULTLIMITCOUNT * 10)))
		{
			//フレーム数リセット
			aResult->nResultCount = 0;

			//スコア描画開始
			aResult->bResultScore = false;
		}
	}

	//スコア描画タイミング
	if (aResult->bResultIniScoreEnd == true)
	{
		//現在のフレーム数と描画開始数値が一致したとき
		if (aResult->nResultCount == (int(RESULTLIMITCOUNT / RESULTLIMITCOUNT)))
		{
			//フレーム数リセット
			aResult->nResultCount = 0;

			//スコア加算開始
			aResult->bResultIniScore = false;
		}
	}

	//全処理終了時
	if (aResult->bResultEnterEnd == true)
	{
		if (INP(PK_ENTER))
		{
			if (aHome->bPkEnter == true)
			{
				//各グラグ操作
				aResult->bResultComp = true;
				aMain->bLoad = true;
				aMain->bGameEnd = false;
				aMain->bResult = false;
			}
			aHome->bPkEnter = false;
		}
		else
		{
			aHome->bPkEnter = true;
		}
	}
}

//***********************************************************
//描画
void DrawResult(RESULT *aResult, TEXT *aText, TIME *aTimeCount, MAIN *aMain, SCORE *aScore)
{
	int nCount, nCountA;

	//リザルト用ウィンドウ描画開始
	if (aResult->bResultWind == true)
	{
		for (nCountA = RESULTY; nCountA < RESULTendY; nCountA++)
		{
			for (nCount = RESULTX; nCount < RESULTendX; nCount += 180)
			{
				COLOR(BLACK, BLACK);
				LOCATE(nCount, nCountA);
				printf("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
			}
		}

		for (nCount = RESULTX; nCount < RESULTendX; nCount += 2)
		{
			COLOR(H_CYAN, H_CYAN);
			LOCATE(nCount, RESULTY);
			printf("■");
			LOCATE(nCount, RESULTendY);
			printf("■■");
		}

		for (nCountA = RESULTY; nCountA < RESULTendY; nCountA++)
		{
			COLOR(H_CYAN, H_CYAN);
			LOCATE(RESULTX, nCountA);
			printf("■");
			LOCATE(RESULTendX, nCountA);
			printf("■");
		}
		//end

		//リザルトフォント
		COLOR(WHITE, BLACK);
		nCount = 0;
		LOCATE((int((RESULTendX - RESULTX)*0.4)), (int((RESULTendY - RESULTY)*0.4)));
		printf("■■■■　　　　　　　　　　　　　　　　　　　　　　　");
		nCount++;
		LOCATE((int((RESULTendX - RESULTX)*0.4)), (int((RESULTendY - RESULTY)*0.4)) + nCount);
		printf("■　　　■　　　　　　　　　　　　　　　　　　　　■　");
		nCount++;
		LOCATE((int((RESULTendX - RESULTX)*0.4)), (int((RESULTendY - RESULTY)*0.4)) + nCount);
		printf("■　　　■　　■■　　　■■　　■　　　■　■　■■■");
		nCount++;
		LOCATE((int((RESULTendX - RESULTX)*0.4)), (int((RESULTendY - RESULTY)*0.4)) + nCount);
		printf("■■■■　　■　　■　■　　　　■　　　■　■　　■　");
		nCount++;
		LOCATE((int((RESULTendX - RESULTX)*0.4)), (int((RESULTendY - RESULTY)*0.4)) + nCount);
		printf("■　　　■　■■■■　　■■　　■　　　■　■　　■　");
		nCount++;
		LOCATE((int((RESULTendX - RESULTX)*0.4)), (int((RESULTendY - RESULTY)*0.4)) + nCount);
		printf("■　　　■　■　　　　　　　■　■　　■■　■　　■　");
		nCount++;
		LOCATE((int((RESULTendX - RESULTX)*0.4)), (int((RESULTendY - RESULTY)*0.4)) + nCount);
		printf("■　　　■　　■■　　　■■　　　■■　■　　■　　■");
		//end

		//リザルトボーダー
		nCount++;
		for (nCountA = (int((RESULTendX - RESULTX)*0.38)); nCountA < (int(RESULTendX*0.97)); nCountA += 2)
		{
			COLOR(WHITE, BLACK);
			LOCATE(nCountA, (int((RESULTendY - RESULTY)*0.4)) + nCount);
			printf("―");
		}
		//end

		//描画終了
		aResult->bResultWind = false;
	}

	//選択したステージの番号描画
	if (aResult->bResultStageEnd == true)
	{
		//描画開始フラグが立ったとき
		if (aResult->bResultStage == false)
		{
			//標示位置
			nCount = 9;
			aText->n_textX = (int((RESULTendX - RESULTX)*0.38));
			aText->n_textY = (int((RESULTendY - RESULTY)*0.4)) + nCount;

			//ステージテキスト
			COLOR(WHITE, BLACK);
			textStage(&aText[0]);

			//ステージ描画終了
			aResult->bResultStage = true;
			aResult->bResultStageEnd = false;

			//時間の描画を開始可能に
			aResult->bResultTimeEnd = true;
		}
	}

	//時間の描画を開始
	if (aResult->bResultTimeEnd == true)
	{
		//描画開始フラグが立ったとき
		if (aResult->bResultTime == false)
		{
			//時間初期化
			HomeTimeLiset(&aTimeCount[0]);

			//標示座標
			nCount = 9;
			aTimeCount->nTimePozi = (int(RESULTendX*0.65));
			aTimeCount->nTimeHeit = (int((RESULTendY - RESULTY)*0.4)) + nCount;

			//選択した時間描画
			COLOR(WHITE, BLACK);
			DrawTime(&aTimeCount[0]);

			//時間描画終了
			aResult->bResultTime = true;
			aResult->bResultTimeEnd = false;

			//スコアの描画を開始可能に
			aResult->bResultScoreEnd = true;
		}
	}

	//スコアテキストの描画を開始
	if (aResult->bResultScoreEnd == true)
	{
		//描画開始フラグが立ったとき
		if (aResult->bResultScore == false)
		{
			//スコア描画に必要なフラグ
			aMain->bHome = true;

			//テキスト標示位置
			nCount = 24;
			aText->n_RecordX = (int((RESULTendX - RESULTX)*0.385));
			aText->n_RecordY = (int((RESULTendY - RESULTY)*0.4)) + nCount;

			//テキスト描画
			COLOR(WHITE, BLACK);
			textRecord(&aText[0]);

			//スコア描画位置
			aMain->nScoreX = (int((RESULTendX - RESULTX)*0.9));
			aMain->nScoreY = (int((RESULTendY - RESULTY)*0.4)) + nCount;

			//スコア初期化
			InitScore(&aMain[0], &aScore[0]);

			//リザルト用スコアを格納
			aResult->nResultScore[0] = (aScore + aMain->nRecordAddress)->nIniRecord[aMain->nMenutTarget][0][0];
			aResult->nResultScore[1] = (aScore + aMain->nRecordAddress)->nIniRecord[aMain->nMenutTarget][0][1];
			aResult->nResultScore[2] = (aScore + aMain->nRecordAddress)->nIniRecord[aMain->nMenutTarget][0][2];

			//スコア初期描画
			COLOR(WHITE, BLACK);
			Score(&aMain[0]);

			//スコアテキスト描画終了
			aResult->bResultScoreEnd = false;

			//スコア描画処理開始
			aResult->bResultIniScoreEnd = true;

			//スコア描画に必要なフラグ
			aMain->bHome = false;
		}
	}

	//スコア描画処理開始
	if (aResult->bResultIniScoreEnd == true)
	{
		//スコア加算開始
		if (aResult->bResultIniScore == false)
		{
			//加算
			aMain->nScore[0]++;

			//繰り上げ処理
			ScoreProcess(&aMain[0], &aScore[0]);

			//描画
			COLOR(WHITE, BLACK);
			Score(&aMain[0]);

			//加算終了
			aResult->bResultIniScore = true;
		}

		//加算中の数値とクリア時のスコアが一致したとき
		if ((aResult->nResultScore[0] == aMain->nScore[0]) && (aResult->nResultScore[1] == aMain->nScore[1]) && (aResult->nResultScore[2] == aMain->nScore[2]))
		{
			//最新記録のフラグが立っていた場合
			if (aMain->bNEWScore == false)
			{
				//テキストの種類
				aText->RecordType = 2;

				//標示位置
				nCount = 26;
				aText->n_RecordX = (int((RESULTendX - RESULTX)*1.0));
				aText->n_RecordY = (int((RESULTendY - RESULTY)*0.4)) + nCount;

				//描画
				COLOR(H_YELLOW, BLACK);
				textRecord(&aText[0]);

				//最新記録フラグを戻す
				aMain->bNEWScore = true;
			}
			//スコア描画終了
			aResult->bResultIniScoreEnd = false;
			aResult->bResultEnterEnd = true;

			//enterキー入力可能
			aResult->bResultEnter = false;
		}
	}

	//ok描画開始
	if (aResult->bResultEnterEnd == true)
	{
		//enterキーが入力可能になったとき
		if (aResult->bResultEnter == false)
		{
			COLOR(H_CYAN, H_CYAN);
			for (nCount = (int(RESULTendX*0.535)); nCount < (int(RESULTendX*0.735)); nCount += 6)
			{
				for (nCountA = (int(RESULTendY*0.85)); nCountA < (int(RESULTendY*0.95)); nCountA++)
				{
					LOCATE(nCount, nCountA);
					printf("　　　");
				}
			}
			nCount = 1;
			COLOR(BLACK, H_CYAN);
			LOCATE((int(RESULTendX*0.6)), (int(RESULTendY*0.85)) + nCount);
			printf("　■■　　■　　　");
			nCount++;
			LOCATE((int(RESULTendX*0.6)), (int(RESULTendY*0.85)) + nCount);
			printf("■　　■　■　　■");
			nCount++;
			LOCATE((int(RESULTendX*0.6)), (int(RESULTendY*0.85)) + nCount);
			printf("■　　■　■　■　");
			nCount++;
			LOCATE((int(RESULTendX*0.6)), (int(RESULTendY*0.85)) + nCount);
			printf("■　　■　■■■　");
			nCount++;
			LOCATE((int(RESULTendX*0.6)), (int(RESULTendY*0.85)) + nCount);
			printf("　■■　　■　　■");

			//ok描画終了
			aResult->bResultEnter = true;
		}
	}
}