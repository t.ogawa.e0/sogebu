//***********************************************************
//	HEW作品制作
//	そげぶキラー[text.h]
//***********************************************************
#include"main.h"

//***********************************************************
//マクロ定義
#ifndef _TEXT_H_			//二重インクルード防止のマクロ定義
#define _TEXT_H_
#define TEXTsize (1)		//text構造の数
#define WindSize_X (60)		//ウィンドウサイズ
#define WindSize_endX (240)	//ウィンドウサイズ
#define WindSize_Y (30)		//ウィンドウサイズ
#define WindSize_endY (65)	//ウィンドウサイズ

//***********************************************************
//構造体宣言
typedef struct
{
	int n_BorderX, n_BorderEndX, n_BorderY;	//ボーダーポジション
	int n_text,n_textold;					//テキスト
	int n_textX, n_textXold;				//ポジション
	int n_textY, n_textYold;				//ポジション
	int n_TimeText, n_TimeTextold;			//タイムテキスト
	int n_TimeTextX, n_TimeTextXold;		//ポジション
	int n_TimeTextY, n_TimeTextYold;		//ポジション
	int n_RecordX, n_RecordY,RecordType;	//レコード
	int n_WindTimeX, n_WindTimeY;			//タイム表示位置
	int n_TextLifeX, n_TextLifeY;
	int nWindKey;							//キー選択
	bool bDrawConfi,bDrawText;				//テキスト表示フラグ
	bool bWindDelete;						//ウィンドウ消去フラグ
}TEXT;

//***********************************************************
//プロトタイプ宣言
void Inittext(TEXT *aText);			//初期化
void Updatetext(TEXT *aText);		//更新
void Drawtext(TEXT *aText);			//描画
void Uninittext(void);				//後処理
void textStage(TEXT *aText);		//ステージテキスト
void textTimeSelect(TEXT *aText);	//タイムテキスト
void textBorder(TEXT *aText);		//ボーダー
void textWind(TEXT *aText);			//テキスト用ウィンドウ
void WindKey(TEXT *aText);			//キーリスト
void WindDelete(TEXT *aText);		//ウィンドウ消去処理
void textRecord(TEXT *aText);		//レコードフォンと表示
void textWintTime(TEXT *aText);		//タイムフォント
void textLife(TEXT *aText);			//ライフテキスト

#endif // !_TEXT_H_