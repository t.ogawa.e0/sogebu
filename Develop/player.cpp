//***********************************************************
//	HEWìi§ì
//	»°ÔL[[player.cpp]
//***********************************************************
#include"main.h"
#include"player.h"

//***********************************************************
//vC[ú
void InitPlayer(PLAYER *aPlayer, WORLD *aWorld)
{
	int nCount;

	//vC[ú
	//vC[el
	aPlayer->Life = PlayerLife;
	aPlayer->fPosX = aWorld->fPlayer_Start_PositionX;
	aPlayer->fPosY = aWorld->fPlayer_Start_PositionY;
	aPlayer->fPosXold = aPlayer->fPosX;
	aPlayer->fPosYold = aPlayer->fPosY;
	aPlayer->fCount[PlayerCountJump] = 0;
	aPlayer->fCount[PlayerCountGravity] = 0;
	aPlayer->bPushDOWN = 0;
	aPlayer->EffectCount = 0;
	aPlayer->EffectPozisyonX = 0;
	aPlayer->EffectPozisyonY = 0;

	//tOú»
	aPlayer->bLifeIni = true;
	aPlayer->bAnnihilationEffect = true;
	aPlayer->bLipopEffect = true;
	aPlayer->EffectEnd = false;
	aPlayer->bTump = true;
	aPlayer->bPush_Key[PlayerSpaceCoordinates] = true;
	aPlayer->bProcess_Judgment[PlayerProcessJump] = true;

	//Cto[ú`æ
	COLOR(H_GREEN);
	nCount = 88;
	LOCATE(PlayerLifeBar, nCount);
	printf("                                        ");
	nCount++;
	LOCATE(PlayerLifeBar, nCount);
	printf("                                        ");
	nCount++;
	LOCATE(PlayerLifeBar, nCount);
	printf("                                        ");
	nCount++;
	LOCATE(PlayerLifeBar, nCount);
	printf("                                        ");
	nCount++;
	LOCATE(PlayerLifeBar, nCount);
	printf("                                        ");
	nCount++;
	LOCATE(PlayerLifeBar, nCount);
	printf("                                        ");
}

//***********************************************************
//vC[XV
void UpdatePlayer(PLAYER *aPlayer, WORLD *aWorld, AA *aAA)
{
	//vC[ÌÁÅGtFNg
	if (aPlayer->EffectEnd == false)
	{
		//vC[ÌCtª0ÈºÌÆ«
		if (aPlayer->Life <= 0)
		{
			//ÁÅGtFNgJn
			aPlayer->EffectEnd = true;
		}
	}

	//vC[ÌCtª0ÈãÌÆ«
	if (!(aPlayer->Life <= 0))
	{
		ProcessUpdate(&aPlayer[0], &aWorld[0]);
		PlayerMove(&aPlayer[0], &aAA[0]);
		PlayerDown(&aPlayer[0]);
		PlayerJump(&aPlayer[0]);
		ProcessJump(&aPlayer[0]);
		GravityJudgment(&aPlayer[0], &aWorld[0]);
		ProcessJumpGravity(&aPlayer[0], &aWorld[0], &aAA[0]);
		ReverseMove(&aPlayer[0], &aWorld[0]);
		PlayerAAlast(&aPlayer[0], &aAA[0]);
	}
}

//***********************************************************
//vC[`æ
void DrawPlayer(PLAYER *aPlayer, AA *aAA, MAIN *aMain, SCORE *aScore)
{
	//ÁÅGtFNg
	PlayerEffect(&aPlayer[0], &aMain[0], &aScore[0]);

	//Ctª0ÈºÌÆ«
	if (aPlayer->Life <= 0)
	{
		aPlayer->fPosX = 290;
		aPlayer->fPosY = 90;
	}

	//vC[ÌÀWªOñÌÀWÆêvµÄ¢È¢Æ«
	if (!((aPlayer->fPosXold == aPlayer->fPosX) && (aPlayer->fPosYold == aPlayer->fPosY)))
	{
		COLOR(WHITE);

		//Ã¢ÀWÉOñ\¦µ½AAð`æ
		AAPlayerold(&aPlayer[0], &aAA[0]);

		//Ctª0ÈºÌÆ«
		if (aPlayer->Life <= 0)
		{
			aPlayer->fPosXold = 290;
			aPlayer->fPosYold = 90;
			aAA->PlayerAA = 99;
		}
	}

	//Ctª0ÈãÌÆ«
	if (!(aPlayer->Life <= 0))
	{
		COLOR(BLACK);

		//»ÝÌAA
		AAPlayer(&aPlayer[0], &aAA[0]);
	}
}

//***********************************************************
//vC[ã
void UninitPlayer(void)
{}

//***********************************************************
//vC[Ú®
void PlayerMove(PLAYER *aPlayer, AA *aAA)
{
	//EÚ®
	if (INP(PK_RIGHT) || INP(PK_D))
	{
		aPlayer->fPosX += PlayerMove_X;
		aAA->PlayerAA = 2;

		//Ú®AAÌli[
		aAA->PlayerlastAA = aAA->PlayerAA;
	}

	//¶Ú®
	if (INP(PK_LEFT) || INP(PK_A)) {
		aPlayer->fPosX -= PlayerMove_X;
		aAA->PlayerAA = 3;

		//Ú®AAÌli[
		aAA->PlayerlastAA = aAA->PlayerAA;
	}
}

//***********************************************************
//vC[~èé
void PlayerDown(PLAYER *aPlayer)
{
	//óÉ¢È¢Æ«
	if (aPlayer->bTump == true)
	{
		//~èétO
		if (INP(PK_DOWN) || INP(PK_S))
		{
			if (aPlayer->bPushDOWN == true)
			{
				aPlayer->fPosY += 1.1;
			}
			aPlayer->bPushDOWN = false;
		}
		else
		{
			aPlayer->bPushDOWN = true;
		}
	}
}

//***********************************************************
//vC[WvtO
void PlayerJump(PLAYER *aPlayer)
{
	//óÉ¢È¢Æ«
	if (aPlayer->bTump == true)
	{
		if (INP(PK_UP) || INP(PK_W))
		{
			if (aPlayer->bPush_Key[PlayerSpaceCoordinates] == true)
			{
				//WvJn
				aPlayer->bProcess_Judgment[PlayerProcessJump] = false;
				aPlayer->bPush_Key[PlayerSpaceCoordinates] = false;
			}
		}
		else
		{
			aPlayer->bPush_Key[PlayerSpaceCoordinates] = true;
		}
	}
}

//***********************************************************
//Wv
void ProcessJump(PLAYER *aPlayer)
{
	//WvJn
	if (aPlayer->bProcess_Judgment[PlayerProcessJump] == false)
	{
		//»ÝÌt[æèWvÉKvÈt[ªå«¢Æ«
		if (aPlayer->fCount[PlayerCountJump] < PlayerJump_frame)
		{
			aPlayer->fPosY -= PlayerMove_Jump;

			//t[
			aPlayer->fCount[PlayerCountJump] += 1;
		}
		else
		{
			//vC[É©©édÍðZbg
			aPlayer->fCount[PlayerCountGravity] = 0;

			//t[Zbg
			aPlayer->fCount[PlayerCountJump] = 0;

			//WvI¹
			aPlayer->bProcess_Judgment[PlayerProcessJump] = true;
		}
	}
}

//***********************************************************
//vC[ß·
void ReverseMove(PLAYER *aPlayer, WORLD *aWorld)
{
	//EÌÇÉûè±ñ¾Æ«Éß·
	if (aPlayer->fPosX + 20 >= (RightWall_X - 1.5))
	{
		aPlayer->fPosX = RightWall_X - 1.5 - 20;
	}

	//¶ÌÇÉûè±ñ¾Æ«Éß·
	if (aPlayer->fPosX - 13 <= RightWall_Y + 1)
	{
		aPlayer->fPosX = RightWall_Y + 1.5 + 13;
	}

	//VäÌÇÉûè±ñ¾Æ«Éß·
	if (aPlayer->fPosY - 1 <= TopWall + 2)
	{
		aPlayer->fPosY = TopWall + 3;

		//t[ÉÉKvÈt[ð«·
		aPlayer->fCount[PlayerCountJump] += PlayerJump_frame;
	}
}

//***********************************************************
//dÍ»è
void GravityJudgment(PLAYER *aPlayer, WORLD *aWorld)
{
	int nCount;
	nCount = 0;

	//dÍ»è[êÔºÌ°]
	if (!(aPlayer->fPosY == UnderWall - 1))
	{
		aPlayer->nGravity = 1;
	}

	//dÍ»è[óÌ°]
	for (nCount = 0; nCount < aWorld->nScaffold; nCount++)
	{
		//°ÌJnn_ÌÀW[X]Æ°ÌI¹n_ÌÀW[X]ÌÔÉAvC[ÌÀW[X]ª¶Ý·éÆ«
		if ((aWorld->fScaffold[nCount][0] - PlayerReloadright <= aPlayer->fPosX) && (aPlayer->fPosX <= aWorld->fScaffold[nCount][1] + PlayerReloadleft))
		{
			//vC[ÌÀW[Y]æè°ÌÀW[Y]ÌÊuªá¢Æ«
			if (aPlayer->fPosY <= aWorld->fScaffold[nCount][2])
			{
				//nn_Ì°îñ
				aWorld->nScaffoldCount = nCount;

				//JEg­§I¹
				nCount = aWorld->nScaffold;
				aPlayer->nGravity = 2;
			}
		}
	}
}

//***********************************************************
//dÍ,nÊ»è
void ProcessJumpGravity(PLAYER *aPlayer, WORLD *aWorld, AA *aAA)
{
	//dÍ
	switch (aPlayer->nGravity)
	{
	case 1:
		//dÍ[êÔºÌ°]
		//êÔµ½Ì°ÌãÉvC[ª¶Ý·éÆ«
		if (UnderWall - 1 <= aPlayer->fPosY)
		{
			aPlayer->fPosY = UnderWall - 1;

			//dÍð0É
			aPlayer->fCount[PlayerCountGravity] = 0;

			//WvÂ\
			aPlayer->bTump = true;
		}
		else
		{
			//»ÝóÉ¢Ü·
			aPlayer->bTump = false;

			//dÍðÁZµÄ¢­
			aPlayer->fPosY += (aPlayer->fCount[PlayerCountGravity] += PlayerMove_Jump_Attenuation);

			//óÅÌAA
			if (aAA->PlayerlastAA == 2)
			{
				aAA->PlayerAA = 7;
			}
			if (aAA->PlayerlastAA == 3)
			{
				aAA->PlayerAA = 4;
			}
		}
		break;
	case 2:
		//dÍ[óÌ°]
		//»ÝóÌ°ÌãÉ¢Ü·
		if (aWorld->fScaffold[aWorld->nScaffoldCount][2] - 1 <= aPlayer->fPosY)
		{
			aPlayer->fPosY = aWorld->fScaffold[aWorld->nScaffoldCount][2] - 1;

			//dÍð0É
			aPlayer->fCount[PlayerCountGravity] = 0;

			//WvÂ\
			aPlayer->bTump = true;
		}
		else
		{
			//»ÝóÉ¢Ü·
			aPlayer->bTump = false;

			//dÍðÁZµÄ¢­
			aPlayer->fPosY += (aPlayer->fCount[PlayerCountGravity] += PlayerMove_Jump_Attenuation);

			//óÅÌAA
			if (aAA->PlayerlastAA == 2)
			{
				aAA->PlayerAA = 7;
			}
			if (aAA->PlayerlastAA == 3)
			{
				aAA->PlayerAA = 4;
			}
		}
		break;
	default:
		break;
	}
}

//***********************************************************
//àeø«p¬
void ProcessUpdate(PLAYER *aPlayer, WORLD *aWorld)
{
	//ú»
	aPlayer->nGravity = 0;

	//OñÌÀWði[
	aPlayer->fPosXold = aPlayer->fPosX;
	aPlayer->fPosYold = aPlayer->fPosY;
	aPlayer->EffectPozisyonX = aPlayer->fPosXold;
	aPlayer->EffectPozisyonY = aPlayer->fPosYold;
}

//***********************************************************
//GtFNg
void PlayerEffect(PLAYER *aPlayer, MAIN *aMain, SCORE *aScore)
{
	//GtFNg­¶
	if (aPlayer->EffectEnd == true)
	{
		//GtFNgOÌú»
		if (aPlayer->bAnnihilationEffect == true)
		{
			aPlayer->EffectCount = 0;

			//Jn
			aPlayer->bAnnihilationEffect = false;
		}

		//ÁÅGtFNg
		if (aPlayer->bAnnihilationEffect == false)
		{
			//GtFNgJEg
			aPlayer->EffectCount++;
			switch (aPlayer->EffectCount)
			{
			case 1:
				LOCATE(aPlayer->EffectPozisyonX - 2, aPlayer->EffectPozisyonY - 1);
				printf("£");
				LOCATE(aPlayer->EffectPozisyonX - 2, aPlayer->EffectPozisyonY);
				printf("¢");
				LOCATE(aPlayer->EffectPozisyonX, aPlayer->EffectPozisyonY - 1);
				printf("~");
				LOCATE(aPlayer->EffectPozisyonX, aPlayer->EffectPozisyonY);
				printf(" ");
				LOCATE(aPlayer->EffectPozisyonX + 2, aPlayer->EffectPozisyonY - 1);
				printf("");
				LOCATE(aPlayer->EffectPozisyonX + 2, aPlayer->EffectPozisyonY);
				printf("");
				break;
			case 2:
				LOCATE(aPlayer->EffectPozisyonX - 4, aPlayer->EffectPozisyonY - 1);
				printf("");
				LOCATE(aPlayer->EffectPozisyonX - 4, aPlayer->EffectPozisyonY);
				printf("~");
				LOCATE(aPlayer->EffectPozisyonX - 2, aPlayer->EffectPozisyonY - 2);
				printf("¤");
				LOCATE(aPlayer->EffectPozisyonX - 2, aPlayer->EffectPozisyonY - 1);
				printf(" ");
				LOCATE(aPlayer->EffectPozisyonX - 2, aPlayer->EffectPozisyonY);
				printf("£");
				LOCATE(aPlayer->EffectPozisyonX, aPlayer->EffectPozisyonY - 2);
				printf("~");
				LOCATE(aPlayer->EffectPozisyonX, aPlayer->EffectPozisyonY - 1);
				printf("");
				LOCATE(aPlayer->EffectPozisyonX, aPlayer->EffectPozisyonY);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX + 2, aPlayer->EffectPozisyonY - 2);
				printf("£");
				LOCATE(aPlayer->EffectPozisyonX + 2, aPlayer->EffectPozisyonY - 1);
				printf("~");
				LOCATE(aPlayer->EffectPozisyonX + 2, aPlayer->EffectPozisyonY);
				printf("¡");
				LOCATE(aPlayer->EffectPozisyonX + 4, aPlayer->EffectPozisyonY - 1);
				printf("¢");
				LOCATE(aPlayer->EffectPozisyonX + 4, aPlayer->EffectPozisyonY);
				printf("");
				break;
			case 3:
				LOCATE(aPlayer->EffectPozisyonX - 4, aPlayer->EffectPozisyonY - 1);
				printf(" ");
				LOCATE(aPlayer->EffectPozisyonX - 4, aPlayer->EffectPozisyonY);
				printf("¢");
				LOCATE(aPlayer->EffectPozisyonX - 2, aPlayer->EffectPozisyonY - 2);
				printf("¤");
				LOCATE(aPlayer->EffectPozisyonX - 2, aPlayer->EffectPozisyonY - 1);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX - 2, aPlayer->EffectPozisyonY);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX, aPlayer->EffectPozisyonY - 2);
				printf("£");
				LOCATE(aPlayer->EffectPozisyonX, aPlayer->EffectPozisyonY - 1);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX + 2, aPlayer->EffectPozisyonY - 2);
				printf("");
				LOCATE(aPlayer->EffectPozisyonX + 2, aPlayer->EffectPozisyonY - 1);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX + 2, aPlayer->EffectPozisyonY);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX + 4, aPlayer->EffectPozisyonY - 1);
				printf("~");
				LOCATE(aPlayer->EffectPozisyonX + 4, aPlayer->EffectPozisyonY);
				printf("¡");
				break;
			case 4:
				LOCATE(aPlayer->EffectPozisyonX - 5, aPlayer->EffectPozisyonY - 2);
				printf(" ");
				LOCATE(aPlayer->EffectPozisyonX - 5, aPlayer->EffectPozisyonY - 1);
				printf("~");
				LOCATE(aPlayer->EffectPozisyonX - 3, aPlayer->EffectPozisyonY - 3);
				printf("");
				LOCATE(aPlayer->EffectPozisyonX, aPlayer->EffectPozisyonY - 3);
				printf("¤");
				LOCATE(aPlayer->EffectPozisyonX + 3, aPlayer->EffectPozisyonY - 3);
				printf("¡");
				LOCATE(aPlayer->EffectPozisyonX + 5, aPlayer->EffectPozisyonY - 2);
				printf("£");
				LOCATE(aPlayer->EffectPozisyonX + 5, aPlayer->EffectPozisyonY - 1);
				printf("¡");

				LOCATE(aPlayer->EffectPozisyonX - 4, aPlayer->EffectPozisyonY - 1);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX - 4, aPlayer->EffectPozisyonY);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX - 2, aPlayer->EffectPozisyonY - 2);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX, aPlayer->EffectPozisyonY - 2);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX + 2, aPlayer->EffectPozisyonY - 2);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX + 4, aPlayer->EffectPozisyonY - 1);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX + 4, aPlayer->EffectPozisyonY);
				printf("@");
				break;
			case 5:
				LOCATE(aPlayer->EffectPozisyonX - 6, aPlayer->EffectPozisyonY - 3);
				printf("¡");
				LOCATE(aPlayer->EffectPozisyonX - 6, aPlayer->EffectPozisyonY - 2);
				printf("£");
				LOCATE(aPlayer->EffectPozisyonX - 4, aPlayer->EffectPozisyonY - 4);
				printf("");
				LOCATE(aPlayer->EffectPozisyonX, aPlayer->EffectPozisyonY - 4);
				printf("~");
				LOCATE(aPlayer->EffectPozisyonX + 4, aPlayer->EffectPozisyonY - 4);
				printf("¤");
				LOCATE(aPlayer->EffectPozisyonX + 6, aPlayer->EffectPozisyonY - 3);
				printf(" ");
				LOCATE(aPlayer->EffectPozisyonX + 6, aPlayer->EffectPozisyonY - 2);
				printf("");

				LOCATE(aPlayer->EffectPozisyonX - 5, aPlayer->EffectPozisyonY - 2);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX - 5, aPlayer->EffectPozisyonY - 1);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX - 3, aPlayer->EffectPozisyonY - 3);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX, aPlayer->EffectPozisyonY - 3);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX + 3, aPlayer->EffectPozisyonY - 3);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX + 5, aPlayer->EffectPozisyonY - 2);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX + 5, aPlayer->EffectPozisyonY - 1);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX + 5, aPlayer->EffectPozisyonY);
				printf("@");
				break;
			case 6:
				LOCATE(aPlayer->EffectPozisyonX - 6, aPlayer->EffectPozisyonY - 3);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX - 6, aPlayer->EffectPozisyonY - 2);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX - 4, aPlayer->EffectPozisyonY - 4);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX, aPlayer->EffectPozisyonY - 4);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX + 4, aPlayer->EffectPozisyonY - 4);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX + 6, aPlayer->EffectPozisyonY - 3);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX + 6, aPlayer->EffectPozisyonY - 2);
				printf("@");
				LOCATE(aPlayer->EffectPozisyonX + 6, aPlayer->EffectPozisyonY);
				printf("@");
				break;
			case 7:
				//I¹
				//Ctðú»
				aPlayer->Life = 0;

				//etOì
				aMain->bIniHome = true;
				aMain->bHome = false;
				aMain->bHomeLiset = true;
				aMain->bStartGame = true;
				aMain->bGameLiset = true;
				aMain->bRog = false;

				//»ÝÌXRAi[
				aMain->nPlayIniRecord[0] = aMain->nScore[0];
				aMain->nPlayIniRecord[1] = aMain->nScore[1];
				aMain->nPlayIniRecord[2] = aMain->nScore[2];

				//XRAú»
				aMain->nScore[0] = 0;
				aMain->nScore[1] = 0;
				aMain->nScore[2] = 0;

				//XRA
				//ÅVL^©Û©Ì
				HomeRecord(&aMain[0], &aScore[0]);

				//OÖi[
				HomeScoreConp(&aMain[0], &aScore[0]);

				//Û¶
				RecordSave(&aScore[0]);

				//etOì
				aMain->bPlayGame = false;
				aMain->bGameEnd = true;
				aMain->bLoad = true;

				//GtFNgI¹
				aPlayer->EffectEnd = false;

				//GtFNgú»
				aPlayer->EffectCount = 0;
				aPlayer->EffectPozisyonX = 0;
				aPlayer->EffectPozisyonY = 0;
				break;
			default:
				break;
			}
		}
	}
}