//***********************************************************
//	HEW作品制作
//	そげぶキラー[Lifebar.h]
//***********************************************************
#include"main.h"
#include"EnemyStructure.h"
#include"PlayerStructure.h"

//***********************************************************
//マクロ定義
#ifndef _Lifebar_H_	//二重インクルード防止のマクロ定義
#define _Lifebar_H_
#define PlayerLifeBar (200)

//***********************************************************
//プロトタイプ宣言
void DrawEnemyLifebar(MAIN *aMain, ENEMY *aEnemy);		//エネミーライフバー
void DrawPlayerLifebar(MAIN *aMain, PLAYER *aPlayer);	//プレイヤーライフバー

#endif // !_Lifebar_H_
