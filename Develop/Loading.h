//***********************************************************
//	HEW作品制作
//	そげぶキラー[Loading.h]
//***********************************************************
#include"main.h"

//***********************************************************
//	マクロ定義
#ifndef _Loading_H_				//二重インクルード防止のマクロ定義
#define _Loading_H_
#define _LOADING_ (1)			//ロード構造数
#define LOADINGMOVE (19)		//ロードの速さ
#define LOADINGMOVELIMIT_Y (95)	//ロード最大座標
#define LOADINGMOVELIMIT_X (300)//ロード最大座標
#define LOADMOVEFRAME (1)		//動作判定
#define LOADMOVEFRAMERIMIT (5)	//ロード時間判定
#define LOADTIME (25)			//ロード時間

//***********************************************************
//構造体宣言
typedef struct
{
	float fLoadCount;								//ロード時間カウント
	int nLoadX, nLoadY, nLoadLimitX, nLoadLimitY;	//描画用座標
	int nLoadCount, nLoadbarLimit;					//ロードカウントと上限
	int nLimitCount;								//上限のカウント
	bool bLoadMove, bLoadDraw, bLoadOpen, bOpenDraw, bOpenTime, bDrawTime, bComplete,bOpnSE,bOpn;	//各フラグ
}LOADING;

//***********************************************************
//	プロトタイプ宣言
void InitLoading(LOADING *aLoading, MAIN *aMain);	//初期化
void UpdateLoading(LOADING *aLoading, MAIN *aMain);	//更新
void DrawLoading(LOADING *aLoading, MAIN *aMain);	//描画

#endif // !_Loading_H_
