//***********************************************************
//	HEW作品制作
//	そげぶキラー[EnemyStructure.h]
//***********************************************************
#include"CScreen.h"

//***********************************************************
//	マクロ定義
#ifndef _EnemyStructure_H_				//二重インクルード防止のマクロ定義
#define _EnemyStructure_H_
#define EnemySpaceCoordinates (0)		//スペースキー座標
#define EnemyProcessCase (1)			//処理判定ケース
#define EnemyCountCase (3)				//カウントの種類
#define EnemyProcessJump (0)			//ジャンプ処理終了判定番地
#define EnemyCountJump (0)				//ジャンプ処理番地
#define EnemyCountGravity (1)			//重力処理番地
#define Enemy_to_Use (0)				//エネミーの番地
#define ReconnaissanceLeftAndRight (0)	//左右索敵番地
#define ReconnaissanceUpAndDown (1)		//上下索敵番地
#define Approach_Distance_Wall (2)		//壁への接近距離
#define MoveLeftOrRight (3)				//右か左か
#define LeftAndRight (80)				//左右の索敵範囲
#define UpAndDown (4)					//上下の索敵範囲
#define EnemyReconnaissance (2)			//索敵の種類
#define LimitProbability (100)			//確率上限
#define Probability (10)				//的中率
#define EnemyPattern (5)				//エネミーの行動パターンの種類
#define PatternProcess (0)				//パターンの処理番地
#define Left (0)						//右
#define Right (1)						//左
#define EnemyMove (1)					//動く判定番地
#define EnemyDown (2)					//床を降りる判定番地
#define EnemyStop (3)					//停止判定番地
#define EnemyJump (4)					//ジャンプ判定番地
#define EnemyPursuit (5)				//余り領域
#define EnemyStopTime (15)				//15フレーム停止//基準値
#define EnemyMoveTime (100)				//100フレーム動く
#define PursuitMode (50)				//50フレーム後に追跡実行
#define RecoveryTime (60)				//復帰時間上限
#define EnemyLife (100)					//エネミーの体力
#define EnemyAttak (20)					//エネミーの攻撃力

//***********************************************************
//構造体宣言
typedef struct
{
	int nEnemyPattern;							//行動パターン選択
	int nCount[EnemyPattern];					//カウント
	int nEnemyProcessFlame[EnemyPattern];		//各処理case
	int nLeftOrRight;							//右か左か
	int Decision;								//確率キー
	int Greater_than;							//確率初めの値
	int Less_than;								//確率終わりの値
	int nGravity;								//重力
	int Life;									//体力
	int EnemyRecovery;							//復帰時間
	int LipopPozisyon;							//リポップする場所
	int RecoveryCount;							//蘇生カウント
	int EffectPozisyonX, EffectPozisyonY;		//エフェクトポジション
	int EffectCount;							//エフェクトカウンター
	int AAbld;									//前回の左右ごとのAA情報
	int EnemyDeadSE;
	float fPosX;								//エネミーの位置(X座標)
	float fPosY;								//エネミーの位置(Y座標)
	float fPosXold;								//エネミーの前回の位置(X座標)
	float fPosYold;								//エネミーの前回の位置(Y座標)
	float fReconnaissance[EnemyReconnaissance];	//索敵
	float fCount[EnemyCountCase];				//カウント格納ケース
	bool bMove_Left_Right[2];					//動く判定
	bool bPushDOWN;								//降りる判定
	bool bTump;									//ジャンプ中かどうか
	bool bProcess_Judgment[EnemyProcessCase];	//処理終了判定
	bool bPattern[EnemyPattern];				//エネミー行動パターンフラグ管理
	bool bScoreFlag;							//スコアフラグ
	bool bAnnihilationEffect, bLipopEffect, EffectEnd;	//エフェクトフラグ
	bool AAbldData;								//格納タイミングフラグ
}ENEMY;

#endif // !_EnemyStructure_H_
