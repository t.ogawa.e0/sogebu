//***********************************************************
//	HEW作品制作
//	そげぶキラー[main.cpp]
//***********************************************************
#include"main.h"
#include"music.h"
#include"title.h"
#include"Home.h"
#include"player.h"
#include"Enemy.h"
#include"world.h"
#include"Barrett.h"
#include"Explosion.h"
#include"AA.h"
#include"TimeCount.h"
#include"Score.h"
#include"text.h"
#include"result.h"
#include"Loading.h"
#include"Debug.h"

//***********************************************************
//	プロトタイプ宣言
void Init(MAIN *aMain, HOME *aHome, PLAYER *aPlayer, ENEMY *aEnemy, WORLD *aWorld, BARRETT *aBarrett, EXPLOSION *aExplosion, DEBUG *aDebug, AA *aAA, TIME *aTimeCount, TEXT *aText, SCORE *aScore, LOADING *aLoading);		//初期処理
void Update(MAIN *aMain, HOME *aHome, PLAYER *aPlayer, ENEMY *aEnemy, WORLD *aWorld, BARRETT *aBarrett, EXPLOSION *aExplosion, AA *aAA, TIME *aTimeCount, TEXT *aText, SCORE *aScore, LOADING *aLoading);	//更新処理
void Draw(MAIN *aMain, HOME *aHome, PLAYER *aPlayer, ENEMY *aEnemy, WORLD *aWorld, BARRETT *aBarrett, EXPLOSION *aExplosion, DEBUG *aDebug, AA *aAA, TIME *aTimeCount, TEXT *aText, SCORE *aScore, LOADING *aLoading);		//描画処理
void Uninit(void);				//後処理
//***********************************************************
//	グローバル宣言
int g_nCountFPS;	//FPSカウンタ

//***********************************************************
//	メイン関数
void main(void)
{
	DWORD dwExecLastTime;
	DWORD dwFPSLastTime;
	DWORD dwCurrentTime;
	MAIN aMain[1];					//メイン構造
	_FPS aFPS[1];
	TITLE aTitle[title];			//タイトルの構造体
	SCORE aScore[RecordCase];		//スコアの構造体
	PLAYER aPlayer[PlayerCase];		//プレイヤーの構造
	ENEMY aEnemy[EnemyCase];		//エネミーの構造
	WORLD aWorld[Worldcase];		//ステージの構造
	BARRETT aBarrett[1];			//バレットの構造
	EXPLOSION aExplosion[1];		//バレット衝突構造
	AA aAA[_AA];					//アスキーアート構造
	TIME aTimeCount[1];				//タイマー構造
	HOME aHome[HomeCount];			//ホーム構造
	TEXT aText[TEXTsize];			//テキスト構造
	LOADING aLoading[_LOADING_];	//ロード画面構造
	RESULT aResult[_RESULT_];		//リザルト構造
	DEBUG aDebug[1];				//デバッグ

	int nCountFrame;				//フレームカウント
	int nCount, nCountA, nCountB;	//カウント
	int nTitleBGM, nLoadingSE, nLoadingOpnSE, nHomeBGM, nWorldBGM, nResultBGM;	//BGM

	aMain->nAddress = 0;			//アドレスの初期化
	aMain->nRecordAddress = 0;		//記録用アドレス
	aMain->nEnemyCount = 0;			//エネミーの体数初期化
	aMain->nMenutTarget = 0;		//メニューのターゲット初期化
	aMain->nMenuStage = 0;			//ステージ選択の初期化
	aMain->nPlayRecord = 0;			//記録の初期化
	aMain->bIniHome = true;			//ホームのフラグ

	//記録データ用格納用領域初期化
	for (aMain->nRecordAddress = 0; aMain->nRecordAddress < RecordCase; aMain->nRecordAddress++)
	{
		aMain->nPlayIniRecord[aMain->nRecordAddress] = 0;
		for (nCount = 0; nCount < Time_Case; nCount++)
		{
			aMain->nMainIniRecord[nCount] = 0;
			(aScore + aMain->nRecordAddress)->nNewRecord[nCount] = 0;
			(aScore + aMain->nRecordAddress)->nOldNewRecord[nCount] = 0;
			for (nCountA = 0; nCountA < RecordCase; nCountA++)
			{
				(aScore + aMain->nRecordAddress)->nRecord[nCountA] = 0;
				(aScore + aMain->nRecordAddress)->nLogRecord[nCount][nCountA] = 0;
				for (nCountB = 0; nCountB < 3; nCountB++)
				{
					(aScore + aMain->nRecordAddress)->nIniRecord[nCount][nCountA][nCountB] = 0;
					(aScore + aMain->nRecordAddress)->nOldIniNewRecord[nCount][nCountB];
					(aScore + aMain->nRecordAddress)->nIniNewRecord[nCount][nCountB] = 0;
				}
			}
		}
	}
	aMain->nRecordAddress = 0;

	//スコア初期化
	for (nCount = 0; nCount < 3; nCount++)
	{
		aMain->nScore[nCount] = 0;
		aMain->nScoreOld[nCount] = 99;
	}
	aMain->nScoreX = 0;
	aMain->nScoreY = 0;

	//各フラグ初期化
	aMain->bRog = true;
	aMain->bStartGame = true;
	aMain->bGameLiset = true;
	aMain->bHome = false;
	aMain->bHomeLiset = true;
	aMain->bMenuGame = true;
	aMain->bNewRecord = true;

	//切り替えの主キー
	aMain->bPlayeTitle = true;	//優先フラグ
	aMain->bPlayGame = false;
	aMain->bGameEnd = false;
	aMain->bLoad = false;
	aMain->bResult = false;
	aLoading->bComplete = true;
	aResult->bResultComp = true;
	aTitle->bTitleComp = true;

	//BGMフラグ
	bool aTitleBGM = true;
	aWorld->bWorldBGMSTART = true;
	aHome->bHomeBGMSTART = true;

	//タイトルのBGM
	nTitleBGM = OPENWAVE(TitleMusic);   // オープン＆初期化
	if (nTitleBGM == 0)
	{
		nTitleBGM = 0;
	}

	//ロード開始SE
	nLoadingSE = OPENWAVE(LoadingSE);   // オープン＆初期化
	if (nLoadingSE == 0)
	{
		nLoadingSE = 0;
	}

	//ロード終了SE
	nLoadingOpnSE = OPENWAVE(LoadingOpnSE);   // オープン＆初期化
	if (nLoadingOpnSE == 0)
	{
		nLoadingOpnSE = 0;
	}

	//ホームBGM
	nHomeBGM = OPENWAVE(HOMEBGM);   // オープン＆初期化
	if (nHomeBGM == 0)
	{
		nHomeBGM = 0;
	}

	//メニューSE
	aHome->nKeySE = OPENWAVE(HomeKeySE);   // オープン＆初期化
	if (aHome->nKeySE == 0)
	{
		aHome->nKeySE = 0;
	}

	//ステージのBGM
	nWorldBGM = OPENWAVE(WORLDBGM);   // オープン＆初期化
	if (nWorldBGM == 0)
	{
		nWorldBGM = 0;
	}

	//リザルトのBGM
	nResultBGM = OPENWAVE(ResultBGM);   // オープン＆初期化
	if (nResultBGM == 0)
	{
		nResultBGM = 0;
	}

	//バレットのSE
	aBarrett->nBarrettSE = OPENWAVE(BarrettSE);   // オープン＆初期化
	if (aBarrett->nBarrettSE == 0)
	{
		aBarrett->nBarrettSE = 0;
	}

	//エネミー消滅時SE
	aEnemy->EnemyDeadSE = OPENWAVE(ENEMYDEADSE);   // オープン＆初期化
	if (aEnemy->EnemyDeadSE == 0)
	{
		aEnemy->EnemyDeadSE = 0;
	}

	CLS(BLACK, WHITE);

	//分解能を設定
	timeBeginPeriod(1);
	dwExecLastTime = timeGetTime();
	dwFPSLastTime = dwExecLastTime;
	dwCurrentTime = nCountFrame = 0;

	//初期処理(ゲーム)
	CUROFF();
	do
	{
		aTimeCount->bEndGame = true;	//終了判定を無効化
		dwCurrentTime = timeGetTime();
		if ((dwCurrentTime - dwFPSLastTime) >= 500)		//0.5秒ごとに実行
		{
			g_nCountFPS = nCountFrame * 1000 / (dwCurrentTime - dwFPSLastTime);
			dwFPSLastTime = dwCurrentTime;
			aFPS->nFPS = nCountFrame;
			FPS(&aFPS[0]);
			nCountFrame = 0;
		}
		//60分の1秒間隔で更新処理と描画処理を実行する
		if ((dwCurrentTime - dwExecLastTime) >= (1000 / 60))
		{
			dwExecLastTime = dwCurrentTime;
			//タイトル
			if (aMain->bPlayeTitle == true)
			{
				//タイトル初期化
				if (aTitle->bTitleComp == true)
				{
					PLAYWAVE(nTitleBGM, 1);
					IniTitle(&aTitle[0]);

					//初期化終了
					aTitle->bTitleComp = false;
				}

				//初期化終了したので処理開始
				if (aTitle->bTitleComp == false)
				{
					UpdateTitle(&aTitle[0], &aHome[0], &aMain[0]);
					DrawTitle(&aTitle[0]);
				}
			}

			//ロード
			if (aMain->bLoad == true)
			{
				//ロードの初期化
				if (aLoading->bComplete == true)
				{
					STOPWAVE(nTitleBGM);
					STOPWAVE(nHomeBGM);
					STOPWAVE(nWorldBGM);
					STOPWAVE(nResultBGM);
					PLAYWAVE(nLoadingSE, 0);
					InitLoading(&aLoading[0], &aMain[0]);

					//初期化終了
					aLoading->bComplete = false;
				}

				//初期化が終了したので処理開始
				if (aLoading->bComplete == false)
				{
					UpdateLoading(&aLoading[0], &aMain[0]);
					if (aLoading->bOpenDraw == false)
					{
						STOPWAVE(nLoadingSE);
					}
					if (aLoading->bOpnSE == false)
					{
						PLAYWAVE(nLoadingOpnSE, 0);
						aLoading->bOpnSE = true;
					}
					DrawLoading(&aLoading[0], &aMain[0]);
				}
			}

			//リザルト
			if (aMain->bResult == true)
			{
				//リザルトの初期化開始
				if (aResult->bResultComp == true)
				{
					STOPWAVE(nLoadingOpnSE);
					PLAYWAVE(nResultBGM, 1);
					IniResult(&aResult[0]);
					aResult->bResultComp = false;
				}

				//初期化が終了したので処理開始
				if (aResult->bResultComp == false)
				{
					UpdateResult(&aResult[0], &aHome[0], &aMain[0]);
					DrawResult(&aResult[0], &aText[0], &aTimeCount[0], &aMain[0], &aScore[0]);
				}
			}

			//ゲーム
			if (aMain->bPlayGame == true)
			{
				//初期化
				Init(&aMain[0], &aHome[0], &aPlayer[0], &aEnemy[0], &aWorld[0], &aBarrett[0], &aExplosion[0], &aDebug[0], &aAA[0], &aTimeCount[0], &aText[0], &aScore[0], &aLoading[0]);

				//更新処理
				Update(&aMain[0], &aHome[0], &aPlayer[0], &aEnemy[0], &aWorld[0], &aBarrett[0], &aExplosion[0], &aAA[0], &aTimeCount[0], &aText[0], &aScore[0], &aLoading[0]);

				//描画処理
				Draw(&aMain[0], &aHome[0], &aPlayer[0], &aEnemy[0], &aWorld[0], &aBarrett[0], &aExplosion[0], &aDebug[0], &aAA[0], &aTimeCount[0], &aText[0], &aScore[0], &aLoading[0]);
				if (aHome->bHomeBGMSTART == false)
				{
					if (aMain->bLoad == false)
					{
						STOPWAVE(nLoadingOpnSE);
						PLAYWAVE(nHomeBGM, 1);
						aHome->bHomeBGMSTART = true;
					}
				}
				if (aWorld->bWorldBGMSTART == false)
				{
					if (aMain->bLoad == false)
					{
						STOPWAVE(nLoadingOpnSE);
						PLAYWAVE(nWorldBGM, 1);
						aWorld->bWorldBGMSTART = true;
					}
				}
			}
			nCountFrame++;
		}
	} while (!(aTimeCount->bEndGame == false));

	//後処理
	Uninit();

	//分解能を戻す
	timeEndPeriod(1);
}

//***********************************************************
//	初期処理
void Init(MAIN *aMain, HOME *aHome, PLAYER *aPlayer, ENEMY *aEnemy, WORLD *aWorld, BARRETT *aBarrett, EXPLOSION *aExplosion, DEBUG *aDebug, AA *aAA, TIME *aTimeCount, TEXT *aText, SCORE *aScore, LOADING *aLoading)
{
	//ホーム
	if (aMain->bHome == false)
	{
		if (aMain->bHomeLiset == true)
		{
			//ホーム
			InitHome(&aHome[0], &aTimeCount[0], &aText[0], &aMain[0], &aScore[0]);
			aMain->bHomeLiset = false;
			aMain->bStartGame = true;
			aMain->bGameLiset = true;
			aHome->bHomeBGMSTART = false;
		}
	}

	//ゲーム
	if (aMain->bStartGame == false)
	{
		if (aMain->bGameLiset == true)
		{
			aMain->nScoreX = ScorePositionX;
			aMain->nScoreY = ScorePositionY;
			InitScore(&aMain[0], &aScore[0]);

			InitAA(&aAA[0]);

			//ワールド処理
			InitWorld(&aMain[0], &aWorld[0], &aHome[0], &aText[0]);

			//エネミー処理
			while (!(aMain->nAddress == aMain->nEnemyCount))
			{
				InitEnemy(&aMain[0], &aEnemy[0], &aWorld[0]);
				aMain->nAddress++;
			}
			aMain->nAddress = 0;

			//プレイヤー処理
			InitPlayer(&aPlayer[0], &aWorld[0]);

			//バレット処理
			InitBarrett(&aBarrett[0]);

			//バレット弾着処理
			InitExplosion(&aExplosion[0]);

			//タイム処理
			InitTime(&aTimeCount[0]);

			//			InitDebug(&aDebug[0]);
			aMain->bGameLiset = false;
		}
	}
}

//***********************************************************
//	更新処理(60秒分の1秒単位で更新)
void Update(MAIN *aMain, HOME *aHome, PLAYER *aPlayer, ENEMY *aEnemy, WORLD *aWorld, BARRETT *aBarrett, EXPLOSION *aExplosion, AA *aAA, TIME *aTimeCount, TEXT *aText, SCORE *aScore, LOADING *aLoading)
{
	//ホーム更新処理
	if (aMain->bHomeLiset == false)
	{
		UpdateHome(&aHome[0], &aTimeCount[0], &aText[0], &aMain[0]);
	}

	//ゲーム更新処理
	if (aMain->bGameLiset == false)
	{
		//スコア処理
		ScoreProcess(&aMain[0], &aScore[0]);

		//タイム処理
		UpdateTime(&aTimeCount[0], &aMain[0], &aScore[0]);

		//ワールド処理
		UpdateWorld(&aWorld[0], &aPlayer[0]);

		//エネミー処理
		while (!(aMain->nAddress == aMain->nEnemyCount))
		{
			UpdateEnemy(&aMain[0], &aPlayer[0], &aEnemy[0], &aWorld[0], &aAA[0], &aScore[0]);
			aMain->nAddress++;
		}

		//アドレスリセット
		aMain->nAddress = 0;

		//プレイヤー処理
		UpdatePlayer(&aPlayer[0], &aWorld[0], &aAA[0]);

		//バレット処理
		UpdateBarrett(&aMain[0], &aBarrett[0], &aPlayer[0], &aAA[0], &aEnemy[0], &aWorld[0]);

		//爆さん処理
		UpdateExplosion(&aBarrett[0], &aExplosion[0], &aPlayer[0]);
	}
}

//***********************************************************
//	描画処理(60秒分の1秒単位で更新)
void Draw(MAIN *aMain, HOME *aHome, PLAYER *aPlayer, ENEMY *aEnemy, WORLD *aWorld, BARRETT *aBarrett, EXPLOSION *aExplosion, DEBUG *aDebug, AA *aAA, TIME *aTimeCount, TEXT *aText, SCORE *aScore, LOADING *aLoading)
{
	//ホーム描画処理
	if (aMain->bHomeLiset == false)
	{
		DrawHome(&aHome[0], &aTimeCount[0], &aText[0], &aMain[0], &aScore[0]);
	}

	//ゲーム描画処理
	if (aMain->bGameLiset == false)
	{
		aTimeCount->nTimePozi = TimePozi;
		aTimeCount->nTimeHeit = TimeHeit;

		//スコア表示
		COLOR(BLACK, WHITE);
		Score(&aMain[0]);

		//タイム処理
		COLOR(BLACK, WHITE);
		DrawTime(&aTimeCount[0]);

		//プレイヤー処理
		DrawPlayer(&aPlayer[0], &aAA[0], &aMain[0], &aScore[0]);

		//エネミー処理
		while (!(aMain->nAddress == aMain->nEnemyCount))
		{
			DrawEnemy(&aMain[0], &aEnemy[0], &aAA[0]);
			aMain->nAddress++;
		}

		//アドレス戻し
		aMain->nAddress = 0;

		//ワールド処理
		DrawWorld(&aMain[0], &aWorld[0], &aPlayer[0], &aEnemy[0]);

		//バレット処理
		DrawBarrett(&aBarrett[0]);

		//爆発処理
		DrawExplosion(&aBarrett[0], &aExplosion[0]);

		//デバッグ用
//		Debug(&aMain[0], &aPlayer[0], &aEnemy[0], &aWorld[0], &aBarrett[0], &aExplosion[0], &aDebug[0]);
	}
}

//***********************************************************
//	後処理
void Uninit(void)
{
	//ワールド処理
	UninitWorld();

	//エネミー処理
	UninitEnemy();

	//プレイヤー処理
	UninitPlayer();

	//バレット処理
	UninitBarrett();

	UninitExplosion();
}

//***********************************************************
//	FPS表示
void FPS(_FPS *aFPS)
{
	LOCATE(2, 2);
	aFPS->nFPS = aFPS->nFPS * 2;
	if (60 < aFPS->nFPS)
	{
		aFPS->nFPS = 60;
	}
	if (0 <= aFPS->nFPS)
	{
		COLOR(H_RED, BLACK);
	}
	if (20 <= aFPS->nFPS)
	{
		COLOR(H_YELLOW, BLACK);
	}
	if (40 <= aFPS->nFPS)
	{
		COLOR(H_GREEN, BLACK);
	}
	printf(" FPS : %2d ", aFPS->nFPS);
}