//***********************************************************
//	HEW作品制作
//	そげぶキラー[Loading.cpp]
//***********************************************************
#include"main.h"
#include"Loading.h"

//***********************************************************
//　初期化
void InitLoading(LOADING *aLoading, MAIN *aMain)
{
	//数値初期化
	aLoading->nLoadX = 0;
	aLoading->nLoadY = 1;
	aLoading->nLoadLimitX = 0;
	aLoading->nLoadLimitY = 1;
	aLoading->nLoadCount = 0;
	aLoading->nLoadbarLimit = 0;
	aLoading->fLoadCount = 0.1;
	aLoading->nLimitCount = 0;

	//各フラグ初期化
	aLoading->bLoadOpen = true;
	aLoading->bOpenDraw = true;
	aLoading->bLoadMove = true;
	aLoading->bLoadDraw = true;
	aLoading->bOpenTime = true;
	aLoading->bDrawTime = false;
	aLoading->bOpnSE = true;
	aLoading->bOpn = true;
}

//***********************************************************
//　更新
void UpdateLoading(LOADING *aLoading, MAIN *aMain)
{
	//ロード画面処理表示処理開始
	//ロード画面がまだ完全に表示されていないとき
	if (aLoading->bLoadMove == true)
	{
		//ロード画面描画に必要な時間と一致したとき
		if (aLoading->nLimitCount >= LOADMOVEFRAMERIMIT + 1)
		{
			//初期化
			aLoading->nLoadCount = 0;
			aLoading->nLimitCount = 0;

			//描画座標
			aLoading->nLoadLimitY = LOADINGMOVELIMIT_Y;
			aLoading->nLoadY = LOADINGMOVELIMIT_Y;

			//ロード画面表示処理終了
			aLoading->bLoadMove = false;
		}
	}

	//ロード画面表示速度制限処理
	//ロード画面がまだ完全に表示されていないとき
	if (aLoading->bLoadMove == true)
	{
		//現在の処理時間がロード画面描画に必要な時間に満たしていないとき
		if (!(aLoading->nLimitCount >= LOADMOVEFRAMERIMIT + 1))
		{
			//描画速度制御
			if (aLoading->nLoadCount == LOADMOVEFRAME)
			{
				//描画する上限を変動させる
				aLoading->nLoadLimitY += LOADINGMOVE;

				//カウント0
				aLoading->nLoadCount = 0;

				//描画開始
				aLoading->bLoadDraw = false;
			}
		}

		//カウント
		aLoading->nLoadCount++;
		aLoading->nLimitCount++;
	}

	//ロード時間処理
	//ロード画面が完全に表示されたとき
	if (aLoading->bLoadMove == false)
	{
		//ロードが終了していないとき
		if (aLoading->bOpenTime == true)
		{
			//必要なロード時間と現在の処理時間が一致したとき
			if (aLoading->nLimitCount == (LOADMOVEFRAMERIMIT * LOADTIME) + 1)
			{
				//初期化
				aLoading->nLoadCount = 0;
				aLoading->nLimitCount = 0;

				//ロード画面消去処理開始
				aLoading->bOpenTime = false;
				aLoading->bLoadOpen = true;
			}
		}
	}

	//ロード時間処理
	//ロードが終了していないとき
	if (aLoading->bOpenTime == true)
	{
		//現在の経過時間に応じてバーの処理を行う
		switch (aLoading->nLimitCount)
		{
		case (int((LOADMOVEFRAMERIMIT * LOADTIME)*0.1)):
			aLoading->bDrawTime = true;
			aLoading->nLoadbarLimit = (int(75 + (150 * 0.1)));
			break;
		case (int((LOADMOVEFRAMERIMIT * LOADTIME)*0.2)):
			aLoading->bDrawTime = true;
			aLoading->nLoadbarLimit = (int(75 + (150 * 0.2)));
			break;
		case (int((LOADMOVEFRAMERIMIT * LOADTIME)*0.3)):
			aLoading->bDrawTime = true;
			aLoading->nLoadbarLimit = (int(75 + (150 * 0.3)));
			break;
		case (int((LOADMOVEFRAMERIMIT * LOADTIME)*0.4)):
			aLoading->bDrawTime = true;
			aLoading->nLoadbarLimit = (int(75 + (150 * 0.4)));
			break;
		case (int((LOADMOVEFRAMERIMIT * LOADTIME)*0.5)):
			aLoading->bDrawTime = true;
			aLoading->nLoadbarLimit = (int(75 + (150 * 0.5)));
			break;
		case (int((LOADMOVEFRAMERIMIT * LOADTIME)*0.6)):
			aLoading->bDrawTime = true;
			aLoading->nLoadbarLimit = (int(75 + (150 * 0.6)));
			break;
		case (int((LOADMOVEFRAMERIMIT * LOADTIME)*0.7)):
			aLoading->bDrawTime = true;
			aLoading->nLoadbarLimit = (int(75 + (150 * 0.7)));
			break;
		case (int((LOADMOVEFRAMERIMIT * LOADTIME)*0.8)):
			aLoading->bDrawTime = true;
			aLoading->nLoadbarLimit = (int(75 + (150 * 0.8)));
			break;
		case (int((LOADMOVEFRAMERIMIT * LOADTIME)*0.9)):
			aLoading->bDrawTime = true;
			aLoading->nLoadbarLimit = (int(75 + (150 * 0.9)));
			break;
		case (int((LOADMOVEFRAMERIMIT * LOADTIME)*1.0)):
			aLoading->bDrawTime = true;
			aLoading->nLoadbarLimit = (int(75 + (150 * 1.0)));
			break;
		default:
			break;
		}
	}

	//ロード画面消去処理
	//ロード画面が完全に表示されたとき
	if (aLoading->bLoadMove == false)
	{
		//ロードが終了したとき
		if (aLoading->bOpenTime == false)
		{
			//ロード画面が完全に消去されていないとき
			if (aLoading->bLoadOpen == true)
			{
				//必要な消去時間と現在の処理時間が一致したとき
				if (!(aLoading->nLimitCount >= LOADMOVEFRAMERIMIT + 1))
				{
					//消去速度制御
					if (aLoading->nLoadCount == LOADMOVEFRAME)
					{
						//描画上限の変動
						aLoading->nLoadLimitY -= LOADINGMOVE;
						aLoading->nLoadCount = 0;

						//消去開始
						aLoading->bOpenDraw = false;
						if (aLoading->bOpn == true)
						{
							aLoading->bOpnSE = false;
							aLoading->bOpn = false;
						}
					}
				}
			}
		}
		//カウント
		aLoading->nLoadCount++;
		aLoading->nLimitCount++;
	}
}

//***********************************************************
//　描画
void DrawLoading(LOADING *aLoading, MAIN *aMain)
{
	int nCount, nCountA, nCountB;

	nCount = 0;
	nCountA = 0;
	nCountB = 0;

	//ロード画面描画開始
	if (aLoading->bLoadDraw == false)
	{
		for (aLoading->nLoadY; aLoading->nLoadY < aLoading->nLoadLimitY; aLoading->nLoadY++)
		{
			for (nCount = 1; nCount < LOADINGMOVELIMIT_X; nCount += 150)
			{
				COLOR(BLACK, BLACK);
				//ロード時間を表す初期バーの描画
				if ((LOADINGMOVELIMIT_Y - (((LOADINGMOVELIMIT_Y / 5) / 5) - 2)) <= aLoading->nLoadY)
				{
					for (nCountB = 75; nCountB < 225; nCountB += 2)
					{
						LOCATE(nCountB, 48);
						COLOR(L_RED, BLACK);
						printf("―");
					}
					COLOR(H_CYAN, H_CYAN);
				}
				LOCATE(nCount, aLoading->nLoadY);
				printf("　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　");
				if (aLoading->nLoadY <= (LOADINGMOVELIMIT_Y - 3))
				{
					LOCATE(nCount, (aLoading->nLoadY + 2));
					COLOR(H_CYAN, H_CYAN);
					printf("　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　");
				}
			}
		}
		//描画終了
		aLoading->bLoadDraw = true;
	}

	//ロードバー描画開始
	if (aLoading->bDrawTime == true)
	{
		for (nCountB = 75; nCountB <= aLoading->nLoadbarLimit; nCountB += 2)
		{
			LOCATE(nCountB, 47);
			COLOR(H_CYAN, BLACK);
			printf("―");
		}
		//描画終了
		aLoading->bDrawTime = false;
	}

	//ロード画面消去開始
	if (aLoading->bOpenDraw == false)
	{
		for (aLoading->nLoadY; aLoading->nLoadLimitY <= aLoading->nLoadY; aLoading->nLoadY--)
		{
			for (nCount = 1; nCount < LOADINGMOVELIMIT_X; nCount += 150)
			{
				if (3 <= aLoading->nLoadY)
				{
					LOCATE(nCount, (aLoading->nLoadY - 2));
					COLOR(H_CYAN, H_CYAN);
					printf("　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　");
				}

				LOCATE(nCount, aLoading->nLoadY);
				COLOR(WHITE, WHITE);
				printf("　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　");
			}
		}

		//ロード終了と同時に各フラグ操作を行う
		if (aLoading->nLoadY <= 0)
		{
			//ゲーム終了時のフラグ操作
			if (aMain->bGameEnd == true)
			{
				aLoading->bOpn = true;
				aMain->bLoad = false;
				aLoading->bComplete = true;
				aMain->bPlayGame = false;
				aMain->bResult = true;
			}

			//リザルト終了時のフラグ操作
			if (aMain->bGameEnd == false)
			{
				aLoading->bOpn = true;
				aMain->bLoad = false;
				aLoading->bComplete = true;
				aMain->bPlayGame = true;
			}
		}

		//消去終了
		aLoading->bOpenDraw = true;
	}
}