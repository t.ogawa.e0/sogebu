//***********************************************************
//	HEW作品制作
//	そげぶキラー[world.cpp]
//***********************************************************
#include"main.h"
#include"world.h"
#include"Enemy.h"

//***********************************************************
//ワールド初期処理
void InitWorld(MAIN *aMain, WORLD *aWorld, HOME *aHome, TEXT *aText)
{
	int nCount, height, nScaffoldCount;

	//BGMスタートフラグ初期化
	aWorld->bWorldBGMSTART = true;

	//ステージ選択に合わせた初期配置に必要な数値代入
	switch (aHome->nMenuStage)
	{
	case 0:
		StageA(&aWorld[0], &aHome[0]);
		aMain->nEnemyCount = EnemyCase;
		break;
	case 1:
		StageB(&aWorld[0], &aHome[0]);
		aMain->nEnemyCount = EnemyCase;
		break;
	case 2:
		StageC(&aWorld[0], &aHome[0]);
		aMain->nEnemyCount = EnemyCase;
		break;
	case 3:
		StageD(&aWorld[0], &aHome[0]);
		aMain->nEnemyCount = EnemyCase;
		break;
	default:
		StageA(&aWorld[0], &aHome[0]);
		aMain->nEnemyCount = EnemyCase;
		break;
	}
	COLOR(BLACK, WHITE);

	//上下壁描画処理
	for (nCount = LeftWall_X + 1; nCount <= RightWall_X - 2; nCount += 2)
	{
		LOCATE(nCount, TopWall);
		printf("□");
		LOCATE(nCount, UnderWall);
		printf("□");
	}

	//空中床描画処理
	for (nScaffoldCount = 0; nScaffoldCount < aWorld->nScaffold; nScaffoldCount++)
	{
		for (nCount = aWorld->fScaffold[nScaffoldCount][0]; nCount < aWorld->fScaffold[nScaffoldCount][1]; nCount += 2)
		{
			LOCATE(nCount, aWorld->fScaffold[nScaffoldCount][2]);
			printf("□");
		}
	}

	//左右壁描画処理
	for (nCount = UnderWall - (UnderWall - 1); nCount <= UnderWall; nCount++)
	{
		LOCATE(LeftWall_X, nCount);
		printf("|");
		LOCATE(RightWall_X, nCount);
		printf("|");
	}

	//時間配置座標
	aText->n_WindTimeX = 5;
	aText->n_WindTimeY = 90;

	//時間初期描画
	textWintTime(&aText[0]);

	//ライフ配置座標
	aText->n_TextLifeX = 158;
	aText->n_TextLifeY = 88;

	//ライフ初期描画
	textLife(&aText[0]);

	//各情報の区切り線描画
	for (nCount = UnderWall + 2; nCount < 95; nCount++)
	{
		COLOR(H_CYAN, H_CYAN);
		LOCATE(TopWall, nCount);
		printf("□");
		LOCATE(110, nCount);
		printf("□");
		LOCATE(150, nCount);
		printf("□");
		LOCATE(198, nCount);
		printf("□");
		LOCATE(200, nCount);
		printf("□");
		LOCATE(278, nCount);
		printf("□");
		LOCATE(280, nCount);
		printf("□");
	}
	COLOR(BLACK, WHITE);

	//BGMスタート
	aWorld->bWorldBGMSTART = false;
}

//***********************************************************
//ワールド更新処理
void UpdateWorld(WORLD *aWorld, PLAYER *aPlayer)
{
	//更新情報無し
}

//***********************************************************
//ワールド描画処理
void DrawWorld(MAIN *aMain, WORLD *aWorld, PLAYER *aPlayer, ENEMY *aEnemy)
{
	int nScaffoldCount, nCount;

	COLOR(BLACK, WHITE);

	//エネミーの数とアドレスが一致している間
	while (!(aMain->nAddress == aMain->nEnemyCount))
	{
		//限定的な描画[エネミー用]
		if (!((aEnemy + aMain->nAddress)->fPosYold == (aEnemy + aMain->nAddress)->fPosY))
		{
			//空中の床と空中の床最大値が一致するまで
			for (nScaffoldCount = 0; nScaffoldCount < aWorld->nScaffold; nScaffoldCount++)
			{
				//現在参照中の床[X]とエネミーの座標[X]が一致したとき
				if ((aWorld->fScaffold[nScaffoldCount][0] - 1.5 <= (aEnemy + aMain->nAddress)->fPosX) && ((aEnemy + aMain->nAddress)->fPosX <= aWorld->fScaffold[nScaffoldCount][1] + 0.5))
				{
					//現在参照中の床[Y]に更新範囲を+-した時の数値内にエネミーの座標[Y]が存在するとき
					if (((aWorld->fScaffold[nScaffoldCount][2] - Update_Enemy) <= (aEnemy + aMain->nAddress)->fPosY) && ((aEnemy + aMain->nAddress)->fPosY <= (aWorld->fScaffold[nScaffoldCount][2] + Update_Enemy)))
					{
						//床描画開始地点から終了地点までループ
						for (nCount = aWorld->fScaffold[nScaffoldCount][0]; nCount < aWorld->fScaffold[nScaffoldCount][1]; nCount += 2)
						{
							//再描画開始地点と再描画終了地点の間の時に描画
							if ((((aEnemy + aMain->nAddress)->fPosX - ReloadDraw) <= nCount) && (nCount <= ((aEnemy + aMain->nAddress)->fPosX + ReloadDraw)))
							{
								LOCATE(nCount, aWorld->fScaffold[nScaffoldCount][2]);
								printf("□");
							}
						}
					}
				}
			}
		}
		//アドレスを更新
		aMain->nAddress++;
	}
	//アドレスを初期化
	aMain->nAddress = 0;

	//限定的な描画[プレイヤー用]
	if (!(aPlayer->fPosYold == aPlayer->fPosY))
	{
		//空中の床と空中の床最大値が一致するまで
		for (nScaffoldCount = 0; nScaffoldCount < aWorld->nScaffold; nScaffoldCount++)
		{
			//現在参照中の床[X]とプレイヤーの座標[X]が一致したとき
			if ((aWorld->fScaffold[nScaffoldCount][0] - PlayerReloadright <= aPlayer->fPosX) && (aPlayer->fPosX <= aWorld->fScaffold[nScaffoldCount][1] + PlayerReloadleft))
			{
				//現在参照中の床[Y]に更新範囲を+-した時の数値内にプレイヤーの座標[Y]が存在するとき
				if (((aWorld->fScaffold[nScaffoldCount][2] - Update_Player) <= aPlayer->fPosY) && (aPlayer->fPosY <= (aWorld->fScaffold[nScaffoldCount][2] + Update_Player)))
				{
					//床描画開始地点から終了地点までループ
					for (nCount = aWorld->fScaffold[nScaffoldCount][0]; nCount < aWorld->fScaffold[nScaffoldCount][1]; nCount += 2)
					{
						//再描画開始地点と再描画終了地点の間の時に描画
						if (((aPlayer->fPosX - ReloadDraw) <= nCount) && (nCount <= (aPlayer->fPosX + ReloadDraw)))
						{
							LOCATE(nCount, aWorld->fScaffold[nScaffoldCount][2]);
							printf("□");
						}
					}
				}
			}
		}
	}
}

//***********************************************************
//ワールド後処理
void UninitWorld(void) {}

//***********************************************************
//ステージ1
void StageA(WORLD *aWorld, HOME *aHome)
{
	int nCount, height;

	//********************************************************************************
	//	足場製作ルール。
	//	高さは数位の小さい順で並べましょう。
	//	プレイ上のバグは確認されませんが、制作上でバグが発生します。
	//	足場の長さ、高さは左右上下の壁を超えないようお願いします。
	//	エネミーの初期座標入力は体数分用意してください。バグの原因となります。
	//	バク回避として、足場の最終値を飛ばした後、追記で一つ増やしてください。
	//	aWorld[Worldcase_to_Use].fScaffold[番地][線の始まり] = X;
	//	aWorld[Worldcase_to_Use].fScaffold[番地][線の終わり] = X;
	//	aWorld[Worldcase_to_Use].fScaffold[番地][線の高さ] = Y;
	//	aWorld->fPlayer_Start_PositionX　←プレイヤーのX座標初期配置
	//	aWorld->fPlayer_Start_PositionY　←プレイヤーのY座標初期配置
	//	aWorld->fEnemy_Start_Position[1][何体目]　←エネミーのX座標初期配置
	//	aWorld->fEnemy_Start_Position[0][何体目]　←エネミーのY座標初期配置
	//	aWorld->nScaffold　←制作した足場の合計値格納ケース
	//********************************************************************************

	//１本目
	nCount = 0;
	height = 15;
	aWorld->fScaffold[nCount][0] = 2;
	aWorld->fScaffold[nCount][1] = 299;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fPlayer_Start_PositionX = (aWorld->fScaffold[nCount][0] + 20);
	aWorld->fPlayer_Start_PositionY = height - 1;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//2本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 52;
	aWorld->fScaffold[nCount][1] = 249;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][7] = height - 1;
	aWorld->fEnemy_Start_Position[1][7] = (aWorld->fScaffold[nCount][0] / 2);
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//3本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 2;
	aWorld->fScaffold[nCount][1] = 299;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][8] = height - 1;
	aWorld->fEnemy_Start_Position[1][8] = (aWorld->fScaffold[nCount][0] / 2);
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//4本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 75;
	aWorld->fScaffold[nCount][1] = 225;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][9] = height - 1;
	aWorld->fEnemy_Start_Position[1][9] = (aWorld->fScaffold[nCount][0] / 2);
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//5本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 2;
	aWorld->fScaffold[nCount][1] = 299;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//6本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 2;
	aWorld->fScaffold[nCount][1] = 299;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//7本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 50;
	aWorld->fScaffold[nCount][1] = 249;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);
	aWorld->fEnemy_Start_Position[0][10] = UnderWall - 1;
	aWorld->fEnemy_Start_Position[1][10] = (RightWall_X / 2);

	//空中床の最大数
	aWorld->nScaffold = nCount + 1;

	//バグ回避
	nCount += 1;
	aWorld->fScaffold[nCount][0] = 2;
	aWorld->fScaffold[nCount][1] = 299;
	aWorld->fScaffold[nCount][2] = UnderWall;
}

//***********************************************************
//ステージ2
void StageB(WORLD *aWorld, HOME *aHome)
{
	int nCount, height;

	//********************************************************************************
	//	足場製作ルール。
	//	高さは数位の小さい順で並べましょう。
	//	プレイ上のバグは確認されませんが、制作上でバグが発生します。
	//	足場の長さ、高さは左右上下の壁を超えないようお願いします。
	//	エネミーの初期座標入力は体数分用意してください。バグの原因となります。
	//	バク回避として、足場の最終値を飛ばした後、追記で一つ増やしてください。
	//	aWorld[Worldcase_to_Use].fScaffold[番地][線の始まり] = X;
	//	aWorld[Worldcase_to_Use].fScaffold[番地][線の終わり] = X;
	//	aWorld[Worldcase_to_Use].fScaffold[番地][線の高さ] = Y;
	//	aWorld->fPlayer_Start_PositionX　←プレイヤーのX座標初期配置
	//	aWorld->fPlayer_Start_PositionY　←プレイヤーのY座標初期配置
	//	aWorld->fEnemy_Start_Position[1][何体目]　←エネミーのX座標初期配置
	//	aWorld->fEnemy_Start_Position[0][何体目]　←エネミーのY座標初期配置
	//	aWorld->nScaffold　←制作した足場の合計値格納ケース
	//********************************************************************************

	//1本目
	nCount = 0;
	height = 15;
	aWorld->fScaffold[nCount][0] = 2;
	aWorld->fScaffold[nCount][1] = 100;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fPlayer_Start_PositionX = (aWorld->fScaffold[nCount][0] + 20);
	aWorld->fPlayer_Start_PositionY = height - 1;
	aWorld->fEnemy_Start_Position[0][nCount] = height + 5;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//2本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 70;
	aWorld->fScaffold[nCount][1] = 168;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][7] = height - 1;
	aWorld->fEnemy_Start_Position[1][7] = (aWorld->fScaffold[nCount][0] / 2);
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//3本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 138;
	aWorld->fScaffold[nCount][1] = 236;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][8] = height - 1;
	aWorld->fEnemy_Start_Position[1][8] = (aWorld->fScaffold[nCount][0] / 2);
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//4本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 2;
	aWorld->fScaffold[nCount][1] = 299;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][9] = height - 1;
	aWorld->fEnemy_Start_Position[1][9] = (aWorld->fScaffold[nCount][0] / 2);
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//5本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 138;
	aWorld->fScaffold[nCount][1] = 236;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//6本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 70;
	aWorld->fScaffold[nCount][1] = 168;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//7本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 2;
	aWorld->fScaffold[nCount][1] = 299;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);
	aWorld->fEnemy_Start_Position[0][10] = UnderWall - 1;
	aWorld->fEnemy_Start_Position[1][10] = (RightWall_X / 2);

	//空中床の最大数
	aWorld->nScaffold = nCount + 1;

	//バグ回避
	nCount += 1;
	aWorld->fScaffold[nCount][0] = 2;
	aWorld->fScaffold[nCount][1] = 299;
	aWorld->fScaffold[nCount][2] = UnderWall;
}

//***********************************************************
//ステージ3
void StageC(WORLD *aWorld, HOME *aHome)
{
	int nCount, height;

	//********************************************************************************
	//	足場製作ルール。
	//	高さは数位の小さい順で並べましょう。
	//	プレイ上のバグは確認されませんが、制作上でバグが発生します。
	//	足場の長さ、高さは左右上下の壁を超えないようお願いします。
	//	エネミーの初期座標入力は体数分用意してください。バグの原因となります。
	//	バク回避として、足場の最終値を飛ばした後、追記で一つ増やしてください。
	//	aWorld[Worldcase_to_Use].fScaffold[番地][線の始まり] = X;
	//	aWorld[Worldcase_to_Use].fScaffold[番地][線の終わり] = X;
	//	aWorld[Worldcase_to_Use].fScaffold[番地][線の高さ] = Y;
	//	aWorld->fPlayer_Start_PositionX　←プレイヤーのX座標初期配置
	//	aWorld->fPlayer_Start_PositionY　←プレイヤーのY座標初期配置
	//	aWorld->fEnemy_Start_Position[1][何体目]　←エネミーのX座標初期配置
	//	aWorld->fEnemy_Start_Position[0][何体目]　←エネミーのY座標初期配置
	//	aWorld->nScaffold　←制作した足場の合計値格納ケース
	//********************************************************************************

	//1本目
	nCount = 0;
	height = 15;
	aWorld->fScaffold[nCount][0] = 2;
	aWorld->fScaffold[nCount][1] = 299;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fPlayer_Start_PositionX = (aWorld->fScaffold[nCount][0] + 20);
	aWorld->fPlayer_Start_PositionY = height - 1;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//2本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 2;
	aWorld->fScaffold[nCount][1] = 70;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][7] = height - 1;
	aWorld->fEnemy_Start_Position[1][7] = (aWorld->fScaffold[nCount][0] / 2);
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//3本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 60;
	aWorld->fScaffold[nCount][1] = 130;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][8] = height - 1;
	aWorld->fEnemy_Start_Position[1][8] = (aWorld->fScaffold[nCount][0] / 2);
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//4本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 2;
	aWorld->fScaffold[nCount][1] = 190;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][9] = height - 1;
	aWorld->fEnemy_Start_Position[1][9] = (aWorld->fScaffold[nCount][0] / 2);
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//5本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 180;
	aWorld->fScaffold[nCount][1] = 250;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//6本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 240;
	aWorld->fScaffold[nCount][1] = 299;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//7本目
	nCount += 1;
	height += 10;
	aWorld->fScaffold[nCount][0] = 2;
	aWorld->fScaffold[nCount][1] = 299;
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);
	aWorld->fEnemy_Start_Position[0][10] = UnderWall - 1;
	aWorld->fEnemy_Start_Position[1][10] = (RightWall_X / 2);

	//空中床の最大数
	aWorld->nScaffold = nCount + 1;

	//バグ回避
	nCount += 1;
	aWorld->fScaffold[nCount][0] = 2;
	aWorld->fScaffold[nCount][1] = 299;
	aWorld->fScaffold[nCount][2] = UnderWall;
}

//***********************************************************
//ステージ4
void StageD(WORLD *aWorld, HOME *aHome)
{
	int nCount, height;

	//********************************************************************************
	//	足場製作ルール。
	//	高さは数位の小さい順で並べましょう。
	//	プレイ上のバグは確認されませんが、制作上でバグが発生します。
	//	足場の長さ、高さは左右上下の壁を超えないようお願いします。
	//	エネミーの初期座標入力は体数分用意してください。バグの原因となります。
	//	バク回避として、足場の最終値を飛ばした後、追記で一つ増やしてください。
	//	aWorld[Worldcase_to_Use].fScaffold[番地][線の始まり] = X;
	//	aWorld[Worldcase_to_Use].fScaffold[番地][線の終わり] = X;
	//	aWorld[Worldcase_to_Use].fScaffold[番地][線の高さ] = Y;
	//	aWorld->fPlayer_Start_PositionX　←プレイヤーのX座標初期配置
	//	aWorld->fPlayer_Start_PositionY　←プレイヤーのY座標初期配置
	//	aWorld->fEnemy_Start_Position[1][何体目]　←エネミーのX座標初期配置
	//	aWorld->fEnemy_Start_Position[0][何体目]　←エネミーのY座標初期配置
	//	aWorld->nScaffold　←制作した足場の合計値格納ケース
	//********************************************************************************

	//1本目
	nCount = 0;
	height = 18;
	aWorld->fScaffold[nCount][0] = (int(RightWall_X*0.05));
	aWorld->fScaffold[nCount][1] = (int(RightWall_X*0.95));
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fPlayer_Start_PositionX = (aWorld->fScaffold[nCount][0] + 20);
	aWorld->fPlayer_Start_PositionY = height - 1;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//2本目
	nCount++;
	height += 10;
	aWorld->fScaffold[nCount][0] = (int(RightWall_X*0.7));
	aWorld->fScaffold[nCount][1] = (int(RightWall_X*0.9));
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//3本目
	nCount++;
	aWorld->fScaffold[nCount][0] = (int(RightWall_X*0.05));
	aWorld->fScaffold[nCount][1] = (int(RightWall_X*0.4));
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//4本目
	height += 5;
	nCount++;
	aWorld->fScaffold[nCount][0] = (int(RightWall_X*0.5));
	aWorld->fScaffold[nCount][1] = (int(RightWall_X*0.7));
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//5本目
	height += 5;
	nCount++;
	aWorld->fScaffold[nCount][0] = (int(RightWall_X*0.3));
	aWorld->fScaffold[nCount][1] = (int(RightWall_X*0.5));
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//6本目
	nCount++;
	aWorld->fScaffold[nCount][0] = (int(RightWall_X*0.6));
	aWorld->fScaffold[nCount][1] = (int(RightWall_X*0.8));
	aWorld->fScaffold[nCount][2] = height + 5;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//7本目
	height += 5;
	nCount++;
	aWorld->fScaffold[nCount][0] = (int(RightWall_X*0.1));
	aWorld->fScaffold[nCount][1] = (int(RightWall_X*0.3));
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//8本目
	height += 10;
	nCount++;
	aWorld->fScaffold[nCount][0] = (int(RightWall_X*0.3));
	aWorld->fScaffold[nCount][1] = (int(RightWall_X*0.6));
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//9本目
	height += 5;
	nCount++;
	aWorld->fScaffold[nCount][0] = (int(RightWall_X*0.05));
	aWorld->fScaffold[nCount][1] = (int(RightWall_X*0.3));
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//10本目
	height += 5;
	nCount++;
	aWorld->fScaffold[nCount][0] = (int(RightWall_X*0.3));
	aWorld->fScaffold[nCount][1] = (int(RightWall_X*0.95));
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//11本目
	height += 10;
	nCount++;
	aWorld->fScaffold[nCount][0] = (int(RightWall_X*0.05));
	aWorld->fScaffold[nCount][1] = (int(RightWall_X*0.5));
	aWorld->fScaffold[nCount][2] = height;
	aWorld->fEnemy_Start_Position[0][nCount] = height - 1;
	aWorld->fEnemy_Start_Position[1][nCount] = (aWorld->fScaffold[nCount][1] / 2);

	//空中床の最大数
	aWorld->nScaffold = nCount + 1;

	//バグ回避
	nCount += 1;
	aWorld->fScaffold[nCount][0] = 2;
	aWorld->fScaffold[nCount][1] = 299;
	aWorld->fScaffold[nCount][2] = UnderWall;
}